const pickFromObject = (obj, keys) => keys
  .map(k => k in obj ? {[k]: obj[k]} : {})
  .reduce((res, o) => Object.assign(res, o), {});
export default pickFromObject;
