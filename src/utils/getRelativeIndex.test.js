import getRelativeIndex from './getRelativeIndex';
describe('getRelativeIndex', () => {
  test.each([
    [2,2,5,false,4],
    [2,-1,5,false,1],
    [2,7,5,false,4],
    [2,-5,5,false,0],
    [2,12,5,true,4],
    [2,-4,5,true,3]
  ])('currentIndex: %i, distance: %i, length: %i, loop: %s, returns: %i',(a,b,c,d, f) => {
    expect(getRelativeIndex(a,b,c,d)).toBe(f)
  })
})
