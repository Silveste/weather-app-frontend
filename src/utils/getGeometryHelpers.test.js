import { getCentroid, contains } from "./geometryHelpers";
const polygon = {
  type: "Polygon",
  coordinates: [
    [
      [0, 0],
      [10, 0],
      [10, 10],
      [0, 10],
      [0, 0]
    ]
  ]
};

const multiPolygon = {
  type: "MultiPolygon",
  coordinates: [
    [
      [
        [0, 0],
        [10, 0],
        [10, 10],
        [0, 10],
        [0, 0]
      ]
    ],
    [
      [
        [100, 100],
        [90, 100],
        [90, 90],
        [100, 90],
        [100, 100]
      ]
    ]
  ]
};

describe("getCentroid", () => {
  test("Returns centroid of Polygon", () => {
    const centroid = getCentroid(polygon);
    expect(centroid).toEqual([5, 5]);
  });
  test("Returns centroid of MultiPolygon", () => {
    const centroid = getCentroid(multiPolygon);
    expect(centroid).toEqual([50, 50]);
  });
  test("Returns point if is a Point", () => {
    const centroid = getCentroid({ type: "Point", coordinates: [2, 3] });
    expect(centroid).toEqual([2, 3]);
  });
});

describe("contains", () => {
  test("Returns if contained by Polygon", () => {
    expect(contains(polygon, [9, 8])).toBe(true);
    expect(contains(polygon, [9, 11])).toBe(false);
    expect(contains(polygon, [-1, 9])).toBe(false);
  });
  test("Returns if contained by MultiPolygon", () => {
    expect(contains(multiPolygon, [9, 8])).toBe(true);
    expect(contains(multiPolygon, [95, 92])).toBe(true);
    expect(contains(multiPolygon, [45, 45])).toBe(false);
  });
  test("Returns false if is not MultiPolygon or Polygon", () => {
    expect(contains({ type: "Point", coordinates: [2.4] }, [2, 4])).toBe(false);
  });
});
