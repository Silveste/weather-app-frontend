let lastId = 0;

const getUniqueId = (prefix='id') => {
  lastId++;
  return `${prefix}-${lastId}`;
}

export default getUniqueId;
