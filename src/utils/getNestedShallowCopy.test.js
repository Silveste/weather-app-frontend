import getNestedShallowCopy from "./getnestedShallowCopy";

const root = {
  livingRoom: {
    sofa: ["sofa1", "sofa2"],
    tv: "tv",
    tables: {
      centre: "center table",
      side: "side table"
    }
  },
  kitchen: {
    table: "table",
    chairs: ["chair1", "chair2", "chair3", "chair4"],
    appliances: [
      "fridge",
      ["blender1", "blender2"],
      { cooker: ["electric cooker", "gas cooker"], oven: ["grill", "oven"] },
      "microwave",
      "dishwasher"
    ]
  }
};

test("I returns new shallow copy of rootData", () => {
  const payload = "Something that will not be inserted";
  const path = "";
  const result = getNestedShallowCopy(root, root, { path, payload });
  expect(result).not.toBe(root);
  expect(result).toEqual(root);
});
test("Return copy with path pointing to payload", () => {
  const payload = ["armchair1", "armchair2"];
  const path = "livingRoom.armchair";
  const result = getNestedShallowCopy(root, { path, payload });
  expect(result.livingRoom.armchair).toBe(payload);
});
test("All nodes in path are a shallow copy of the original in rootData", () => {
  const path = "kitchen.appliances[2].cooker";
  const payload = ["oven", "electric cooker"];
  const result = getNestedShallowCopy(root, { path, payload });
  expect(result).not.toBe(root);
  expect(result).toContainAllKeys(Object.keys(root));
  expect(result.kitchen).not.toBe(root.kitchen);
  expect(result.kitchen).toContainAllKeys(Object.keys(root.kitchen));
  expect(result.kitchen.appliances).not.toBe(root.kitchen.appliances);
  expect(result.kitchen.appliances).toBeArrayOfSize(
    root.kitchen.appliances.length
  );
  expect(result.kitchen.appliances[2]).not.toBe(root.kitchen.appliances[2]);
  expect(result.kitchen.appliances[2]).toContainAllKeys(
    Object.keys(root.kitchen.appliances[2])
  );
  expect(result.kitchen.appliances[2].cooker).toBe(payload);
});
test("All nested nodes in copy not in path are the same as in rootData", () => {
  const path = "kitchen.appliances[2].cooker";
  const payload = ["oven", "electric cooker"];
  const result = getNestedShallowCopy(root, { path, payload });
  expect(result).not.toBe(root);
  expect(result).toContainAllKeys(Object.keys(root));
  expect(result.livingRoom).toBe(root.livingRoom);
  expect(result.kitchen.table).toBe(root.kitchen.table);
  expect(result.kitchen.chairs).toBe(root.kitchen.chairs);
  expect(result.kitchen.appliances[0]).toBe(root.kitchen.appliances[0]);
  expect(result.kitchen.appliances[1]).toBe(root.kitchen.appliances[1]);
  expect(result.kitchen.appliances[2].oven).toBe(
    root.kitchen.appliances[2].oven
  );
  expect(result.kitchen.appliances[3]).toBe(root.kitchen.appliances[3]);
});
test("Path can have dot notation", () => {
  const path = "livingRoom.tables.side";
  const payload = root.livingRoom.tables.side;
  const result = getNestedShallowCopy(root, { path, payload });
  expect(result).not.toBe(root);
  expect(result).toEqual(root); //payload points to same primitive in root
});
test("Path can have square bracket notation", () => {
  const path = "livingRoom[tables][side]";
  const payload = root.livingRoom.tables.side;
  const result = getNestedShallowCopy(root, { path, payload });
  expect(result).not.toBe(root);
  expect(result).toEqual(root); //payload points to same primitive in root
});
test("Path can have mixed dot and square bracket notation", () => {
  const path = "livingRoom[tables].side";
  const payload = root.livingRoom.tables.side;
  const result = getNestedShallowCopy(root, { path, payload });
  expect(result).not.toBe(root);
  expect(result).toEqual(root); //payload points to same primitive in root
});
test("Path can include arrays", () => {
  const path = "kitchen.appliances[2][oven][0]";
  const payload = root.kitchen.appliances[2].oven[0];
  const result = getNestedShallowCopy(root, { path, payload });
  expect(result).not.toBe(root);
  expect(result).toEqual(root); //payload points to same primitive in root
});
test("If path do not exist in root, it is created in the copy", () => {
  const path = "rooms[0].wardrobe.drawers[0]";
  const payload = "shoes";
  const result = getNestedShallowCopy(root, { path, payload });
  expect(result.rooms[0].wardrobe.drawers[0]).toBe(payload);
  expect(result.rooms).toBeArrayOfSize(1);
  expect(result.rooms[0].wardrobe).toBeObject();
  expect(result.rooms[0].wardrobe.drawers).toBeArrayOfSize(1);
});
test("Dynamic node names can be provided using dynamicProperties parameter", () => {
  const dynamicKey = "@<.sd[e]f'\"[0]ç0";
  const path = "rooms[0].wardrobe.dynamicKey.drawers[0]";
  const payload = "shoes";
  const result = getNestedShallowCopy(root, {
    path,
    payload,
    dynamicProperties: { dynamicKey }
  });
  expect(result.rooms[0].wardrobe).not.toHaveProperty("dynamicKey");
  expect(result.rooms[0].wardrobe[dynamicKey]).toEqual({ drawers: [payload] });
});
test("It allows to include different payloads", () => {
  const dynamicKey = "@<.sd[e]f'\"[0]ç0";
  const path1 = "rooms[0].wardrobe.dynamicKey.drawers[0]";
  const payload1 = { shoes: "shoes" };
  const path2 = "livingRoom.armchair";
  const payload2 = ["armchair1", "armchair2"];
  const result = getNestedShallowCopy(root, [
    { path: path1, payload: payload1, dynamicProperties: { dynamicKey } },
    { path: path2, payload: payload2 }
  ]);
  expect(result.rooms[0].wardrobe[dynamicKey].drawers[0]).toBe(payload1);
  expect(result.livingRoom.armchair).toEqual(payload2);
});
test("On Nil payload, If pointing to an object, deletes the last key of the path", () => {
  const path = "kitchen.appliances[2].oven";
  const result = getNestedShallowCopy(root, { path });
  expect(Object.keys(result.kitchen.appliances[2])).not.toIncludeAnyMembers([
    "oven"
  ]);
  expect(result.kitchen.appliances[2].oven).toBeUndefined();
});
test("On Nil payload, If pointing to an array index, removes the item", () => {
  const path = "kitchen.appliances[1][0]";
  const result = getNestedShallowCopy(root, { path });
  expect(result.kitchen.appliances[1]).toBeArrayOfSize(
    root.kitchen.appliances[1].length - 1
  );
  expect(result.kitchen.appliances[1]).not.toIncludeAnyMembers([
    root.kitchen.appliances[1][0]
  ]);
});
