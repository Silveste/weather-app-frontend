import React from 'react';
import { render } from '@testing-library/react';
import SideDrawer from './index';

jest.mock(
  '../UI/Buttons/MutableButton',
  () => (props) => (
    <button
      data-testid='Switcher'
      onClick={props.onAction}
      className={props.className} />
  )
);
jest.mock(
  '../UI/Backdrop',
  () => (props) => (<div>backdrop</div>)
);

//Parent props initialized beforeEach
let mockParentProps;
jest.mock(
  '../../containers/LocManager',
  () => (props) => (<div data-testid="LocManager">{props.children(mockParentProps)}</div>)
);
jest.mock(
  '../Search',
  () => (props) => (<form role="search"></form>)
);
jest.mock(
  '../CitiesList',
  () => (props) => (<ul></ul>)
);
jest.mock(
  '../../containers/LocsInfo',
  () => (props) => (<article data-testid='LocsInfo'></article>)
);
beforeEach(() => {
  mockParentProps = {
    handleSearch: jest.fn(),
    handleSubmit: jest.fn(),
    searchResults: [],
    searching: false,
    removeLocation: jest.fn(),
    toggleLocationSaved: jest.fn(),
    toggleLocationOnMap: jest.fn(),
    onSelectLocation: jest.fn(),
    selectedLocationName: 'aaa',
    selectedWeatherAreaId: '',
    locations: []
  }
});
describe('Content',() => {
  test('Renders the switcher button',() => {
    const { getByRole } = render(<SideDrawer />);
    getByRole('button');
  });
  test('Renders the backdrop',() => {
    const { getByText } = render(<SideDrawer />);
    getByText('backdrop');
  });
  test('Renders LocManager container',() => {
    const { getByTestId } = render(<SideDrawer />);
    getByTestId('LocManager');
  });
  test('Renders the search component', () => {
    const { getByRole } = render(<SideDrawer />);
    getByRole('search');
  });
  test('Renders the CitiesList component', () => {
    const { getByRole } = render(<SideDrawer />);
    getByRole('list');
  });
  test('Renders the LocsInfo component', () => {
    const { getByRole } = render(<SideDrawer />);
    getByRole('article');
  });
  test('If no CityItem selected, LocsInfo does not render', () => {
    mockParentProps.selectedLocationName = null;
    const { queryByTestId } = render(<SideDrawer />);
    expect(queryByTestId('LocsInfo')).toBeNil()
  });
});
