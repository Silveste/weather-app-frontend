import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Search from "./index";
import Spinner from "../UI/Spinner";

jest.mock("../../components/UI/Spinner", () => {
  return {
    __esModule: true,
    default: jest.fn(props => <div></div>)
  };
});
const mockSendMessage = jest.fn();
jest.mock("../../hooks/messages", () => () => mockSendMessage);
jest.mock("react-icons", () => {
  return {
    IconContext: {
      Provider: jest.fn(props => <div></div>)
    }
  };
});
jest.mock("react-icons/fa", () => {
  return {
    FaSearch: jest.fn(props => <div></div>),
    FaMapMarkerAlt: jest.fn(props => <div></div>),
    FaChevronUp: jest.fn(props => <div></div>)
  };
});

describe("Content", () => {
  test("It has an input field", () => {
    const { getByRole } = render(
      <Search onSearch={jest.fn()} onSubmit={jest.fn()} />
    );
    getByRole("searchbox");
  });
  test("It has a list to select the search result", () => {
    const { getByRole } = render(
      <Search onSearch={jest.fn()} onSubmit={jest.fn()} />
    );
    getByRole("list");
  });
});
describe("Behaviour", () => {
  beforeEach(() => jest.useFakeTimers());
  afterEach(() => {
    mockSendMessage.mockClear();
    jest.runOnlyPendingTimers();
    jest.useRealTimers();
  });
  test("Spinner renders when status searching is true", () => {
    Spinner.mockClear();
    render(<Search onSearch={jest.fn()} onSubmit={jest.fn()} searching />);
    expect(Spinner).toHaveBeenCalled();
  });
  test("Spinner does not render when searching is false", () => {
    Spinner.mockClear();
    render(<Search onSearch={jest.fn()} onSubmit={jest.fn()} />);
    expect(Spinner).not.toHaveBeenCalled();
  });
  test("Results list is hidden if there is no data", () => {
    const { getByRole, queryByRole } = render(
      <Search onSearch={jest.fn()} onSubmit={jest.fn()} />
    );
    expect(getByRole("list")).toHaveClass("Hidden");
    expect(queryByRole("listitem")).toBeNull();
  });
  test("Show message if search results are empty", () => {
    const { rerender } = render(
      <Search onSearch={jest.fn()} onSubmit={jest.fn()} searching={true} />
    );
    rerender(
      <Search
        onSearch={jest.fn()}
        onSubmit={jest.fn()}
        searching={false}
        data={[]}
      />
    );
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });
  test("There is a button that hides/show the result list manually", () => {
    const data = [
      { id: "123", description: "option-123" },
      { id: "abc", description: "option-abc" }
    ];
    const { getByRole } = render(
      <Search
        data={data}
        onSearch={() => jest.fn()}
        onSubmit={() => jest.fn()}
      />
    );
    const button = getByRole("button");
    const list = getByRole("list");
    expect(list).not.toHaveClass("Hidden");
    fireEvent.click(button);
    expect(list).toHaveClass("Hidden");
    fireEvent.click(button);
    expect(list).not.toHaveClass("Hidden");
  });
  test("Button does not show list when there is no data", () => {
    const { getByRole } = render(
      <Search onSearch={jest.fn()} onSubmit={jest.fn()} />
    );
    const button = getByRole("button");
    const list = getByRole("list");
    fireEvent.click(button);
    expect(list).toHaveClass("Hidden");
  });

  test("Search start when user finish typing (after 1 second) at least 3 characters", () => {
    const onSearch = jest.fn();
    const { getByRole } = render(
      <Search onSearch={onSearch} onSubmit={jest.fn()} />
    );
    const input = getByRole("searchbox");
    expect(onSearch).not.toHaveBeenCalled();
    fireEvent.change(input, { target: { value: "ab" } });
    jest.advanceTimersByTime(1000);
    expect(onSearch).not.toHaveBeenCalled();
    fireEvent.change(input, { target: { value: "abc" } });
    jest.advanceTimersByTime(999);
    expect(onSearch).not.toHaveBeenCalled();
    jest.advanceTimersByTime(1000);
    expect(onSearch).toHaveBeenCalledTimes(1);
  });
  test("Submit form when result is selected by click", () => {
    const onSubmit = jest.fn();
    const { getByText } = render(
      <Search
        onSearch={jest.fn()}
        onSubmit={onSubmit}
        data={[{ id: "123", description: "option-123" }]}
      />
    );
    const item1 = getByText("option-123");
    fireEvent.click(item1);
    expect(onSubmit).toHaveBeenCalledTimes(1);
    expect(onSubmit).toHaveBeenCalledWith("123");
  });
  test("Submit form when result is selected by Enter key when focused", () => {
    const onSubmit = jest.fn();
    const { getByText } = render(
      <Search
        onSearch={jest.fn()}
        onSubmit={onSubmit}
        data={[{ id: "123", description: "option-123" }]}
      />
    );
    const item1 = getByText("option-123");
    fireEvent.keyDown(item1, { key: "Enter", keyCode: 13 });
    expect(onSubmit).toHaveBeenCalledTimes(1);
    expect(onSubmit).toHaveBeenCalledWith("123");
  });
  test("Arrow Up/Down navigate through results", () => {
    const onSubmit = jest.fn();
    const { getByText } = render(
      <Search
        onSearch={jest.fn()}
        onSubmit={onSubmit}
        data={[
          { id: "123", description: "option-123" },
          { id: "abc", description: "option-abc" }
        ]}
      />
    );
    const item1 = getByText("option-123");
    const itema = getByText("option-abc");
    fireEvent.keyDown(item1, { key: "ArrowDown", keyCode: 40 });
    expect(item1).not.toHaveClass("SelectedItem");
    expect(itema).toHaveClass("SelectedItem");
    fireEvent.keyDown(itema, { key: "ArrowDown", keyCode: 40 });
    expect(item1).toHaveClass("SelectedItem");
    expect(itema).not.toHaveClass("SelectedItem");
    fireEvent.keyDown(item1, { key: "ArrowUp", keyCode: 38 });
    expect(item1).not.toHaveClass("SelectedItem");
    expect(itema).toHaveClass("SelectedItem");
    fireEvent.keyDown(itema, { key: "ArrowUp", keyCode: 38 });
    expect(item1).toHaveClass("SelectedItem");
    expect(itema).not.toHaveClass("SelectedItem");
  });
  test("Escape hides results", () => {
    const { getByRole } = render(
      <Search
        onSearch={jest.fn()}
        onSubmit={jest.fn()}
        data={[{ id: "123", description: "option-123" }]}
      />
    );
    const list = getByRole("list");
    expect(list).not.toHaveClass("Hidden");
    fireEvent.keyDown(list, { key: "Escape", keyCode: 27 });
    expect(list).toHaveClass("Hidden");
  });
  test("Focus outside Search hides results", () => {
    const { getByRole, container } = render(
      <Search
        onSearch={jest.fn()}
        onSubmit={jest.fn()}
        data={[{ id: "123", description: "option-123" }]}
      />
    );
    const list = getByRole("list");
    expect(list).not.toHaveClass("Hidden");
    fireEvent.blur(container.firstChild);
    expect(list).toHaveClass("Hidden");
  });
});
