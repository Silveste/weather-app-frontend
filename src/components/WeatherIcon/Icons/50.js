//Workaround for create react app issue: https://github.com/facebook/create-react-app/issues/5276
import React from 'react';
import { ReactComponent as Image } from '../../../assets/images/weather-icons/50.svg';

const Icon = (props) => {
  return <Image {...props}/>
}

export default Icon;
