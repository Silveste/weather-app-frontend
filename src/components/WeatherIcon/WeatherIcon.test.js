import React from 'react';
import { waitFor, screen, render } from '@testing-library/react';
import WeatherIcon from './index';

// Mocks for icons
jest.mock(
  './Icons/01',
  () => (props) => (
    <svg
      data-testid='01'
    />
  )
);

jest.mock(
  './Icons/02',
  () => (props) => (
    <svg
      data-testid='02'
    />
  )
);

jest.mock(
  './Icons/03',
  () => (props) => (
    <svg
      data-testid='03'
    />
  )
);

jest.mock(
  './Icons/09',
  () => (props) => (
    <svg
      data-testid='09'
    />
  )
);

jest.mock(
  './Icons/11',
  () => (props) => (
    <svg
      data-testid='11'
    />
  )
);

jest.mock(
  './Icons/13',
  () => (props) => (
    <svg
      data-testid='13'
    />
  )
);

jest.mock(
  './Icons/50',
  () => (props) => (
    <svg
      data-testid='50'
    />
  )
);

jest.mock(
  '../UI/Spinner',
  () => (props) => (
    <svg
      data-testid='spinner'
    />
  )
);

describe('content', () => {
  test('It renders an svg',() => {
    const { container } = render(<WeatherIcon height={50} width={50} />);
    expect(container.querySelector('svg')).not.toBeNull();
  });
});

describe('behaviour', () => {
  test.each([
    ['01d','01'],
    ['01n','01'],
    ['02d','02'],
    ['02n','02'],
    ['03d','03'],
    ['03n','03'],
    ['04d','03'],
    ['04n','03'],
    ['09d','09'],
    ['09n','09'],
    ['10d','09'],
    ['10n','09'],
    ['11d','11'],
    ['11n','11'],
    ['13d','13'],
    ['13n','13'],
    ['50d','50'],
    ['50n','50']
  ])('It renders the correct icon type: %s', async (type, id) => {
    const consoleErrorMock = jest.spyOn(console,'error');
    render(<WeatherIcon iconType={type} height={50} width={50} />);
    await waitFor(() => screen.getByTestId(id));
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
    consoleErrorMock.mockRestore();
  });
  test('It should render a spinner when the icon type is not provided', () => {
    const consoleErrorMock = jest.spyOn(console,'error');
    render(<WeatherIcon height={50} width={50}/>);
    screen.getByTestId(/spinner/i);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
    consoleErrorMock.mockRestore();
  })
})
