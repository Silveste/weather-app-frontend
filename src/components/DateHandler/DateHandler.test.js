import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import DateHandler from './index';

//Mocks for internal functions
jest.mock(
  '../UI/Buttons/MutableButton',
  () => (props) => (
    <button onClick={props.disabled ? null : props.onAction}>
      {props.mode}
    </button>
  )
);
jest.mock(
  '../UI/ScreenLoader',
  () => (props) => (
    <div data-testid='ScreenLoader'>
    </div>
  )
);

//Mocks for props
//Wednesday 1st of January of 2020
const minDay = new Date(2020, 0, 1);
//Thursday 2nd of January of 2020
const selectedDay = new Date(2020, 0, 2);
//Monday 6th ofJanualy of 2020
const maxDay = new Date(2020, 0, 6);
const newDayHandler = jest.fn();


describe('Content', () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console,'error');
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test('It renders the selected day', () => {
    const { getByText } = render(
      <DateHandler
        dates={{
          min: minDay,
          max: maxDay,
          current: selectedDay,
        }}
        dayHandler={newDayHandler}
      />
    );
    const timeElement = getByText(/Thu.*?2.*?Jan/i);
    expect(timeElement).toHaveAttribute('datetime', selectedDay.toISOString());
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders a button to move a day forward', () => {
    const { getByText } = render(
      <DateHandler
        dates={{
          min: minDay,
          max: maxDay,
          current: selectedDay,
        }}
        dayHandler={newDayHandler}
      />
    );
    getByText(/right/i);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders a button to move a day backward', () => {
    const { getByText } = render(
      <DateHandler
        dates={{
          min: minDay,
          max: maxDay,
          current: selectedDay,
        }}
        dayHandler={newDayHandler}
      />
    );
    getByText(/left/i);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});
describe('Behaviour', () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console,'error');
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  beforeEach(() => {
      newDayHandler.mockReset();
  })
  test('Allows to move forward the day', () => {
    const { getByText } = render(
      <DateHandler
        dates={{
          min: minDay,
          max: maxDay,
          current: selectedDay,
        }}
        dayHandler={newDayHandler}
      />
    );
    fireEvent.click(getByText(/right/i));
    expect(newDayHandler).toHaveBeenCalledWith(1);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('Allows to move backward the day', () => {
      const { getByText } = render(
        <DateHandler
          dates={{
            min: minDay,
            max: maxDay,
            current: selectedDay,
          }}
          dayHandler={newDayHandler}
        />
      );
      fireEvent.click(getByText(/left/i));
      expect(newDayHandler).toHaveBeenCalledWith(-1);
      // If prop-types print errors the test is not passed
      expect(consoleErrorMock).not.toHaveBeenCalled();
    });
  test('If selected day is the first, do not allow to move backward', () => {
    const { getByText } = render(
      <DateHandler
        dates={{
          min: minDay,
          max: maxDay,
          current: minDay,
        }}
        dayHandler={newDayHandler}
      />
    );
    fireEvent.click(getByText(/left/i));
    expect(newDayHandler).not.toHaveBeenCalled();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test
  ('If selected day is the last, do not allow to move forward', () => {
    const { getByText } = render(
      <DateHandler
        dates={{
          min: minDay,
          max: maxDay,
          current: maxDay,
        }}
        dayHandler={newDayHandler}
      />
    );
    fireEvent.click(getByText(/right/i));
    expect(newDayHandler).not.toHaveBeenCalled();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('Dates not present renders screen loader', () => {
    const { getByTestId } = render(
      <DateHandler dayHandler={newDayHandler} />
    );
    getByTestId(/screenloader/i);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
})
