import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import CitiesList from './index';

jest.mock(
  './CityItem',
  () => (props) => (<div>CityItem</div>)
);

describe('Content', () =>{
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console,'error');
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test('It has a list container', () => {
    const { getByRole } = render(<CitiesList onSelect={() => jest.fn()} />);
    getByRole('list');
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('List items are CityItem components', () => {
    const { getAllByText } = render(
      <CitiesList
        data={[
          {id: 'item 1', name: 'Item 1'},
          { id: 'Item 2', name: 'Item 2'}
        ]}
        onSelect={() => jest.fn()}
      />
    );
    expect(getAllByText('CityItem')).toHaveLength(2);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  })
});
describe('Behaviour', () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console,'error');
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test('On focus of List or any children, Up/down arrow keys change focus and item selected', () => {
    const mockOnSelect = jest.fn();
    const { container } = render(
      <CitiesList
        data={[
          {id: 'item1', name: 'Item 1'},
          {id: 'item2', name: 'Item 2'},
        ]}
        onSelect={mockOnSelect}
      />
    )
    const parent = container.querySelector('ul')
    expect(mockOnSelect).not.toHaveBeenCalled();
    fireEvent.keyDown(parent, { key: 'ArrowDown', keyCode: 40 });
    expect(mockOnSelect).toHaveBeenCalledTimes(1);
    expect(mockOnSelect).toHaveBeenCalledWith('item1');
    fireEvent.keyDown(parent, { key: 'ArrowDown', keyCode: 40 });
    expect(mockOnSelect).toHaveBeenCalledTimes(2);
    expect(mockOnSelect).toHaveBeenCalledWith('item2');
    fireEvent.keyDown(parent, { key: 'ArrowDown', keyCode: 40 });
    expect(mockOnSelect).toHaveBeenCalledTimes(3);
    expect(mockOnSelect).toHaveBeenCalledWith('item1');
    fireEvent.keyDown(parent, { key: 'ArrowUp', keyCode: 38 });
    expect(mockOnSelect).toHaveBeenCalledTimes(4);
    expect(mockOnSelect).toHaveBeenCalledWith('item2');
    fireEvent.keyDown(parent, { key: 'ArrowUp', keyCode: 38 });
    expect(mockOnSelect).toHaveBeenCalledTimes(5);
    expect(mockOnSelect).toHaveBeenCalledWith('item1');
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});
