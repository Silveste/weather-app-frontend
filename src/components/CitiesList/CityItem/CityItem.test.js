import React from "react";
import { render, fireEvent } from "@testing-library/react";
import CityItem from "./index";
import wihUserInput from "../../../hoc/withUserInput";

/**
 * Mocks for internal components
 */
jest.mock("../../../hoc/withUserInput", () => {
  return {
    __esModule: true,
    default: jest.fn((ChildComponent, ...rest) => {
      return props => <ChildComponent {...props} />;
    })
  };
});

jest.mock("../../UI/Spinner", () => props => (
  <div data-testid="Spinner">Spinner</div>
));

/**
 * Internal defaults
 */
// All buttons have a title used to query them
const btnTitles = {
  tglSaved: /bookmarks?/i,
  tglMap: /map/i,
  delete: /delete/i
};

/**
 * Mocks for props
 */
// Some test require only the name of the prop
const propsNames = {
  remove: "removeLocation",
  save: "toggleSaved"
};

const props = {
  city: {
    id: "1",
    name: "name 1",
    desc: "Description 1",
    weatherAreaId: "wa1",
    isOnMap: true,
    isSaved: true
  },
  toggleOnMap: jest.fn(),
  toggleSaved: jest.fn(),
  removeLocation: jest.fn(),
  onSelect: jest.fn()
};

describe("Content", () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console, "error");
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test("It displays the city name", () => {
    const { city } = props;
    const { getByText } = render(<CityItem {...props} />);
    getByText(city.name);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It displays the city description as a tooltip if provided", () => {
    const { city } = props;
    const { desc, name } = city;
    const cityNoDesc = { ...city, desc: undefined };
    const { getByText, queryByTitle, rerender } = render(
      <CityItem {...props} city={cityNoDesc} />
    );
    getByText(name);
    expect(queryByTitle(desc)).toBeNull();
    rerender(<CityItem {...props} />);
    getByText(name);
    expect(queryByTitle(desc)).not.toBeNull();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It has a button to save/remove from local storage", () => {
    const { getByRole } = render(<CityItem {...props} />);
    getByRole("button", { name: btnTitles.tglSaved });
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It has a button to show/hide from map", () => {
    const { getByRole } = render(<CityItem {...props} />);
    getByRole("button", { name: btnTitles.tglMap });
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It has a button to remove location", () => {
    const { getByRole } = render(<CityItem {...props} />);
    getByRole("button", { name: btnTitles.delete });
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Calls onSelect when focus on element", () => {
    props.onSelect.mockReset();
    const { container } = render(<CityItem {...props} />);
    expect(props.onSelect).not.toHaveBeenCalled();
    fireEvent.focus(container.firstChild);
    expect(props.onSelect).toHaveBeenCalledTimes(1);
  });
  test("Calls onSelect when focus on element's children", () => {
    props.onSelect.mockReset();
    const { container } = render(<CityItem {...props} />);
    expect(props.onSelect).not.toHaveBeenCalled();
    fireEvent.focus(container.firstChild.firstChild);
    expect(props.onSelect).toHaveBeenCalledTimes(1);
  });
});

describe("Behaviour", () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console, "error");
  });
  // After each reset mocks
  afterEach(() => {
    const { toggleOnMap, toggleSaved, removeLocation } = props;
    toggleOnMap.mockClear();
    toggleSaved.mockClear();
    removeLocation.mockClear();
    consoleErrorMock.mockRestore();
  });
  test("Click on save/remove (localstorage) calls toggleSaved prop", () => {
    const { toggleSaved } = props;
    const { getByRole } = render(<CityItem {...props} />);
    const saveButton = getByRole("button", { name: btnTitles.tglSaved });
    fireEvent.click(saveButton);
    expect(toggleSaved).toHaveBeenCalledTimes(1);
    expect(toggleSaved).toHaveBeenCalledWith(props.city.id);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Click on show/hide calls toggleOnMap prop", () => {
    const { toggleOnMap } = props;
    const { getByRole } = render(<CityItem {...props} />);
    const showIconBtn = getByRole("button", { name: btnTitles.tglMap });
    fireEvent.click(showIconBtn);
    expect(toggleOnMap).toHaveBeenCalledTimes(1);
    expect(toggleOnMap).toHaveBeenCalledWith(props.city.id);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Click on remove calls removeLocation prop", () => {
    const { removeLocation } = props;
    const { getByRole } = render(<CityItem {...props} />);
    const showIconBtn = getByRole("button", { name: btnTitles.delete });
    fireEvent.click(showIconBtn);
    expect(removeLocation).toHaveBeenCalledTimes(1);
    expect(removeLocation).toHaveBeenCalledWith(props.city.id);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If waId is empty string, displays spinner instead show/hide button", () => {
    const { city } = props;
    const noWACity = { ...city, weatherAreaId: "" };
    const testProps = { ...props, city: noWACity };
    const { getByTestId } = render(<CityItem {...testProps} />);
    getByTestId("Spinner");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  //TODO: skiped until bookmark feature is setup
  test.skip("On click to remove from bookmarks triggers confirmation message", () => {
    //withUserInput is called when importing CityItem, no need for call render
    const { cbName, inputRequired } = wihUserInput.mock.calls[0][2];
    const fakeProps = { city: { isSaved: true } };
    const inputRequiredResult = inputRequired(null, fakeProps);
    expect(inputRequiredResult).toBeTrue();
    expect(cbName).toBe(propsNames.save);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  //TODO: skiped until bookmark feature is setup
  test.skip("On click to bookmark location do not trigger confirmation message", () => {
    //withUserInput is called when importing CityItem, no need for call render
    const { cbName, inputRequired } = wihUserInput.mock.calls[0][2];
    const fakeProps = { city: { isSaved: false } };
    const inputRequiredResult = inputRequired(null, fakeProps);
    expect(inputRequiredResult).toBeFalse();
    expect(cbName).toBe(propsNames.save);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("On click to remove location triggers confirmation message", () => {
    //withUserInput is called when importing CityItem, no need for call render
    const { cbName } = wihUserInput.mock.calls[0][1];
    expect(cbName).toBe(propsNames.remove);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});
