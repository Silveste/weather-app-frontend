export const day = {
  date: new Date("2020-10-10T12:00:00.000Z"),
  sunset: new Date("2020-10-08T19:00:00Z"),
  sunrise: new Date("2020-10-08T08:00:00Z"),
  temp: {
    avg: 15,
    feelsLike: 11
  },
  wind: {
    deg: 125,
    speed: 234
  },
  main: {
    main: "Cloudy",
    icon: "10n",
    description: "Clouds and small rain"
  },
  clouds: 75
};
