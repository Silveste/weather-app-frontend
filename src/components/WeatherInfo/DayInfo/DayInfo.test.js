import React from "react";
import { render } from "@testing-library/react";
import DayInfo from "./index.js";

//Mocks for props
import { day } from "./__mocks__/props";

//Mock for internal components
jest.mock("../../WeatherIcon", () => props => (
  <div data-testid="WeatherIcon">{props.iconType}</div>
));

//Time format functions
const timeFormat = new Intl.DateTimeFormat("en", {
  timeStyle: "short",
  timeZone: "Europe/Dublin"
});
//Expected values
delete day.date;
const sunriseText = timeFormat.format(day.sunrise);
const sunsetText = timeFormat.format(day.sunset);

describe("Content", () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console, "error");
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test.each([
    ["sunrise", sunriseText, false],
    ["sunset", sunsetText, false],
    ["clouds", day.clouds.toString(), false],
    ["temp.avg", day.temp.avg.toString(), false],
    ["temp.feelsLike", day.temp.feelsLike.toString(), false],
    ["wind.deg", day.wind.deg.toString(), true],
    ["wind.speed", day.wind.speed.toString(), false],
    ["main.main", day.main.main, false],
    ["main.description", day.main.description, false]
  ])("It renders: %s", (propName, propValue, notText) => {
    const { getByTitle, getByText } = render(<DayInfo data={day} />);
    getByTitle(propValue, { exact: false });
    if (!notText) {
      getByText(propValue, { exact: false });
    }
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders the correct weather icon", () => {
    const { getByText } = render(<DayInfo data={day} />);
    getByText(day.main.icon);
  });
});
