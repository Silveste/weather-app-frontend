export const days = {
  "2020-10-06T12:00:00.000Z": {
    date: new Date("2020-10-06 12:00:00.000Z"),
    sunrise: new Date("2020-10-06 06:38:36.000Z"),
    sunset: new Date("2020-10-06 17:51:24.000Z"),
    temp: {
      avg: 86.05,
      feelsLike: 81.52,
      min: 82.37,
      max: 87.59
    },
    wind: {
      deg: 311,
      speed: 6.43
    },
    main: {
      main: "Rain",
      description: "moderate rain",
      icon: "10d"
    },
    rain: 3.44,
    clouds: 80
  },
  "2020-10-07T12:00:00.000Z": {
    date: new Date("2020-10-07 12:00:00.000Z"),
    sunrise: new Date("2020-10-07 06:40:26.000Z"),
    sunset: new Date("2020-10-07 17:48:58.000Z"),
    temp: {
      avg: 85.4,
      feelsLike: 82.99,
      min: 80,
      max: 85.49
    },
    wind: {
      deg: 216,
      speed: 3.17
    },
    main: {
      main: "Rain",
      description: "light rain",
      icon: "10d"
    },
    rain: 2.8,
    clouds: 100
  },
  "2020-10-08T12:00:00.000Z": {
    date: new Date("2020-10-08 12:00:00.000Z"),
    sunrise: new Date("2020-10-08 06:42:17.000Z"),
    sunset: new Date("2020-10-08 17:46:33.000Z"),
    temp: {
      avg: 84.33,
      feelsLike: 80.49,
      min: 77.95,
      max: 84.33
    },
    wind: {
      deg: 4.4,
      speed: 301
    },
    main: {
      main: "Rain",
      description: "moderate rain",
      icon: "01n"
    },
    snow: 3.04,
    clouds: 8
  },
  "2020-10-09T12:00:00.000Z": {
    date: new Date("2020-10-09 12:00:00.000Z"),
    sunrise: new Date("2020-10-09 06:44:08.000Z"),
    sunset: new Date("2020-10-09 17:44:08.000Z"),
    temp: {
      avg: 83.38,
      feelsLike: 77.54,
      min: 78.19,
      max: 83.38
    },
    wind: {
      deg: 279,
      speed: 6.97
    },
    main: {
      main: "Rain",
      description: "moderate rain",
      icon: "10d"
    },
    rain: 5.37,
    snow: 2.6,
    clouds: 65
  },
  "2020-10-10T12:00:00.000Z": {
    date: new Date("2020-10-10T12:00:00.000Z"),
    sunrise: new Date("2020-10-10 06:45:59.000Z"),
    sunset: new Date("2020-10-10 17:41:45.000Z"),
    temp: {
      avg: 85.3,
      feelsLike: 80.12,
      min: 79.62,
      max: 85.3
    },
    wind: {
      deg: 307,
      speed: 6.42
    },
    main: {
      main: "Rain",
      description: "light rain",
      icon: "09d"
    },
    rain: 1.4,
    snow: 3.2,
    clouds: 3
  },
  "2020-10-11T12:00:00.000Z": {
    date: new Date("2020-10-11T12:00:00.000Z"),
    sunrise: new Date("2020-10-11 06:47:51.000Z"),
    sunset: new Date("2020-10-11 17:39:21.000Z"),
    temp: {
      avg: 84.49,
      feelsLike: 82.38,
      min: 77.43,
      max: 84.61
    },
    wind: {
      deg: 248,
      speed: 2.23
    },
    main: {
      main: "Rain",
      description: "light rain",
      icon: "50d"
    },
    clouds: 33
  },
  "2020-10-12T12:00:00.000Z": {
    date: new Date("2020-10-12T12:00:00.000Z"),
    sunrise: new Date("2020-10-12 06:49:43.000Z"),
    sunset: new Date("2020-10-12 17:36:59.000Z"),
    temp: {
      avg: 281.94,
      feelsLike: 274.95,
      min: 79.95,
      max: 83.57
    },
    wind: {
      deg: 277,
      speed: 8.58
    },
    main: {
      main: "Rain",
      description: "moderate rain",
      icon: "02n"
    },
    rain: 7.18,
    clouds: 42
  }
};
