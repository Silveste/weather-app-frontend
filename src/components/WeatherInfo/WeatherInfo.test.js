import React from "react";
import { render } from "@testing-library/react";
import WeatherInfo from "./index";
//Mocks for props
import { days } from "./__mocks__/props";

jest.mock("./DayInfo", () => props => (
  <div data-testid="DayInfo">
    {Object.keys(props.data).map(k => (
      <div key={k} data-testid={k}>
        {props.data[k].toString()}
      </div>
    ))}
  </div>
));
jest.mock("./DaysInfo", () => props => (
  <div data-testid="DaysInfo">
    <div>{props.selected.toISOString()}</div>
    {props.data.map(day =>
      Object.keys(day).map(k => <div key={k} data-testid={k}></div>)
    )}
  </div>
));
jest.mock("../UI/ScreenLoader", () => props => (
  <div data-testid="ScreenLoader"></div>
));

describe("Content", () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console, "error");
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test("Renders DaysInfo", () => {
    const { getByTestId } = render(
      <WeatherInfo data={days} daySelected={Object.values(days)[0].date} />
    );
    getByTestId(/daysinfo/i);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Renders DayInfo", () => {
    const { getByTestId } = render(
      <WeatherInfo data={days} daySelected={Object.values(days)[0].date} />
    );
    getByTestId(/dayinfo/i);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});

describe("Behaviour", () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console, "error");
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test.each(["sunset", "sunrise", "clouds", "temp", "wind", "main"])(
    "DayInfo gets property: %s in datum prop",
    property => {
      const { getAllByTestId } = render(
        <WeatherInfo data={days} daySelected={Object.values(days)[0].date} />
      );
      getAllByTestId(property);
      // If prop-types print errors the test is not passed
      expect(consoleErrorMock).not.toHaveBeenCalled();
    }
  );
  test("DayInfo gets data for the selected day", () => {
    const { getAllByText } = render(
      <WeatherInfo data={days} daySelected={Object.values(days)[0].date} />
    );
    getAllByText(Object.values(days)[0].date.toDateString(), { exact: false });
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test.each(["date", "temp", "main", "rain", "snow"])(
    "DaysInfo gets property: %s in datum prop",
    property => {
      const { getAllByTestId } = render(
        <WeatherInfo data={days} daySelected={Object.values(days)[0].date} />
      );
      getAllByTestId(property);
      // If prop-types print errors the test is not passed
      expect(consoleErrorMock).not.toHaveBeenCalled();
    }
  );
  test("DaysInfo gets the day selected as a prop", () => {
    const { getByText } = render(
      <WeatherInfo data={days} daySelected={Object.values(days)[0].date} />
    );
    getByText(Object.values(days)[0].date.toISOString(), { exact: false });
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If data is null/undefined render skeleton screens", () => {
    const { getAllByTestId } = render(<WeatherInfo />);
    getAllByTestId(/screenloader/i);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});
