import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Map from "./index.js";

//Mocks for internal functions
jest.mock("../WeatherIcon", () => props => (
  <div data-testid={"Icon"} onMouseEnter={props.onMouseEnter}>
    props.iconType:{props.iconType}end
  </div>
));

jest.mock("../../containers/LocsInfo", () => props => (
  <div data-testid={"LocsInfo"}>
    start
    {Object.keys(props).map((key, i) => (
      <span key={i}>{key}</span>
    ))}
    end
  </div>
));

//Mocks for props
const icons = [
  {
    geometry: { type: "Point", coordinates: [-7.24, -54.2521] },
    iconType: "10n",
    weatherAreaId: "aswderftg",
    locationNames: "",
    iconId: "icon-1"
  }
];

const iconsClose = [
  {
    geometry: { type: "Point", coordinates: [-7.24, -54.2521] },
    iconType: "10n",
    weatherAreaId: "aswderftg",
    locationNames: "",
    iconId: "icon-1"
  },
  {
    geometry: { type: "Point", coordinates: [-7.25, -54.2621] },
    iconType: "10n",
    weatherAreaId: "bgdgrvvsd",
    locationNames: "",
    iconId: "icon-2"
  }
];

const onAction = jest.fn();
const date = new Date();

describe("content", () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console, "error");
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test("It renders an SVG with the map of Ireland", () => {
    const { getByTitle } = render(<Map date={date} onAction={onAction} />);
    getByTitle(/map/i);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders Icon(s)", () => {
    const { getAllByTestId } = render(
      <Map date={date} icons={icons} onAction={onAction} />
    );
    getAllByTestId("Icon");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});

describe("behaviour", () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console, "error");
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test("onClick in the map renders the icon", () => {
    onAction.mockReset();
    const { getByTitle } = render(
      <Map date={date} icons={icons} onAction={onAction} />
    );
    const svg = getByTitle(/map/i);
    const map = svg.querySelector("g");
    expect(onAction).not.toHaveBeenCalled();
    fireEvent.click(map);
    expect(onAction).toHaveBeenCalledWith([
      expect.any(Number),
      expect.any(Number)
    ]);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("While retrieving the Icon renders a spinner", () => {
    const { geometry } = icons[0];
    const { getByText } = render(
      <Map
        date={date}
        icons={[
          {
            iconId: "icon-1",
            geometry,
            weatherAreaId: "aswderftg",
            locationNames: ""
          }
        ]}
        onAction={onAction}
      />
    );
    getByText("props.iconType:end");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If icons are to close renders a red dot", () => {
    render(<Map date={date} icons={iconsClose} onAction={onAction} />);
    expect(screen.queryByTestId("Icon")).toBeNull();

    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("On WeatherIcon mouse enter it renders LocsInfo", () => {
    const { queryByTestId } = render(
      <Map icons={icons} onAction={onAction} date={date} />
    );
    const icon = queryByTestId("Icon");
    expect(queryByTestId("LocsInfo")).toBeNull();
    fireEvent.mouseEnter(icon);
    expect(queryByTestId("LocsInfo")).not.toBeNull();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("On red dot mouse enter it renders LocsInfo", () => {
    const { container, queryByTestId } = render(
      <Map icons={iconsClose} onAction={onAction} date={date} />
    );
    const dot = container.querySelector("svg g circle");
    expect(queryByTestId("LocsInfo")).toBeNull();
    fireEvent.mouseEnter(dot);
    expect(queryByTestId("LocsInfo")).not.toBeNull();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});
