import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import Message from "./index";

// Message props
const props = {
  close: jest.fn(),
  duration: 2000,
  title: "error",
  mode: "Danger"
};
const text = "This is a message";

describe("Content", () => {
  test("It has a close button", () => {
    const { getByRole } = render(<Message {...props}>{text}</Message>);
    getByRole("button");
  });
  test("It renders the title", () => {
    const { getByText } = render(<Message {...props}>{text}</Message>);
    getByText(props.title);
  });
  test("It renders the children nodes", () => {
    const { getByText } = render(<Message {...props}>{text}</Message>);
    getByText(text);
  });
});

describe("Behaviour", () => {
  test("Click on close button calls close prop", async () => {
    const { close } = props;
    const { getByRole } = render(<Message {...props}>{text}</Message>);
    const btn = getByRole("button");
    expect(close).not.toHaveBeenCalled();
    fireEvent.click(btn);
    await waitFor(() => {
      expect(close).toHaveBeenCalled();
    });
    close.mockClear();
  });
});
