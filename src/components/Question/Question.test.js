import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Question from './index';

/**
 * Mocks for internal components
 */
jest.mock(
  '../UI/Modal',
  () => props => (<><h1>Modal</h1>{props.children}</>)
);

jest.mock(
  '../UI/Buttons/DefaultButton',
  () => props => (
    <button
      data={props.format}
      title={props.title}
      onClick={props.onAction}
    >
      {props.children}
    </button>
  )
)

/**
 * Mocks for props
 */
// Required pops
const question = 'Question test';
const options = [
  { onAction: jest.fn(), text: 'button1' },
  { onAction: jest.fn(), text: 'button2' }
]
// optional props
const optionalOptions = [
  { onAction: jest.fn(), text: 'button1', format: 'Danger', tooltip: 'tooltip1' },
  { onAction: jest.fn(), text: 'button2', format: 'Highlight', tooltip: 'tooltip2'  }
]
/**
 * Internal defaults
 */



describe('Content', () => {
  test('It renders a question to the user', () => {
    const { getByText } = render(
      <Question options={options}>{question}</Question>
    )
    getByText(/question/i);
  });
  test('It renders buttons to allow user to answer', () => {
    const { getAllByRole } = render(
      <Question options={options}>{question}</Question>
    )
    getAllByRole('button');
  });
});

describe('Behaviour', () => {
  test('It maps a button for each control', () => {
    const { queryAllByRole } = render(
      <Question options={options}>{question}</Question>
    )
    expect(queryAllByRole('button')).toBeArrayOfSize(options.length);
  });
  test('It renders the text of the control', () => {
    const { getByText } = render(
      <Question options={options}>{question}</Question>
    )
    getByText(/button1/i);
    getByText(/button2/i);
  });
  test('It renders a tooltip if it has been specified', () => {
    const { getByText } = render(
      <Question
        options={optionalOptions}
      >
        {question}
      </Question>
    )
    expect(getByText(/button1/i)).toHaveAttribute('title', 'tooltip1');
    expect(getByText(/button2/i)).toHaveAttribute('title', 'tooltip2');
  });
  test('It renders the text as tooltip if tooltip not specified', () => {
    const { getByText } = render(
      <Question
        options={options}
      >
        {question}
      </Question>
    )
    expect(getByText(/button1/i)).toHaveAttribute('title', 'button1');
    expect(getByText(/button2/i)).toHaveAttribute('title', 'button2');
  });
  test('It executes the prop callback when user interacts with button', () => {
    const btnCbs = [jest.fn(),jest.fn()];
    const newOpts = options.map((opt, i) => {
      return {
        ...opt,
        onAction: btnCbs[i]
      }
    })
    const { getByText } = render(
      <Question
        options={newOpts}
      >
        {question}
      </Question>
    )
    const button1 = getByText(/button1/i);
    fireEvent.click(button1);
    const button2 = getByText(/button2/i);
    fireEvent.click(button2);
    expect(btnCbs[0]).toHaveBeenCalledTimes(1);
    expect(btnCbs[1]).toHaveBeenCalledTimes(1);
  });
})
