import React from 'react';
import { render } from '@testing-library/react';
import H3 from './index';

describe('Behaviour', () => {
  test('It renders children', () => {
    const text = 'This is a test';
    const { getByText } = render(<H3>{text}</H3>);
    getByText(text);
  });
})
