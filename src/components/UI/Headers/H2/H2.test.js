import React from 'react';
import { render } from '@testing-library/react';
import H2 from './index';

describe('Behaviour', () => {
  test('It renders children', () => {
    const text = 'This is a test';
    const { getByText } = render(<H2>{text}</H2>);
    getByText(text);
  });
})
