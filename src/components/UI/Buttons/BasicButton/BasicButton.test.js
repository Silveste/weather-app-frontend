import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Button from './index';

/**
 * Mocks for props
 */
// Required pops
const reqProps = {
  onAction: jest.fn(),
  title: 'title1',
  styles: { color: 'green' },
  stylesEnabled: { backgroundColor: 'yellow' },
  stylesActive: { fontSize: '12px' },
  stylesDisabled: { position: '0' }
}
const expectedStyle = 'color: green;';
const expectedActiveStyle = 'font-size: 12px;';
const expectedEnabledStyle = 'background-color: yellow;';
const expectedDisabledStyle = 'position: 0;';
const children = 'text1';

describe('Content', () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console,'error');
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test('It renders a button', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    getByRole('button');
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the children', () => {
    const { getByText } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    getByText(children);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the title', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    expect(getByRole('button')).toHaveAttribute('title', reqProps.title);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the class', () => {
    const styles = 'myClass'
    const props = { ...reqProps, styles }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    expect(getByRole('button')).toHaveClass(styles);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the enabled class', () => {
    const stylesEnabled = 'myClass'
    const props = { ...reqProps, stylesEnabled }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    expect(getByRole('button')).toHaveClass(stylesEnabled);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the active class', () => {
    const stylesActive = 'myClass'
    const props = { ...reqProps, stylesActive }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    const button = getByRole('button');
    fireEvent.keyDown(button, { key: 'Enter', keyCode: 13 });
    expect(button).toHaveClass(stylesActive);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the disabled class', () => {
    const stylesDisabled = 'myClass'
    const props = { ...reqProps, disabled: true, stylesDisabled }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    expect(getByRole('button')).toHaveClass(stylesDisabled);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the style', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    expect(getByRole('button'))
      .toHaveAttribute('style', expect.stringContaining(expectedStyle));
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the enabled style', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    expect(getByRole('button'))
      .toHaveAttribute('style', expect.stringContaining(expectedEnabledStyle));
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the active style', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    const button = getByRole('button');
    fireEvent.keyDown(button, { key: 'Enter', keyCode: 13 });
    expect(getByRole('button'))
      .toHaveAttribute('style', expect.stringContaining(expectedActiveStyle));
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It renders the disabled style', () => {
    const props = { ...reqProps, disabled: true }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    expect(getByRole('button'))
      .toHaveAttribute(
        'style', expect.stringContaining(expectedDisabledStyle)
      );
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});

describe('Behaviour', () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console,'error');
    reqProps.onAction.mockClear();
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test('Active class only renders when active', () => {
    const stylesActive = 'myClass'
    const props = { ...reqProps, stylesActive }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    const button = getByRole('button');
    fireEvent.keyDown(button, { key: 'Enter', keyCode: 13 });
    expect(button).toHaveClass(stylesActive);
    fireEvent.keyUp(button, { key: 'Enter', keyCode: 13 });
    expect(button).not.toHaveClass(stylesActive);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('Active class only renders when active', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    const button = getByRole('button');
    fireEvent.keyDown(button, { key: 'Enter', keyCode: 13 });
    expect(button)
      .toHaveAttribute('style', expect.stringContaining(expectedActiveStyle));
    fireEvent.keyUp(button, { key: 'Enter', keyCode: 13 });
    expect(getByRole('button'))
      .not.toHaveAttribute(
        'style', expect.stringContaining(expectedActiveStyle)
      );
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('Disabled class do not render when disabled false', () => {
    const stylesDisabled = 'myClass'
    const props = { ...reqProps, disabled: false, stylesDisabled }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    expect(getByRole('button')).not.toHaveClass(stylesDisabled);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('Disabled style do not render when disabled false', () => {
    const props = { ...reqProps, disabled: false }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    expect(getByRole('button'))
      .not.toHaveAttribute(
        'style', expect.stringContaining(expectedDisabledStyle)
      );
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('Enabled class do not render when disabled false', () => {
    const stylesEnabled = 'myClass'
    const props = { ...reqProps, disabled: true, stylesEnabled }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    expect(getByRole('button')).not.toHaveClass(stylesEnabled);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('Enabled style do not render when disabled true', () => {
    const props = { ...reqProps, disabled: true }
    const { getByRole } = render(
      <Button {...props}>
        {children}
      </Button>
    );
    expect(getByRole('button'))
      .not.toHaveAttribute(
        'style', expect.stringContaining(expectedEnabledStyle)
      );
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It executes the call back on click', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    const btn = getByRole('button');
    fireEvent.click(btn);
    expect(reqProps.onAction).toHaveBeenCalledTimes(1);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('It executes the call back on pressing enter (onkeyup)', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    const btn = getByRole('button');
    fireEvent.keyUp(btn, { key: 'Enter', keyCode: 13 });
    fireEvent.keyUp(btn, { key: 'a', keyCode: 65 });
    expect(reqProps.onAction).toHaveBeenCalledTimes(1);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('If disabled it does not execute the callback', () => {
    const { getByRole } = render(
      <Button {...reqProps} disabled >
        {children}
      </Button>
    );
    const btn = getByRole('button');
    fireEvent.click(btn);
    expect(reqProps.onAction).not.toHaveBeenCalled();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test('Button loose focus after being activated', () => {
    const { getByRole } = render(
      <Button {...reqProps} >
        {children}
      </Button>
    );
    const button = getByRole('button');
    button.focus();
    fireEvent.click(button);
    expect(button).not.toHaveFocus();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
})
