import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Button from './index';

// Mocks for internal functions
jest.mock(
  '../BasicButton',
  () => (props) => (
    <button
      className={
        [
            props.styles,
            props.stylesEnabled,
            props.stylesActive,
            props.stylesDisabled
        ].join(' ')
      }
      onClick={props.onAction}
      title={props.title}
      disabled={props.disabled || false}
      data-testid="BasicButton"
    >
      {props.children}
    </button>
  )
);

// Required pops
const reqProps = {
  type: 'Default',
  onAction: jest.fn(),
  title: 'title1',
}
const children = 'text1';

describe('Content', () => {
  test('It renders a BasicButton', () => {
    const { getByTestId } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    getByTestId('BasicButton');
  });
  test('It renders the text', () => {
    const { getByText } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    getByText(children);
  });
  test('It renders the title', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    expect(getByRole('button')).toHaveAttribute('title', reqProps.title);
  });
  test('It renders the proper type', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    expect(getByRole('button')).toHaveClass(reqProps.type);
  });
  test('It renders the active class', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    expect(getByRole('button'))
      .toHaveAttribute('class', expect.stringMatching(/active/i));
  });
  test('It renders the disabled class', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    expect(getByRole('button'))
      .toHaveAttribute('class', expect.stringMatching(/disabled/i));
  });
});

describe('Behaviour', () => {
  beforeEach(() => {
    reqProps.onAction.mockClear();
  });
  test('It executes the call back on user interaction', () => {
    const { getByRole } = render(
      <Button {...reqProps}>
        {children}
      </Button>
    );
    const btn = getByRole('button');
    fireEvent.click(btn);
    expect(reqProps.onAction).toHaveBeenCalledTimes(1);
  });
  test('If disabled it does not execute the callback', () => {
    const { getByRole } = render(
      <Button {...reqProps} disabled >
        {children}
      </Button>
    );
    const btn = getByRole('button');
    fireEvent.click(btn);
    expect(reqProps.onAction).not.toHaveBeenCalled();
  });
})
