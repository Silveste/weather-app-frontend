import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import MutableButton from './index';

// Mocks for internal functions
jest.mock(
  '../BasicButton',
  () => (props) => (
    <button
      className={
        [
            props.styles,
            props.stylesEnabled,
            props.stylesActive,
            props.stylesDisabled
        ].join(' ')
      }
      onClick={props.onAction}
      title={props.title}
      disabled={props.disabled || false}
      data-testid="BasicButton"
    >
      {props.children}
    </button>
  )
);

describe('Content',() => {
  test('The component has a button', () => {
    const { getByRole } = render(<MutableButton onAction={jest.fn()}/>);
    getByRole('button');
  })
});

describe('Behaviour',() => {
  test('Click on button calls function passed by onClick prop', () => {
    const onClickMock = jest.fn();
    const { getByRole } = render(<MutableButton onAction={onClickMock} />);
    const button = getByRole('button');
    fireEvent.click(button);
    expect(onClickMock).toHaveBeenCalledTimes(1);
  });
  test.each([
    'left',
    'right',
    'close',
    'burger'
  ])('Accepts mode: %s', (mode) => {
    const errorSpy = jest.spyOn(console, 'error');
    try {
      render(
        <MutableButton mode={mode} onAction={jest.fn()} />
      );
      expect(errorSpy).not.toHaveBeenCalled();
    } finally {
      errorSpy.mockRestore();
    }
  });
  test('Button can be disabled', () => {
    const onClickMock = jest.fn();
    const { getByRole } = render(
      <MutableButton disabled onAction={onClickMock} />
    );
    const button = getByRole('button');
    fireEvent.click(button);
    expect(onClickMock).toHaveBeenCalledTimes(0);
  });
});
