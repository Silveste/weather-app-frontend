import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Backdrop from './index';

describe('content',() => {
  test('Contains ane element with presentational role', () => {
    const { getByRole } = render(<Backdrop show={true} />);
    getByRole('presentation');
  });
  test('Backdrop has no content', () => {
    const { getByRole } = render(<Backdrop show={true}/>);
    const backdrop = getByRole('presentation');
    expect(backdrop).toBeEmptyDOMElement();
  })
});
describe('Behaviour', () => {
  test('Backdrop provides a prop that toggle its presence', () => {
    const { queryByRole, rerender } = render(<Backdrop show={true} />);
    expect(queryByRole('presentation')).not.toBeNull();
    rerender(<Backdrop show={false} />);
    expect(queryByRole('presentation')).toBeNull();
  });
  test('Backdrop provides onClick interface', () => {
    const clickable = jest.fn();
    const { queryByRole } = render(<Backdrop show={true} onClick={clickable}/>);
    fireEvent.click(queryByRole('presentation'));
    expect(clickable).toHaveBeenCalledTimes(1);
  })
});
