import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Modal from './index';

jest.mock(
  '../Backdrop',
  () => (props) => (<div onClick={props.onClick}>backdrop</div>)
);

// Modal props
const props = {
  show: true,
  close: jest.fn()
}
const children = (<div>A child node</div>);

describe('Content',() => {
  test('It has a close button', () => {
    const { getByRole } = render(<Modal {...props}>{children}</Modal>);
    getByRole('button');
  });
  test('It renders the children nodes', () => {
    const { getByText } = render(<Modal {...props}>{children}</Modal>);
    getByText(/child/i);
  })
});

describe('Behaviour', () => {
  beforeEach(() => {
    props.close.mockClear();
  });
  test('Click the backdrop calls the close function', () => {
    const { getByText } = render(<Modal {...props}>{children}</Modal>);
    fireEvent.click(getByText('backdrop'))
    expect(props.close).toHaveBeenCalledTimes(1);
  });
  test('Click the close button calls the close function', () => {
    const { getByRole } = render(<Modal {...props}>{children}</Modal>);
    fireEvent.click(getByRole('button'));
    expect(props.close).toHaveBeenCalledTimes(1);
  });
});
