import thunks from "./thunks";
import { getLocations, locIds } from "../__mocks__/state";

const locations = getLocations();
const locationsState = locations;
const dispatch = jest.fn();
const actions = {
  deleteLocation: jest.fn(),
  setTracker: jest.fn(),
  setOnlyCached: jest.fn(),
  setOnMap: jest.fn(),
  getWeatherAreaId: jest.fn()
};
const getState = jest.fn().mockReturnValue({ locations: locationsState });
const errorTracker = "error-123";

afterEach(() => {
  getState.mockClear();
  dispatch.mockReset();
  actions.setOnlyCached.mockReset();
  actions.setOnMap.mockReset();
  actions.deleteLocation.mockReset();
  actions.setTracker.mockReset();
  actions.getWeatherAreaId.mockReset();
});

describe("Thunk to update the query trackers", () => {
  test("It dispatches one action if payload.locationIds is a string", () => {
    thunks.updatingQueryTrackers(
      dispatch,
      getState,
      {
        locationIds: locIds[0],
        addToTracker: 1
      },
      actions,
      errorTracker
    );
    expect(dispatch).toHaveBeenCalledTimes(1);
  });
  test("It dispatches as many actions as locIds if payload.locations is an array", () => {
    thunks.updatingQueryTrackers(
      dispatch,
      getState,
      {
        locationIds: locIds,
        addToTracker: 1
      },
      actions,
      errorTracker
    );
    expect(dispatch).toHaveBeenCalledTimes(locIds.length);
  });
  test("If onlyCached = false dispatch setTracker", () => {
    const testAction = "DispatchSetTracker";
    actions.setTracker.mockReturnValue(testAction);
    const id = locIds[0];
    const testLocation = {
      ...locations[id],
      onlyCached: false
    };
    const locs = {};
    locs[id] = testLocation;
    getState.mockReturnValueOnce({ locations: locs });
    thunks.updatingQueryTrackers(
      dispatch,
      getState,
      {
        locationIds: id,
        addToTracker: 1
      },
      actions,
      errorTracker
    );
    expect(actions.setTracker).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledTimes(1);
  });
  test.each([
    [0, 1],
    [2, -1]
  ])(
    "%# - If tracker + val > 0 dispatch setTracker, tracker: %i, val: %i",
    (tracker, toAdd) => {
      const id = locIds[0];
      const testLocation = {
        ...locations[id],
        onlyCached: true,
        queriesTracker: tracker
      };
      const locs = {};
      locs[id] = testLocation;
      getState.mockReturnValueOnce({ locations: locs });
      thunks.updatingQueryTrackers(
        dispatch,
        getState,
        {
          locationIds: id,
          addToTracker: toAdd
        },
        actions,
        errorTracker
      );
      expect(actions.setTracker).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledTimes(1);
    }
  );
  test.each([
    [0, 1],
    [2, -1],
    [7, -6],
    [10, 4]
  ])(
    "%# - Calls setTracker has correct args, tracker: %i, val: %i",
    (tracker, toAdd) => {
      const testAction = "DispatchSetTracker";
      actions.setTracker.mockReturnValue(testAction);
      const id = locIds[0];
      const testLocation = {
        ...locations[id],
        onlyCached: true,
        queriesTracker: tracker
      };
      const locs = {};
      locs[id] = testLocation;
      getState.mockReturnValueOnce({ locations: locs });
      thunks.updatingQueryTrackers(
        dispatch,
        getState,
        {
          locationIds: id,
          addToTracker: toAdd
        },
        actions,
        errorTracker
      );
      expect(actions.setTracker).toHaveBeenCalledTimes(1);
      expect(actions.setTracker).toHaveBeenCalledWith(
        id,
        tracker + toAdd,
        errorTracker
      );
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith(testAction);
    }
  );
  test.each([
    [0, 0],
    [1, -1],
    [1, -4],
    [3, -5]
  ])(
    "If tracker + val <= 0 && onlyCached = true dispatch deleteLocation, tracker: %i, val: %i",
    (tracker, toAdd) => {
      const id = locIds[0];
      const testLocation = {
        ...locations[id],
        onlyCached: true,
        queriesTracker: tracker
      };
      const locs = {};
      locs[id] = testLocation;
      getState.mockReturnValueOnce({ locations: locs });
      thunks.updatingQueryTrackers(
        dispatch,
        getState,
        {
          locationIds: id,
          addToTracker: toAdd
        },
        actions,
        errorTracker
      );
      expect(actions.deleteLocation).toHaveBeenCalledTimes(1);
      expect(actions.deleteLocation).toHaveBeenCalledWith(id, errorTracker);
      expect(dispatch).toHaveBeenCalledTimes(1);
    }
  );
  test("Calls deleteLocation has correct args: id", () => {
    const testAction = "DispatchDeleteLocation";
    actions.deleteLocation.mockReturnValue(testAction);
    const id = locIds[0];
    const testLocation = {
      ...locations[id],
      onlyCached: true,
      queriesTracker: 0
    };
    const locs = {};
    locs[id] = testLocation;
    getState.mockReturnValueOnce({ locations: locs });
    thunks.updatingQueryTrackers(
      dispatch,
      getState,
      {
        locationIds: id,
        addToTracker: 0
      },
      actions,
      errorTracker
    );
    expect(actions.deleteLocation).toHaveBeenCalledTimes(1);
    expect(actions.deleteLocation).toHaveBeenCalledWith(id, errorTracker);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(testAction);
  });
});

describe("Thunk to remove location", () => {
  test("If tracker is 0 dispatch deleteLocation", () => {
    const testAction = "DispatchDeleteLocation";
    actions.deleteLocation.mockReturnValue(testAction);
    const id = locIds[0];
    const testLocation = {
      ...locations[id],
      onlyCached: true,
      queriesTracker: 0
    };
    const locs = {};
    locs[id] = testLocation;
    getState.mockReturnValueOnce({ locations: locs });
    thunks.removingLocation(dispatch, getState, id, actions, errorTracker);
    expect(actions.deleteLocation).toHaveBeenCalledTimes(1);
    expect(actions.deleteLocation).toHaveBeenCalledWith(id, errorTracker);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(testAction);
  });
  test("If tracker is > 0 dispatch setOnlyCached with val = true", () => {
    const testAction = "DispatchSetOnlyCached";
    actions.setOnlyCached.mockReturnValue(testAction);
    const id = locIds[0];
    const testLocation = {
      ...locations[id],
      onlyCached: false,
      queriesTracker: 1
    };
    const locs = {};
    locs[id] = testLocation;
    getState.mockReturnValueOnce({ locations: locs });
    thunks.removingLocation(dispatch, getState, id, actions, errorTracker);
    expect(actions.setOnlyCached).toHaveBeenCalledTimes(1);
    expect(actions.setOnlyCached).toHaveBeenCalledWith(id, true, errorTracker);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(testAction);
  });
});

describe("Thunk to toggle weather", () => {
  test.each([false, true])(
    "weatherAreaId != null, It dispatches only setOnMap with args: locId, %p",
    newIsOnMap => {
      const testAction = "DispatchSetOnMap";
      actions.setOnMap.mockReturnValue(testAction);
      const id = locIds[0];
      const testLocation = {
        ...locations[id],
        isOnMap: !newIsOnMap,
        weatherAreaId: "aaaa"
      };
      const locs = {};
      locs[id] = testLocation;
      getState.mockReturnValueOnce({ locations: locs });
      thunks.togglingWeather(
        dispatch,
        getState,
        { locationId: id, isOnMap: newIsOnMap },
        actions,
        errorTracker
      );
      expect(actions.setOnMap).toHaveBeenCalledTimes(1);
      expect(actions.setOnMap).toHaveBeenCalledWith(
        id,
        newIsOnMap,
        errorTracker
      );
      expect(actions.getWeatherAreaId).not.toHaveBeenCalled();
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith(testAction);
    }
  );
  test.each([false, true])(
    "weatherAreaId = null, it dispatches setOnMap and getWeatherAreaId with required args: locId, %p",
    newIsOnMap => {
      const testAction1 = "DispatchSetOnMap";
      const testAction2 = "DispatchGetWeatherAreaId";
      actions.setOnMap.mockReturnValue(testAction1);
      actions.getWeatherAreaId.mockReturnValue(testAction2);
      const id = locIds[0];
      const testLocation = {
        ...locations[id],
        isOnMap: !newIsOnMap,
        weatherAreaId: null
      };
      const locs = {};
      locs[id] = testLocation;
      getState.mockReturnValueOnce({ locations: locs });
      thunks.togglingWeather(
        dispatch,
        getState,
        { locationId: id, isOnMap: newIsOnMap },
        actions,
        errorTracker
      );
      expect(actions.setOnMap).toHaveBeenCalledTimes(1);
      expect(actions.setOnMap).toHaveBeenCalledWith(
        id,
        newIsOnMap,
        errorTracker
      );
      expect(actions.getWeatherAreaId).toHaveBeenCalledTimes(1);
      expect(actions.getWeatherAreaId).toHaveBeenCalledWith(
        id,
        testLocation.geometry,
        errorTracker
      );
      expect(dispatch).toHaveBeenCalledTimes(2);
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction1], [testAction2]])
      );
    }
  );
});

describe("Thunk to use location", () => {
  test("It dispatches setOnLyCached with args: locationId, false", () => {
    const testAction = "DispatchSetOnlyCached";
    actions.setOnlyCached.mockReturnValue(testAction);
    const id = locIds[0];
    const testLocation = {
      ...locations[id],
      onlyCached: true,
      weatherAreaId: "aaaa"
    };
    const locs = {};
    locs[id] = testLocation;
    getState.mockReturnValueOnce({ locations: locs });
    thunks.usingLocation(dispatch, getState, id, actions, errorTracker);
    expect(actions.setOnlyCached).toHaveBeenCalledTimes(1);
    expect(actions.setOnlyCached).toHaveBeenCalledWith(id, false, errorTracker);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(testAction);
  });
});
