import locations from "./index";
import weatherAreas from "../weatherAreas";
import thunks from "./thunks";
import actionTypes from "./actionTypes";

// mocks & spies
const locIds = ["loc1", "loc2"];
const errorTracker = "error-123";
const dispatch = jest.fn();
const getState = jest.fn();

describe("reducer", () => {
  test("It has reducer property", () => {
    expect(locations.reducer).toBeFunction();
  });
});
describe("actions", () => {
  test("It has actions property", () => {
    expect(locations.actions).toBeObject();
  });
  describe("Action Creators that return thunks", () => {
    describe("updateQueriesTracker", () => {
      beforeAll(() => {
        jest
          .spyOn(thunks, "updatingQueryTrackers")
          .mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.updatingQueryTrackers.mockClear();
      });
      afterAll(() => {
        thunks.updatingQueryTrackers.mockRestore();
      });
      test("Actions has updateQueryTrackers", () => {
        expect(locations.actions.updateQueryTrackers).toBeFunction();
      });
      test("It returns a function", () => {
        expect(locations.actions.updateQueryTrackers()).toBeFunction();
      });
      test("returning function calls updatingQueriesTracker thunk", () => {
        locations.actions.updateQueryTrackers()();
        expect(thunks.updatingQueryTrackers).toHaveBeenCalledTimes(1);
      });
      test("updatingQueriesTracker is called with: dispatch, getState, payload, actions, errorTracker", () => {
        locations.actions.updateQueryTrackers(
          locIds,
          1,
          errorTracker
        )(dispatch, getState);
        expect(thunks.updatingQueryTrackers).toHaveBeenCalledWith(
          dispatch,
          getState,
          expect.anything(),
          expect.anything(),
          errorTracker
        );
      });
      test("payload argument has: locationIds, addToTracker properties", () => {
        locations.actions.updateQueryTrackers(
          locIds,
          1,
          errorTracker
        )(dispatch, getState);
        expect(thunks.updatingQueryTrackers).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          { locationIds: locIds, addToTracker: 1 },
          expect.anything(),
          expect.anything()
        );
      });
      test("actions argument has: asetTracker, deleteLocation actions creators", () => {
        const { deleteLocation, setTracker } = locations.actions;
        locations.actions.updateQueryTrackers(
          locIds,
          1,
          errorTracker
        )(dispatch, getState);
        expect(thunks.updatingQueryTrackers).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          {
            deleteLocation,
            setTracker
          },
          expect.anything()
        );
      });
    });
    describe("removeLocation", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "removingLocation").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.removingLocation.mockClear();
      });
      afterAll(() => {
        thunks.removingLocation.mockRestore();
      });
      test("Actions has removeLocation", () => {
        expect(locations.actions.removeLocation).toBeFunction();
      });
      test("It returns a function", () => {
        expect(locations.actions.removeLocation()).toBeFunction();
      });
      test("returning function calls removingLocations thunk", () => {
        locations.actions.removeLocation()();
        expect(thunks.removingLocation).toHaveBeenCalledTimes(1);
      });
      test("removingLocations is called with: dispatch, getState, locationId, actions", () => {
        locations.actions.removeLocation(locIds[0], errorTracker)(
          dispatch,
          getState
        );
        expect(thunks.removingLocation).toHaveBeenCalledWith(
          dispatch,
          getState,
          locIds[0],
          expect.anything(),
          errorTracker
        );
      });
      test("actions argument has: setOnlyCached, deleteLocation actions creators", () => {
        const { deleteLocation, setOnlyCached } = locations.actions;
        locations.actions.removeLocation(locIds[0], errorTracker)(
          dispatch,
          getState
        );
        expect(thunks.removingLocation).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          {
            deleteLocation,
            setOnlyCached
          },
          expect.anything()
        );
      });
    });
    describe("useLocation", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "usingLocation").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.usingLocation.mockClear();
      });
      afterAll(() => {
        thunks.usingLocation.mockRestore();
      });
      test("Actions has useLocation", () => {
        expect(locations.actions.useLocation).toBeFunction();
      });
      test("It returns a function", () => {
        expect(locations.actions.useLocation()).toBeFunction();
      });
      test("returning function calls usingLocation thunk", () => {
        locations.actions.useLocation()();
        expect(thunks.usingLocation).toHaveBeenCalledTimes(1);
      });
      test("useLocation is called with: dispatch, getState, locationId, actions", () => {
        locations.actions.useLocation(locIds[0], errorTracker)(
          dispatch,
          getState
        );
        expect(thunks.usingLocation).toHaveBeenCalledWith(
          dispatch,
          getState,
          locIds[0],
          expect.anything(),
          errorTracker
        );
      });
      test("actions argument has: setOnlyCached, getWeatherAreaId actions creators", () => {
        const { setOnlyCached } = locations.actions;
        locations.actions.useLocation(locIds[0], errorTracker)(
          dispatch,
          getState
        );
        expect(thunks.usingLocation).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          { setOnlyCached },
          expect.anything()
        );
      });
    });
  });

  describe("toggleWeather", () => {
    beforeAll(() => {
      jest.spyOn(thunks, "togglingWeather").mockImplementation(() => {});
    });
    afterEach(() => {
      thunks.togglingWeather.mockClear();
    });
    afterAll(() => {
      thunks.togglingWeather.mockRestore();
    });
    test("Actions has toggleWeather", () => {
      expect(locations.actions.toggleWeather).toBeFunction();
    });
    test("It returns a function", () => {
      expect(locations.actions.toggleWeather()).toBeFunction();
    });
    test("togglingWeather is called with: dispatch, getState, payload, actions", () => {
      locations.actions.toggleWeather(
        locIds[0],
        true,
        errorTracker
      )(dispatch, getState);
      expect(thunks.togglingWeather).toHaveBeenCalledWith(
        dispatch,
        getState,
        expect.anything(),
        expect.anything(),
        errorTracker
      );
    });
    test("payload argument has: locationId, isOnMap properties", () => {
      locations.actions.toggleWeather(
        locIds[0],
        true,
        errorTracker
      )(dispatch, getState);
      expect(thunks.togglingWeather).toHaveBeenCalledWith(
        expect.anything(),
        expect.anything(),
        { locationId: locIds[0], isOnMap: true },
        expect.anything(),
        expect.anything()
      );
    });
    test("actions argument has: setOnlyCached, getWeatherAreaId actions creators", () => {
      const { setOnMap } = locations.actions;
      locations.actions.toggleWeather(
        locIds[0],
        true,
        errorTracker
      )(dispatch, getState);
      expect(thunks.togglingWeather).toHaveBeenCalledWith(
        expect.anything(),
        expect.anything(),
        expect.anything(),
        {
          getWeatherAreaId: weatherAreas.actions.addToLocation,
          setOnMap
        },
        expect.anything()
      );
    });
  });

  describe("Action Creators that return actions", () => {
    describe("setIsSaved", () => {
      test("It is a property of locations.actions", () => {
        expect(locations.actions.setIsSaved).toBeFunction();
      });
      test("It returns an object", () => {
        expect(locations.actions.setIsSaved("locId", true)).toBeObject();
      });
      test("Returning abject has type: actionTypes.SET_IS_SAVED", () => {
        const action = locations.actions.setIsSaved("locId", true);
        expect(action.type).toBe(actionTypes.SET_IS_SAVED);
      });
      test("Returning object has payload: { locationId, isSaved }", () => {
        const locId = "locId";
        const action = locations.actions.setIsSaved(locId, true);
        expect(action.payload).toEqual({ locationId: locId, isSaved: true });
      });
    });
    describe("setOnMap", () => {
      test("It is a property of locations.actions", () => {
        expect(locations.actions.setOnMap).toBeFunction();
      });
      test("It returns an object", () => {
        expect(locations.actions.setOnMap("locId", true)).toBeObject();
      });
      test("Returning abject has type: actionTypes.SET_ON_MAP", () => {
        const action = locations.actions.setOnMap("locId", true);
        expect(action.type).toBe(actionTypes.SET_ON_MAP);
      });
      test("Returning object has payload: { locationId, isOnMap }", () => {
        const locId = "locId";
        const action = locations.actions.setOnMap(locId, true);
        expect(action.payload).toEqual({ locationId: locId, isOnMap: true });
      });
    });
    describe("add", () => {
      const locs = [{ id: "locId1" }, { id: "locId2" }];
      test("It is a property of locations.actions", () => {
        expect(locations.actions.add).toBeFunction();
      });
      test("It returns an object", () => {
        expect(locations.actions.add(locs)).toBeObject();
      });
      test("Returning abject has type: actionTypes.ADD", () => {
        const action = locations.actions.add(locs);
        expect(action.type).toBe(actionTypes.ADD);
      });
      test("Returning object has payload: [location]", () => {
        const action = locations.actions.add(locs);
        expect(action.payload).toEqual(locs);
      });
    });
    describe("setTracker", () => {
      test("It is a property of locations.actions", () => {
        expect(locations.actions.setTracker).toBeFunction();
      });
      test("It returns an object", () => {
        expect(locations.actions.setTracker("locId", 2)).toBeObject();
      });
      test("Returning abject has type: actionTypes.SET_QUERY_TRACKER", () => {
        const action = locations.actions.setTracker("locId", 2);
        expect(action.type).toBe(actionTypes.SET_QUERY_TRACKER);
      });
      test("Returning object has payload: { locationId, queriesTracker }", () => {
        const locId = "locId";
        const action = locations.actions.setTracker(locId, 2);
        expect(action.payload).toEqual({
          locationId: locId,
          queriesTracker: 2
        });
      });
    });
    describe("deleteLocation", () => {
      test("It is a property of locations.actions", () => {
        expect(locations.actions.deleteLocation).toBeFunction();
      });
      test("It returns an object", () => {
        expect(locations.actions.deleteLocation("locId")).toBeObject();
      });
      test("Returning abject has type: actionTypes.DELETE_LOCATION", () => {
        const action = locations.actions.deleteLocation("locId");
        expect(action.type).toBe(actionTypes.DELETE_LOCATION);
      });
      test("Returning object has payload: locationId", () => {
        const action = locations.actions.deleteLocation("locId");
        expect(action.payload).toBe("locId");
      });
    });
    describe("setOnlyCached", () => {
      test("It is a property of locations.actions", () => {
        expect(locations.actions.setOnlyCached).toBeFunction();
      });
      test("It returns an object", () => {
        expect(locations.actions.setOnlyCached("locId", true)).toBeObject();
      });
      test("Returning abject has type: actionTypes.SET_ONLY_CACHED", () => {
        const action = locations.actions.setOnlyCached("locId", true);
        expect(action.type).toBe(actionTypes.SET_ONLY_CACHED);
      });
      test("Returning object has payload: { locationId, onlyCached }", () => {
        const action = locations.actions.setOnlyCached("locId", true);
        expect(action.payload).toEqual({
          locationId: "locId",
          onlyCached: true
        });
      });
    });
    describe("setWeatherArea", () => {
      test("It is a property of locations.actions", () => {
        expect(locations.actions.setWeatherArea).toBeFunction();
      });
      test("It returns an object", () => {
        expect(locations.actions.setWeatherArea("locId", "aaa")).toBeObject();
      });
      test("Returning abject has type: actionTypes.SET_WEATHER_AREA", () => {
        const action = locations.actions.setWeatherArea("locId", "aaa");
        expect(action.type).toBe(actionTypes.SET_WEATHER_AREA);
      });
      test("Returning object has payload: { locationId, weatherAreaId }", () => {
        const action = locations.actions.setWeatherArea("locId", "aaa");
        expect(action.payload).toEqual({
          locationId: "locId",
          weatherAreaId: "aaa"
        });
      });
    });
  });
});
