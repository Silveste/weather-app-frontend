import reducer from "./reducer";
import { getLocation } from "../__mocks__/state";

test("Exports reducer as a function", () => {
  expect(reducer).toBeFunction();
});
describe("Initial state", () => {
  let state;
  beforeEach(() => {
    state = reducer(undefined, { type: null });
  });
  test("It is an empty object", () => {
    expect(state).toBeObject();
    expect(state).toBeEmpty();
  });
});
describe("ADD_LOCATIONS", () => {
  test("Action.payload can be a single Location object or Array of locations", () => {
    const state = reducer(undefined, {
      type: "ADD_LOCATIONS",
      payload: [getLocation({ id: "loc1" }), getLocation({ id: "loc2" })]
    });
    expect(state).toContainKeys(["loc1", "loc2"]);
    const state2 = reducer(undefined, {
      type: "ADD_LOCATIONS",
      payload: getLocation({ id: "loc3" })
    });
    expect(state2).toContainKey("loc3");
  });
  test("It only adds new locations to the state and discard repeated ones", () => {
    let state = {};
    state = reducer(state, {
      type: "ADD_LOCATIONS",
      payload: [
        getLocation({ id: "loc1", name: "loc1" }),
        getLocation({ id: "loc2", name: "loc2" })
      ]
    });
    state = reducer(state, {
      type: "ADD_LOCATIONS",
      payload: getLocation({ id: "loc1", name: "loc3" })
    });
    expect(state).toContainAllKeys(["loc1", "loc2"]);
    expect(state.loc2.name).toBe("loc2");
    expect(state.loc1.name).toBe("loc1");
  });
});
describe("SET_LOCATION_SAVED", () => {
  const loc1 = getLocation({ isSaved: true });
  const loc2 = getLocation({ isSaved: false });
  const loc3 = getLocation({ isSaved: true });
  const loc4 = getLocation({ isSaved: false });
  const initialState = { loc1, loc2, loc3, loc4 };
  test("Set isSave=payload.isSaved of loc id equals to payload.locationId", () => {
    let state;
    state = reducer(initialState, {
      type: "SET_LOCATION_SAVED",
      payload: { locationId: "loc1", isSaved: false }
    });
    expect(state.loc1.isSaved).toBe(false);
    state = reducer(initialState, {
      type: "SET_LOCATION_SAVED",
      payload: { locationId: "loc2", isSaved: true }
    });
    expect(state.loc2.isSaved).toBe(true);
    state = reducer(initialState, {
      type: "SET_LOCATION_SAVED",
      payload: { locationId: "loc3", isSaved: true }
    });
    expect(state.loc3.isSaved).toBe(true);
    state = reducer(initialState, {
      type: "SET_LOCATION_SAVED",
      payload: { locationId: "loc4", isSaved: false }
    });
    expect(state.loc4.isSaved).toBe(false);
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const state = reducer(initialState, {
      type: "SET_LOCATION_SAVED",
      payload: { locationId: "loc1", isSaved: false }
    });
    expect(state).not.toBe(initialState);
    expect(state.loc1).not.toBe(initialState.loc1);
    expect(state.loc2).toBe(initialState.loc2);
  });
});
describe("SET_LOCATION_ON_MAP", () => {
  const loc1 = getLocation({ isOnMap: true });
  const loc2 = getLocation({ isOnMap: false });
  const loc3 = getLocation({ isOnMap: true });
  const loc4 = getLocation({ isOnMap: false });
  const initialState = { loc1, loc2, loc3, loc4 };
  test("Set isOnMap=payload.isOnMap of loc id equals to payload.locationId", () => {
    let state;
    state = reducer(initialState, {
      type: "SET_LOCATION_ON_MAP",
      payload: { locationId: "loc1", isOnMap: false }
    });
    expect(state.loc1.isOnMap).toBe(false);
    state = reducer(initialState, {
      type: "SET_LOCATION_ON_MAP",
      payload: { locationId: "loc2", isOnMap: true }
    });
    expect(state.loc2.isOnMap).toBe(true);
    state = reducer(initialState, {
      type: "SET_LOCATION_ON_MAP",
      payload: { locationId: "loc3", isOnMap: true }
    });
    expect(state.loc3.isOnMap).toBe(true);
    state = reducer(initialState, {
      type: "SET_LOCATION_ON_MAP",
      payload: { locationId: "loc4", isOnMap: false }
    });
    expect(state.loc4.isOnMap).toBe(false);
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const state = reducer(initialState, {
      type: "SET_LOCATION_ON_MAP",
      payload: { locationId: "loc1", isOnMap: false }
    });
    expect(state).not.toBe(initialState);
    expect(state.loc1).not.toBe(initialState.loc1);
    expect(state.loc2).toBe(initialState.loc2);
  });
});
describe("SET_QUERY_TRACKER", () => {
  const loc1 = getLocation({ queriesTracker: 0 });
  const loc2 = getLocation({ queriesTracker: 1 });
  const initialState = { loc1, loc2 };
  test("Set queriesTracker=payload.queriesTracker of loc id equals to payload.locationId", () => {
    let state;
    state = reducer(initialState, {
      type: "SET_QUERY_TRACKER",
      payload: { locationId: "loc1", queriesTracker: 2 }
    });
    expect(state.loc1.queriesTracker).toBe(2);
    state = reducer(initialState, {
      type: "SET_QUERY_TRACKER",
      payload: { locationId: "loc2", queriesTracker: 0 }
    });
    expect(state.loc2.queriesTracker).toBe(0);
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const state = reducer(initialState, {
      type: "SET_QUERY_TRACKER",
      payload: { locationId: "loc1", queriesTracker: 2 }
    });
    expect(state).not.toBe(initialState);
    expect(state.loc1).not.toBe(initialState.loc1);
    expect(state.loc2).toBe(initialState.loc2);
  });
});
describe("SET_ONLY_CACHED", () => {
  const loc1 = getLocation({ onlyCached: true });
  const loc2 = getLocation({ onlyCached: false });
  const loc3 = getLocation({ onlyCached: true });
  const loc4 = getLocation({ onlyCached: false });
  const initialState = { loc1, loc2, loc3, loc4 };
  test("Set onlyCached=payload.onlyCached of loc id equals to payload.locationId", () => {
    let state;
    state = reducer(initialState, {
      type: "SET_ONLY_CACHED",
      payload: { locationId: "loc1", onlyCached: false }
    });
    expect(state.loc1.onlyCached).toBe(false);
    state = reducer(initialState, {
      type: "SET_ONLY_CACHED",
      payload: { locationId: "loc2", onlyCached: true }
    });
    expect(state.loc2.onlyCached).toBe(true);
    state = reducer(initialState, {
      type: "SET_ONLY_CACHED",
      payload: { locationId: "loc3", onlyCached: true }
    });
    expect(state.loc3.onlyCached).toBe(true);
    state = reducer(initialState, {
      type: "SET_ONLY_CACHED",
      payload: { locationId: "loc4", onlyCached: false }
    });
    expect(state.loc4.onlyCached).toBe(false);
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const state = reducer(initialState, {
      type: "SET_ONLY_CACHED",
      payload: { locationId: "loc1", onlyCached: false }
    });
    expect(state).not.toBe(initialState);
    expect(state.loc1).not.toBe(initialState.loc1);
    expect(state.loc2).toBe(initialState.loc2);
  });
});
describe("SET_LOCATION_WEATHER_AREA", () => {
  const loc1 = getLocation({ weatherAreaId: null });
  const loc2 = getLocation({ weatherAreaId: "aaaaa" });
  const initialState = { loc1, loc2 };
  test("Set weatherArea=payload.weatherArea of loc id equals to payload.locationId", () => {
    let state;
    state = reducer(initialState, {
      type: "SET_LOCATION_WEATHER_AREA",
      payload: { locationId: "loc1", weatherAreaId: "test1" }
    });
    expect(state.loc1.weatherAreaId).toBe("test1");
    state = reducer(initialState, {
      type: "SET_LOCATION_WEATHER_AREA",
      payload: { locationId: "loc2", weatherAreaId: "test2" }
    });
    expect(state.loc2.weatherAreaId).toBe("test2");
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const state = reducer(initialState, {
      type: "SET_LOCATION_WEATHER_AREA",
      payload: { locationId: "loc1", weatherAreaId: "test1" }
    });
    expect(state).not.toBe(initialState);
    expect(state.loc1).not.toBe(initialState.loc1);
    expect(state.loc2).toBe(initialState.loc2);
  });
});
describe("DELETE_LOCATION", () => {
  const loc1 = getLocation();
  const loc2 = getLocation();
  const initialState = { loc1, loc2 };
  test("Remove location property from state", () => {
    let state;
    state = reducer(initialState, {
      type: "DELETE_LOCATION",
      payload: "loc1"
    });
    expect(state).not.toContainKey("loc1");
    expect(state).toContainKey("loc2");
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const state = reducer(initialState, {
      type: "DELETE_LOCATION",
      payload: "loc1"
    });
    expect(state).not.toBe(initialState);
    expect(state.loc2).toBe(initialState.loc2);
  });
});
