const actionTypes = {
  ADD_NEW: "ADD_NEW_WEATHER_AREA",
  SET_LOADING: "SET_WEATHER_AREA_LOADING",
  UPDATE_WEATHER: "UPDATE_WEATHER"
};

export default actionTypes;
