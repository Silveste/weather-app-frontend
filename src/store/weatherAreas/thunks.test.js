import { getCentroid } from "../../utils/geometryHelpers";
import thunks from "./thunks";
import backend from "../../backend";
import {
  getWeatherAreas,
  getLocations,
  waIds,
  getWeatherArea,
  locIds,
  generateRandomPolygonAround,
  generateRandomPolygonNotAround
} from "../__mocks__/state";
jest.mock("../../backend");

const weatherAreas = getWeatherAreas();
const locations = getLocations();
const errorTracker = "error-123";
const dispatch = jest.fn();
const actions = {
  updateWeather: jest.fn(),
  setLoading: jest.fn(),
  setLocationWeatherArea: jest.fn(),
  addNew: jest.fn(),
  addError: jest.fn()
};
const getState = jest.fn().mockReturnValue({ locations, weatherAreas });
backend.getWeatherByCoords.mockResolvedValue({});

afterEach(() => {
  getState.mockClear();
  dispatch.mockReset();
  actions.updateWeather.mockReset();
  actions.setLoading.mockReset();
  actions.setLocationWeatherArea.mockReset();
  actions.addNew.mockReset();
  actions.addError.mockReset();
  backend.getWeatherByCoords.mockClear();
});

describe("Thunk to update the weather area", () => {
  test("It calls backend.getWeatherByCoords", () => {
    thunks.updatingWeatherArea(
      dispatch,
      getState,
      waIds[0],
      actions,
      errorTracker
    );
    expect(backend.getWeatherByCoords).toHaveBeenCalled();
  });
  test("backend.getWeatherByCoords args: centroid of WA (lon,lat)", () => {
    const coords = getCentroid(weatherAreas[waIds[0]].geometry);
    thunks.updatingWeatherArea(
      dispatch,
      getState,
      waIds[0],
      actions,
      errorTracker
    );
    expect(backend.getWeatherByCoords).toHaveBeenCalledWith(
      coords[0],
      coords[1]
    );
  });
  test("Dispatches setLoading to set loading to true", () => {
    const testAction = "DispatchSetLoading";
    actions.setLoading.mockReturnValue(testAction);
    thunks.updatingWeatherArea(
      dispatch,
      getState,
      waIds[0],
      actions,
      errorTracker
    );
    expect(actions.setLoading).toHaveBeenCalledTimes(1);
    expect(actions.setLoading).toHaveBeenCalledWith(waIds[0], true);
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch.mock.calls).toEqual(expect.arrayContaining([[testAction]]));
  });
  describe("On backend.getWeatherByCoords promise fulfilled", () => {
    const waTestId = "waTestId1";
    let getState, updatedWA;
    beforeEach(() => {
      var oldDate = new Date();
      oldDate.setDate(oldDate.getDate() - 5);
      const currentWA = getWeatherArea({
        loading: false,
        updatedAt: oldDate
      });
      updatedWA = getWeatherArea({
        id: waTestId,
        loading: false,
        updatedAt: new Date(),
        geometry: currentWA.geometry
      });
      const weatherAreas = {};
      weatherAreas[waTestId] = currentWA;
      getState = jest.fn().mockReturnValueOnce({
        locations,
        weatherAreas
      });
    });
    test("Dispatches updateWeather with weatherArea and errorTracker", async () => {
      const testAction = "DispatchUpdateWeather";
      actions.updateWeather.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockResolvedValueOnce(updatedWA);
      await thunks.updatingWeatherArea(
        dispatch,
        getState,
        waTestId,
        actions,
        errorTracker
      );
      expect(actions.updateWeather).toHaveBeenCalledTimes(1);
      expect(actions.updateWeather).toHaveBeenCalledWith(
        updatedWA,
        errorTracker
      );
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
    test("Dispatches setLoading to set loading to false", async () => {
      const testAction = "DispatchSetLoading";
      actions.setLoading.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockResolvedValueOnce(updatedWA);
      await thunks.updatingWeatherArea(
        dispatch,
        getState,
        waTestId,
        actions,
        errorTracker
      );
      expect(actions.setLoading).toHaveBeenNthCalledWith(2, waTestId, false);
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
  });
  describe("On backend.getWeatherByCoords promise rejected", () => {
    test("Dispatch addError(error)", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.updatingWeatherArea(
        dispatch,
        getState,
        waIds[0],
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
    test("error.data is equal to error.response.data", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      const error = { response: { data: "Error data" } };
      backend.getWeatherByCoords.mockRejectedValueOnce(error);
      await thunks.updatingWeatherArea(
        dispatch,
        getState,
        waIds[0],
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(actions.addError).toHaveBeenCalledWith(
        expect.objectContaining({ data: error.response.data })
      );
    });
    test("error.type = 'network'", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.updatingWeatherArea(
        dispatch,
        getState,
        waIds[0],
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(actions.addError).toHaveBeenCalledWith(
        expect.objectContaining({ type: "network" })
      );
    });
    test("error.message is not empty string", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.updatingWeatherArea(
        dispatch,
        getState,
        waIds[0],
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(actions.addError).toHaveBeenCalledWith(
        expect.objectContaining({ message: expect.stringMatching(/\S+/) })
      );
    });
    test("error.id = errorTracker", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.updatingWeatherArea(
        dispatch,
        getState,
        waIds[0],
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(actions.addError).toHaveBeenCalledWith(
        expect.objectContaining({ id: errorTracker })
      );
    });
    test("Dispatches setLoading to set loading to false", async () => {
      const testAction = "DispatchSetLoading";
      actions.setLoading.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.updatingWeatherArea(
        dispatch,
        getState,
        waIds[0],
        actions,
        errorTracker
      );
      expect(actions.setLoading).toHaveBeenNthCalledWith(2, waIds[0], false);
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
  });
});

describe("Thunk to get the weather area for a specific location", () => {
  let testLocId,
    locationCentroid,
    waIncludesLocationId,
    waIncludesLocation,
    wasWithoutLoc;
  beforeAll(() => {
    testLocId = locIds[0];
    locationCentroid = getCentroid(locations[testLocId].geometry);
    waIncludesLocationId = "testWa1";
    waIncludesLocation = {
      testWa1: getWeatherArea({
        geometry: generateRandomPolygonAround(...locationCentroid)
      })
    };
    wasWithoutLoc = waIds.reduce((acc, waId) => {
      acc[waId] = getWeatherArea({
        geometry: generateRandomPolygonNotAround(...locationCentroid)
      });
      return acc;
    }, {});
  });
  test("If state has WA that includes location, only dispatch setLocationWeatherArea", () => {
    const testAction = "dispatchSetLocationWeatherArea";
    actions.setLocationWeatherArea.mockReturnValue(testAction);
    const testweatherAreas = { ...wasWithoutLoc, ...waIncludesLocation };
    const getState = jest.fn().mockReturnValueOnce({
      locations,
      weatherAreas: testweatherAreas
    });
    const payload = {
      locationId: testLocId,
      geometry: locations[testLocId].geometry
    };
    thunks.addingToLocation(dispatch, getState, payload, actions, errorTracker);
    expect(backend.getWeatherByCoords).not.toHaveBeenCalled();
    expect(actions.setLocationWeatherArea).toHaveBeenCalledTimes(1);
    expect(actions.setLocationWeatherArea).toHaveBeenCalledWith(
      testLocId,
      waIncludesLocationId,
      errorTracker
    );
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch.mock.calls).toEqual(expect.arrayContaining([[testAction]]));
  });
  test("If no WA in state includes location. call backend.getWeatherByCoords", () => {
    const getState = jest.fn().mockReturnValueOnce({
      locations,
      weatherAreas: wasWithoutLoc
    });
    const payload = {
      locationId: testLocId,
      geometry: locations[testLocId].geometry
    };
    thunks.addingToLocation(dispatch, getState, payload, actions, errorTracker);
    expect(backend.getWeatherByCoords).toHaveBeenCalled();
  });
  test("backend.getWeatherByCoords is called with geometry location centroid", () => {
    const getState = jest.fn().mockReturnValueOnce({
      locations,
      weatherAreas: wasWithoutLoc
    });
    const payload = {
      locationId: testLocId,
      geometry: locations[testLocId].geometry
    };

    thunks.addingToLocation(dispatch, getState, payload, actions, errorTracker);
    expect(backend.getWeatherByCoords).toHaveBeenCalledWith(
      ...locationCentroid
    );
  });
  describe("On backend.getWeatherByCoords promise fulfilled", () => {
    let getState, payload, waFromBackend;
    beforeEach(() => {
      waFromBackend = { ...waIncludesLocation[waIncludesLocationId] };
      waFromBackend.id = waIncludesLocationId;
      backend.getWeatherByCoords.mockResolvedValueOnce(waFromBackend);
      getState = jest.fn().mockReturnValueOnce({
        locations,
        weatherAreas: wasWithoutLoc
      });
      payload = {
        locationId: testLocId,
        geometry: locations[testLocId].geometry
      };
    });
    test("Dispatch addNew", async () => {
      const testAction = "dispatchAddNew";
      actions.addNew.mockReturnValue(testAction);
      await thunks.addingToLocation(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.addNew).toHaveBeenCalledTimes(1);
      expect(actions.addNew).toHaveBeenCalledWith(waFromBackend, errorTracker);
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
    test("Dispatch setLocationWeatherArea", async () => {
      const testAction = "dispatchSetLocationWeatherArea";
      actions.setLocationWeatherArea.mockReturnValue(testAction);
      await thunks.addingToLocation(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.setLocationWeatherArea).toHaveBeenCalledTimes(1);
      expect(actions.setLocationWeatherArea).toHaveBeenCalledWith(
        testLocId,
        waIncludesLocationId,
        errorTracker
      );
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
  });
  describe("On backend.getWeatherByCoords promise rejected", () => {
    let getState, payload;
    beforeEach(() => {
      getState = jest.fn().mockReturnValueOnce({
        locations,
        weatherAreas: wasWithoutLoc
      });
      payload = {
        locationId: testLocId,
        geometry: locations[testLocId].geometry
      };
    });
    test("Dispatch addError(error)", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.addingToLocation(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
    test("error.data is equal to error.response.data", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      const error = { response: { data: "Error data" } };
      backend.getWeatherByCoords.mockRejectedValueOnce(error);
      await thunks.addingToLocation(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(actions.addError).toHaveBeenCalledWith(
        expect.objectContaining({ data: error.response.data })
      );
    });
    test("error.type = 'network'", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.addingToLocation(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(actions.addError).toHaveBeenCalledWith(
        expect.objectContaining({ type: "network" })
      );
    });
    test("error.message is not empty string", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.addingToLocation(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(actions.addError).toHaveBeenCalledWith(
        expect.objectContaining({ message: expect.stringMatching(/\S+/) })
      );
    });
    test("error.id = errorTracker", async () => {
      const testAction = "DispatchAddError";
      actions.addError.mockReturnValue(testAction);
      backend.getWeatherByCoords.mockRejectedValueOnce(
        new Error("Async error")
      );
      await thunks.addingToLocation(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.addError).toHaveBeenCalledTimes(1);
      expect(actions.addError).toHaveBeenCalledWith(
        expect.objectContaining({ id: errorTracker })
      );
    });
  });
});
