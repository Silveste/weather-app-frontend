import reducer from "./reducer";
import { getWeatherArea } from "../__mocks__/state";

test("Exports reducer as a function", () => {
  expect(reducer).toBeFunction();
});
describe("Initial state", () => {
  let state;
  beforeEach(() => {
    state = reducer(undefined, { type: null });
  });
  test("It is an empty object", () => {
    expect(state).toBeObject();
    expect(state).toBeEmpty();
  });
});
describe("ADD_NEW_WEATHER_AREA", () => {
  test("Adds weatherArea to the state", () => {
    const id = "wa1";
    const newWA = getWeatherArea({ id });
    const state = reducer(undefined, {
      type: "ADD_NEW_WEATHER_AREA",
      payload: newWA
    });
    expect(state).toContainKey(id);
    expect(state.wa1.geometry).toEqual(newWA.geometry);
  });
  test("If weatherArea exists, do nothing", () => {
    const waId = "wa1";
    const newWA = getWeatherArea({ id: waId });
    const { id, ...initialWA } = newWA;
    const initialState = { wa1: initialWA };
    const geometry = "it shouldn't be added";
    const newWA2 = getWeatherArea({ id: waId, geometry });
    const state = reducer(initialState, {
      type: "ADD_NEW_WEATHER_AREA",
      payload: newWA2
    });
    expect(state).toContainKey(id);
    expect(state.wa1.geometry).not.toEqual(newWA2.geometry);
    expect(state.wa1.geometry).toEqual(newWA.geometry);
  });
});
describe("SET_WEATHER_AREA_LOADING", () => {
  const wa1 = getWeatherArea({ isLoading: true });
  const wa2 = getWeatherArea({ isLoading: false });
  const wa3 = getWeatherArea({ isLoading: true });
  const wa4 = getWeatherArea({ isLoading: false });
  const initialState = { wa1, wa2, wa3, wa4 };
  test("Set isLoading=payload.isLoading of WA id equals to payload.weatherAreaId", () => {
    let state;
    state = reducer(initialState, {
      type: "SET_WEATHER_AREA_LOADING",
      payload: { weatherAreaId: "wa1", isLoading: false }
    });
    expect(state.wa1.isLoading).toBe(false);
    state = reducer(initialState, {
      type: "SET_WEATHER_AREA_LOADING",
      payload: { weatherAreaId: "wa2", isLoading: true }
    });
    expect(state.wa2.isLoading).toBe(true);
    state = reducer(initialState, {
      type: "SET_WEATHER_AREA_LOADING",
      payload: { weatherAreaId: "wa3", isLoading: true }
    });
    expect(state.wa3.isLoading).toBe(true);
    state = reducer(initialState, {
      type: "SET_WEATHER_AREA_LOADING",
      payload: { weatherAreaId: "wa4", isLoading: false }
    });
    expect(state.wa4.isLoading).toBe(false);
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const state = reducer(initialState, {
      type: "SET_WEATHER_AREA_LOADING",
      payload: { weatherAreaId: "wa1", isLoading: false }
    });
    expect(state).not.toBe(initialState);
    expect(state.wa1).not.toBe(initialState.wa1);
    expect(state.wa2).toBe(initialState.wa2);
  });
});
describe("UPDATE_WEATHER", () => {
  let spyDate, wa1, wa2, wa3, mockCurrentDate, oneMSBeforeCurrent;
  beforeAll(() => {
    const pastDate = new Date();
    pastDate.setDate(pastDate.getDate() - 5);
    wa1 = getWeatherArea({ updatedAt: pastDate });
    mockCurrentDate = new Date();
    oneMSBeforeCurrent = new Date(mockCurrentDate.valueOf() - 1);
    spyDate = jest
      .spyOn(global, "Date")
      .mockImplementation(() => mockCurrentDate);
    wa2 = getWeatherArea({ updatedAt: oneMSBeforeCurrent });
    wa3 = getWeatherArea({ updatedAt: null });
  });
  afterEach(() => {
    spyDate.mockClear();
  });
  afterAll(() => {
    spyDate.mockRestore();
  });
  test("Adds days to weatherArea", () => {
    const initialState = { wa1: { ...wa1, days: ["Non updated days"] } };
    const updatedDays = ["updatedDays"];
    const state = reducer(initialState, {
      type: "UPDATE_WEATHER",
      payload: {
        ...wa1,
        id: "wa1",
        updatedAt: mockCurrentDate,
        days: updatedDays
      }
    });
    expect(state.wa1.days).toEqual(updatedDays);
  });
  test("Updates updatedAt property with new weatherArea value or new Date", () => {
    const initialState = { wa1 };
    const state = reducer(initialState, {
      type: "UPDATE_WEATHER",
      payload: { ...wa2, id: "wa1" }
    });
    expect(state.wa1.updatedAt).toEqual(oneMSBeforeCurrent);
    expect(spyDate).not.toHaveBeenCalled(); //wa2 has updatedAt;
    const state2 = reducer(initialState, {
      type: "UPDATE_WEATHER",
      payload: { ...wa3, id: "wa1" }
    });
    expect(state2.wa1.updatedAt).toEqual(mockCurrentDate);
    expect(spyDate).toHaveBeenCalledTimes(1); //wa1 doesn't have updatedAt;
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const initialState = { wa1, wa2 };
    const state = reducer(initialState, {
      type: "UPDATE_WEATHER",
      payload: { ...wa2, id: "wa1" }
    });
    expect(state).not.toBe(initialState);
    expect(state.wa1).not.toBe(initialState.wa1);
    expect(state.wa1.days).not.toBe(initialState.wa1.days);
    expect(state.wa1.geometry).toBe(initialState.wa1.geometry);
    expect(state.wa2).toBe(initialState.wa2);
  });
});
