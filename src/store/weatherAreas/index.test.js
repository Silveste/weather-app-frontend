import weatherAreas from "./index";
import locations from "../locations";
import errors from "../errors";
import thunks from "./thunks";
import actionTypes from "./actionTypes";

// mocks & spies
const was = ["wa1", "wa2"];
const errorTracker = "error-123";
const dispatch = jest.fn();
const getState = jest.fn();

describe("reducer", () => {
  test("It has reducer property", () => {
    expect(weatherAreas.reducer).toBeFunction();
  });
});
describe("actions", () => {
  test("It has actions property", () => {
    expect(weatherAreas.actions).toBeObject();
  });
  describe("Action creators that return thunks", () => {
    describe("update", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "updatingWeatherArea").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.updatingWeatherArea.mockClear();
      });
      afterAll(() => {
        thunks.updatingWeatherArea.mockRestore();
      });
      test("Actions has update", () => {
        expect(weatherAreas.actions.update).toBeFunction();
      });
      test("It returns a function", () => {
        expect(weatherAreas.actions.update()).toBeFunction();
      });
      test("returning function calls updatingWeatherArea thunk", () => {
        weatherAreas.actions.update()();
        expect(thunks.updatingWeatherArea).toHaveBeenCalledTimes(1);
      });
      test("updatingWeatherArea is called with: dispatch, getState, weatherAreaId, { updateWeather, setLoading, addError }, errorTracker", () => {
        const { updateWeather, setLoading } = weatherAreas.actions;
        const { add: addError } = errors.actions;
        weatherAreas.actions.update(was[0], errorTracker)(dispatch, getState);
        expect(thunks.updatingWeatherArea).toHaveBeenCalledWith(
          dispatch,
          getState,
          was[0],
          expect.objectContaining({ updateWeather, setLoading, addError }),
          errorTracker
        );
      });
    });
    describe("addToLocation", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "addingToLocation").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.addingToLocation.mockClear();
      });
      afterAll(() => {
        thunks.addingToLocation.mockRestore();
      });
      test("Actions has addToLocation", () => {
        expect(weatherAreas.actions.addToLocation).toBeFunction();
      });
      test("It returns a function", () => {
        expect(weatherAreas.actions.addToLocation()).toBeFunction();
      });
      test("returning function calls updatingQueriesTracker thunk", () => {
        weatherAreas.actions.addToLocation()();
        expect(thunks.addingToLocation).toHaveBeenCalledTimes(1);
      });
      test("updatingQueriesTracker is called with: dispatch, getState, payload, actions, errorTracker", () => {
        weatherAreas.actions.addToLocation(
          "locId1",
          {
            type: "Polygon",
            coordinates: []
          },
          errorTracker
        )(dispatch, getState);
        expect(thunks.addingToLocation).toHaveBeenCalledWith(
          dispatch,
          getState,
          expect.anything(),
          expect.anything(),
          errorTracker
        );
      });
      test("payload argument has: locationId, geometry properties", () => {
        weatherAreas.actions.addToLocation(
          "locId1",
          {
            type: "Polygon",
            coordinates: []
          },
          errorTracker
        )(dispatch, getState);
        expect(thunks.addingToLocation).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          {
            locationId: "locId1",
            geometry: { type: "Polygon", coordinates: [] }
          },
          expect.anything(),
          expect.anything()
        );
      });
      test("actions argument has: addNew, deleteLocation, addError, actions creators", () => {
        const { addNew } = weatherAreas.actions;
        const { add: addError } = errors.actions;
        const { setWeatherArea: setLocationWeatherArea } = locations.actions;
        weatherAreas.actions.addToLocation(
          "locId1",
          {
            type: "Polygon",
            coordinates: []
          },
          errorTracker
        )(dispatch, getState);
        expect(thunks.addingToLocation).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          {
            addNew,
            setLocationWeatherArea,
            addError
          },
          expect.anything()
        );
      });
    });
  });
  describe("Action creators that return actions", () => {
    describe("addNew", () => {
      test("It is a property of weatherArea.actions", () => {
        expect(weatherAreas.actions.addNew).toBeFunction();
      });
      test("It returns an object", () => {
        expect(weatherAreas.actions.addNew(was)).toBeObject();
      });
      test("Returning abject has type: actionTypes.ADD", () => {
        const action = weatherAreas.actions.addNew(was);
        expect(action.type).toBe(actionTypes.ADD_NEW);
      });
      test("Returning object has payload: [location]", () => {
        const action = weatherAreas.actions.addNew(was);
        expect(action.payload).toEqual(was);
      });
    });
    describe("setLoading", () => {
      test("It is a property of weatherArea.actions", () => {
        expect(weatherAreas.actions.setLoading).toBeFunction();
      });
      test("It returns an object", () => {
        expect(weatherAreas.actions.setLoading(was[0], true)).toBeObject();
      });
      test("Returning abject has type: actionTypes.SET_LOADING", () => {
        const action = weatherAreas.actions.setLoading(was[0], true);
        expect(action.type).toBe(actionTypes.SET_LOADING);
      });
      test("Returning object has payload: {weatherAreaId, isLoading}", () => {
        const action = weatherAreas.actions.setLoading(was[0], true);
        expect(action.payload).toEqual({
          weatherAreaId: was[0],
          isLoading: true
        });
      });
    });
    describe("updateWeather", () => {
      test("It is a property of weatherArea.actions", () => {
        expect(weatherAreas.actions.updateWeather).toBeFunction();
      });
      test("It returns an object", () => {
        expect(weatherAreas.actions.updateWeather(was[0])).toBeObject();
      });
      test("Returning abject has type: actionTypes.UPDATE_WEATHER", () => {
        const action = weatherAreas.actions.updateWeather(was[0]);
        expect(action.type).toBe(actionTypes.UPDATE_WEATHER);
      });
      test("Returning object has payload: weatherArea", () => {
        const action = weatherAreas.actions.updateWeather(was[0]);
        expect(action.payload).toEqual(was[0]);
      });
    });
  });
});
