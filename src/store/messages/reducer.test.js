import reducer from "./reducer";
import faker from "faker";

const messageId = "messageId";
jest.mock("../../utils/uniqueId", () => {
  return {
    __esModule: true,
    default: jest.fn(() => messageId)
  };
});
const messagePayload = {
  message: faker.lorem.sentence(),
  type: "Danger",
  duration: 2000,
  id: "bbb",
  timerId: "aaaaa"
};

test("Exports reducer as a fucntion", () => {
  expect(reducer).toBeFunction();
});

describe("Initial state", () => {
  let state;
  beforeEach(() => {
    state = reducer(undefined, { type: null });
  });
  test("It is an empty array", () => {
    expect(state).toBeArray();
    expect(state).toBeEmpty();
  });
});

describe("PUSH_MESSAGE", () => {
  const reducerArgs = [
    undefined,
    {
      type: "PUSH_MESSAGE",
      payload: messagePayload
    }
  ];
  test("Creates new message", () => {
    const state = reducer(...reducerArgs);
    expect(state).toBeArrayOfSize(1);
  });
  test.each([
    ["message", "String"],
    ["type", "String"],
    ["timerId", "String"], //String only in the mockPayload
    ["id", "String"],
    ["duration", "Number"]
  ])("Returned message has property: %s type %s", (property, type) => {
    const state = reducer(...reducerArgs);
    expect(state[0]).toHaveProperty(property);
    expect(state[0][property]).not.toBeNil();
    switch (type.toLowerCase()) {
      case "boolean":
        expect(state[0][property]).toBeBoolean();
        break;
      case "array":
        expect(state[0][property]).toBeArray();
        break;
      case "number":
        expect(state[0][property]).toBeNumber();
        break;
      case "date":
        expect(state[0][property]).toBeDate();
        break;
      case "function":
        expect(state[0][property]).toBeFunction();
        break;
      case "object":
        expect(state[0][property]).toBeObject();
        break;
      default:
        expect(state[0][property].constructor.name).toBe(type);
    }
  });
  test.each(["message", "id", "timerId"])(
    "Message is not created if payload.%s is missing",
    property => {
      const { type, payload } = reducerArgs[1];
      const newPayload = { ...payload };
      delete newPayload[property];
      const state = reducer(undefined, { type, payload: newPayload });
      expect(state[0]).toBeNil();
    }
  );
  test('If optional payload.type is missing, default is "Default"', () => {
    const { type, payload } = reducerArgs[1];
    const newPayload = { ...payload };
    delete newPayload.type;
    const state = reducer(undefined, { type, payload: newPayload });
    expect(state[0].type).toBe("Default");
  });
  test("If optional payload.duration is missing, default value is 5000", () => {
    const { type, payload } = reducerArgs[1];
    const newPayload = { ...payload };
    delete newPayload.duration;
    const state = reducer(undefined, { type, payload: newPayload });
    expect(state[0].duration).toBe(5000);
  });
});

describe("POP_MESSAGE", () => {
  const message1 = { ...messagePayload, id: "messageId1" };
  const message2 = { ...messagePayload, id: "messageId2" };
  const initialState = [message1, message2];
  test("Removes message with id === payload", () => {
    const state = reducer(initialState, {
      type: "POP_MESSAGE",
      payload: "messageId1"
    });
    expect(state).toBeArrayOfSize(1);
    expect(state).not.toBe(initialState);
    expect(state[0]).toBe(message2);
    expect(state[0]).toEqual(message2);
  });
});
