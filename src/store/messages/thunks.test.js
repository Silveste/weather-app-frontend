import faker from "faker";
import thunks from "./thunks";
import getUniqueid from "../../utils/uniqueId";

const id = "messageId";
jest.mock("../../utils/uniqueId", () => {
  return {
    __esModule: true,
    default: jest.fn(() => id)
  };
});

const dispatch = jest.fn();
const getState = jest.fn();
const actions = {
  push: jest.fn(),
  pop: jest.fn()
};

afterEach(() => {
  dispatch.mockReset();
  getState.mockReset();
  actions.push.mockReset();
  actions.pop.mockReset();
});

describe("Thunk to add new message", () => {
  const payload = {
    message: faker.lorem.sentence(),
    type: "Danger",
    duration: 3000
  };
  test("It dispatches push action", () => {
    const testAction = "DispatchPush";
    actions.push.mockReturnValue(testAction);
    thunks.adding(dispatch, getState, payload, actions);
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch.mock.calls).toEqual(expect.arrayContaining([[testAction]]));
  });
  test("It dispatches push action with correct args", () => {
    const timerId = "timerId";
    const { message, type, duration } = payload;
    const setTimeoutSpy = jest
      .spyOn(global, "setTimeout")
      .mockImplementation(() => timerId);
    const testAction = "DispatchPush";
    actions.push.mockReturnValue(testAction);
    thunks.adding(dispatch, getState, payload, actions);
    expect(actions.push).toHaveBeenCalledTimes(1);
    expect(actions.push).toHaveBeenCalledWith(
      message,
      type,
      duration,
      id,
      timerId
    );
    setTimeoutSpy.mockRestore();
  });
  test("It dispatches pop action after message.duration miliseconds", () => {
    jest.useFakeTimers();
    const testAction = "DispatchPop";
    actions.pop.mockReturnValue(testAction);
    thunks.adding(dispatch, getState, payload, actions);
    jest.advanceTimersByTime(payload.duration);
    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch.mock.calls).toEqual(expect.arrayContaining([[testAction]]));
  });
  test("It dispatches pop action with correct args", () => {
    jest.useFakeTimers();
    const testAction = "DispatchPop";
    actions.pop.mockReturnValue(testAction);
    thunks.adding(dispatch, getState, payload, actions);
    jest.runAllTimers();
    expect(actions.pop).toHaveBeenCalledTimes(1);
    expect(actions.pop).toHaveBeenCalledWith(id);
  });
});

describe("Thunk to remove message", () => {
  const timerId = "timerId";
  beforeEach(() => {
    getState.mockImplementationOnce(() => {
      return {
        messages: [
          {
            id,
            timerId,
            message: faker.lorem.sentence(),
            type: "Danger",
            duration: 3000
          },
          {
            id: "id2",
            timerId: "otherTId",
            message: faker.lorem.sentence(),
            type: "Default",
            duration: 4000
          }
        ]
      };
    });
  });
  test("It dispatches pop action with correct args", () => {
    const testAction = "DispatchPop";
    actions.pop.mockReturnValue(testAction);
    thunks.removing(dispatch, getState, id, actions);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(actions.pop).toHaveBeenCalledTimes(1);
    expect(actions.pop).toHaveBeenCalledWith(id);
    expect(dispatch).toHaveBeenCalledWith(testAction);
  });
  test("It cancels setTimeout using timerId", () => {
    const clearTimeoutSpy = jest.spyOn(global, "clearTimeout");
    clearTimeoutSpy.mockClear();
    thunks.removing(dispatch, getState, id, actions);
    expect(clearTimeoutSpy).toHaveBeenCalledTimes(1);
    expect(clearTimeoutSpy).toHaveBeenCalledWith(timerId);
  });
  test("If message already removed, do nothing", () => {
    const clearTimeoutSpy = jest.spyOn(global, "clearTimeout");
    clearTimeoutSpy.mockClear();
    thunks.removing(dispatch, getState, "nonExistentId", actions);
    expect(clearTimeoutSpy).not.toHaveBeenCalled();
    expect(dispatch).not.toHaveBeenCalled();
    expect(actions.pop).not.toHaveBeenCalled();
  });
});
