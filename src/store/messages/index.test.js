import faker from "faker";
import messages from "./index";
import actionTypes from "./actionTypes";
import thunks from "./thunks";

const messageMock = {
  message: faker.lorem.sentence(),
  type: "Default",
  duration: 2000,
  id: "aaaa",
  timerId: "bbbb"
};

const dispatch = jest.fn();
const getState = jest.fn();

describe("reducer", () => {
  test("It has reducer property", () => {
    expect(messages.reducer).toBeFunction();
  });
});

describe("actions", () => {
  describe("Action Creators that return actions", () => {
    test("It has actions property", () => {
      expect(messages.actions).toBeObject();
    });
    describe("Action creators that return actions", () => {
      describe("push", () => {
        test("It is a property of errors.actions", () => {
          expect(messages.actions.push).toBeFunction();
        });
        test("It returns an object", () => {
          const { message, type, duration, id, timerId } = messageMock;
          expect(
            messages.actions.push(message, type, duration, id, timerId)
          ).toBeObject();
        });
        test("Returning abject has type: actionTypes.PUSH", () => {
          const { message, type, duration, id } = messageMock;
          const action = messages.actions.push(message, type, duration, id);
          expect(action.type).toBe(actionTypes.PUSH);
        });
        test("Returning object has payload: messages", () => {
          const { message, type, duration, id, timerId } = messageMock;
          const action = messages.actions.push(
            message,
            type,
            duration,
            id,
            timerId
          );
          expect(action.payload).toEqual(messageMock);
        });
      });
      describe("pop", () => {
        test("It is a property of messages.actions", () => {
          expect(messages.actions.pop).toBeFunction();
        });
        test("It returns an object", () => {
          expect(messages.actions.pop(messageMock.id)).toBeObject();
        });
        test("Returning abject has type: actionTypes.POP", () => {
          const action = messages.actions.pop(messageMock.id);
          expect(action.type).toBe(actionTypes.POP);
        });
        test("Returning object has payload: timerId", () => {
          const action = messages.actions.pop(messageMock.id);
          expect(action.payload).toBe(messageMock.id);
        });
      });
    });
  });
  describe("Action Creators that return thunks", () => {
    describe("add", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "adding").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.adding.mockClear();
      });
      afterAll(() => {
        thunks.adding.mockRestore();
      });
      test("Actions has add", () => {
        expect(messages.actions.add).toBeFunction();
      });
      test("It returns a function", () => {
        expect(messages.actions.add()).toBeFunction();
      });
      test("returning function calls adding thunk", () => {
        messages.actions.add()();
        expect(thunks.adding).toHaveBeenCalledTimes(1);
      });
      test("adding thunk is called with: dispatch, getState, payload, actions", () => {
        const { message, type, duration } = messageMock;
        messages.actions.add(message, type, duration)(dispatch, getState);
        expect(thunks.adding).toHaveBeenCalledWith(
          dispatch,
          getState,
          expect.anything(),
          expect.anything()
        );
      });
      test("payload argument has: message, type, duration properties", () => {
        const { message, type, duration } = messageMock;
        messages.actions.add(message, type, duration)(dispatch, getState);
        expect(thunks.adding).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          { message, type, duration },
          expect.anything()
        );
      });
      test("actions argument has: push, pop actions creators", () => {
        const { message, type, duration } = messageMock;
        const { push, pop } = messages.actions;
        messages.actions.add(message, type, duration)(dispatch, getState);
        expect(thunks.adding).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          { push, pop }
        );
      });
    });
    describe("remove", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "removing").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.removing.mockClear();
      });
      afterAll(() => {
        thunks.removing.mockRestore();
      });
      test("Actions has remove", () => {
        expect(messages.actions.remove).toBeFunction();
      });
      test("It returns a function", () => {
        expect(messages.actions.remove()).toBeFunction();
      });
      test("returning function calls removing thunk", () => {
        messages.actions.remove()();
        expect(thunks.removing).toHaveBeenCalledTimes(1);
      });
      test("removing thunk is called with: dispatch, getState, id, { pop }", () => {
        const { id } = messageMock;
        const { pop } = messages.actions;
        messages.actions.remove(id)(dispatch, getState);
        expect(thunks.removing).toHaveBeenCalledWith(dispatch, getState, id, {
          pop
        });
      });
    });
  });
});
