const actionTypes = {
  PUSH: "PUSH_MESSAGE",
  POP: "POP_MESSAGE"
};

export default actionTypes;
