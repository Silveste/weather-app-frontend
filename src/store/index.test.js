import { actions } from "./index";
import thunks from "./thunks";
import weatherAreas from "./weatherAreas";
import locations from "./locations";
import errors from "./errors";

// mocks & spies
const point = [33.5, 32.8];
const errorTracker = "error-aaa";
const dispatch = jest.fn();
const getState = jest.fn();

describe("Action creators exposed to react components", () => {
  test("Exposes only the action creators required by react components", () => {
    expect(actions).toBeObject();
    expect(actions).toContainAllKeys([
      "locations",
      "weatherAreas",
      "queries",
      "showPointData",
      "errors",
      "messages"
    ]);
    expect(actions.locations).toContainAllKeys([
      "removeLocation",
      "useLocation",
      "setIsSaved",
      "toggleWeather"
    ]);
    expect(actions.weatherAreas).toContainAllKeys(["update", "addToLocation"]);
    expect(actions.errors).toContainAllKeys(["add", "setResolved"]);
    expect(actions.messages).toContainAllKeys(["add", "remove"]);
    expect(actions.queries).toContainAllKeys(["searchLocations"]);
  });
  describe("Global (Return a thunk)", () => {
    describe("showPointData", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "showingPointData").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.showingPointData.mockClear();
      });
      afterAll(() => {
        thunks.showingPointData.mockRestore();
      });
      test("It returns a function", () => {
        expect(actions.showPointData()).toBeFunction();
      });
      test("returning function calls showingPointData thunk", () => {
        actions.showPointData()();
        expect(thunks.showingPointData).toHaveBeenCalledTimes(1);
      });
      test("showingPointData is called with: dispatch, getState, payload, actions, errorTracker", () => {
        actions.showPointData(...point, errorTracker)(dispatch, getState);
        expect(thunks.showingPointData).toHaveBeenCalledWith(
          dispatch,
          getState,
          expect.anything(),
          expect.anything(),
          errorTracker
        );
      });
      test("payload argument has: lon, lat properties", () => {
        actions.showPointData(...point, errorTracker)(dispatch, getState);
        expect(thunks.showingPointData).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          { lon: point[0], lat: point[1] },
          expect.anything(),
          expect.anything()
        );
      });
      test("actions argument has: setWeatherArea, addLocation, setLocationOnMap, addNewWeatherArea, setLocationOnlyCached actions creators, addError", () => {
        const {
          setWeatherArea,
          add: addLocation,
          setOnMap: setLocationOnMap,
          setOnlyCached: setLocationOnlyCached
        } = locations.actions;
        const { addNew: addNewWeatherArea } = weatherAreas.actions;
        const { add: addError } = errors.actions;
        actions.showPointData(...point, errorTracker)(dispatch, getState);
        expect(thunks.showingPointData).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          {
            setWeatherArea,
            addLocation,
            setLocationOnMap,
            addNewWeatherArea,
            setLocationOnlyCached,
            addError
          },
          expect.anything()
        );
      });
    });
  });
  describe("locations", () => {
    test("It has locations.removeLocation", () => {
      expect(actions.locations.removeLocation).toBeFunction();
    });
    test("It has locations.useLocation", () => {
      expect(actions.locations.useLocation).toBeFunction();
    });
    test("It has locations.setIsSaved", () => {
      expect(actions.locations.setIsSaved).toBeFunction();
    });
    test("It has locations.toggleWeather", () => {
      expect(actions.locations.toggleWeather).toBeFunction();
    });
    test("It has locations.useLocation", () => {
      expect(actions.locations.useLocation).toBeFunction();
    });
  });
  describe("Weather Areas", () => {
    test("It has weatherAreas.update", () => {
      expect(actions.weatherAreas.update).toBeFunction();
    });
    test("It has weatherAreas.addToLocation", () => {
      expect(actions.weatherAreas.addToLocation).toBeFunction();
    });
  });
  describe("Errors", () => {
    test("It has errors.add", () => {
      expect(actions.errors.add).toBeFunction();
    });
    test("It has errors.setResolved", () => {
      expect(actions.errors.setResolved).toBeFunction();
    });
  });
  describe("Messages", () => {
    test("It has messages.add", () => {
      expect(actions.messages.add).toBeFunction();
    });
    test("It has messages.remove", () => {
      expect(actions.messages.remove).toBeFunction();
    });
  });
  describe("Queries", () => {
    test("It has queries.searchLocations", () => {
      expect(actions.queries.searchLocations).toBeFunction();
    });
  });
});
