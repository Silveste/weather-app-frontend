import faker from "faker";

/*
GEOMETRY
 */
export const generateRandomPolygon = (scale = 5, originX = 0, originY = 0) => {
  const initXGeo = Math.random() * scale + originX;
  const initYGeo = Math.random() * scale + originY;
  const middleXGeo = Math.random() * scale + scale + originX;
  const middleYGeo = Math.random() * scale + scale + originY;
  return {
    type: "Polygon",
    coordinates: [
      [
        [initXGeo, initYGeo],
        [
          initXGeo + Math.random() * (middleXGeo - initXGeo),
          middleYGeo + Math.random() * scale
        ],
        [middleXGeo, middleYGeo],
        [
          middleXGeo + Math.random() * scale,
          initYGeo + Math.random() * (middleYGeo - initYGeo)
        ],
        [initXGeo, initYGeo]
      ]
    ]
  };
};

export const generateRandomPolygonNotAround = (lon, lat) => {
  //Get scale according to coords scale
  let divisor = 1;
  let divLon = lon;
  let divLat = lat;
  while (divLon > 1 || divLat > 1) {
    divisor = divisor * 10;
    divLon = divLon / divisor;
    divLat = divLat / divisor;
  }
  const scale = divisor / 2;
  return generateRandomPolygon(scale, lon + scale, lat + scale);
};

export const generateRandomPolygonAround = (lon, lat, scale = 1) => {
  const initXGeo = lon - (Math.random() * lon * scale) / 2;
  const initYGeo = lat - (Math.random() * lat * scale) / 2;
  const middleXGeo = (Math.random() * lon * scale) / 2 + lon;
  const middleYGeo = (Math.random() * lat * scale) / 2 + lat;
  return {
    type: "Polygon",
    coordinates: [
      [
        [initXGeo, initYGeo],
        [
          (Math.random() * lon * scale) / 2 + lon,
          lat - lat * scale + (Math.random() * lat * scale) / 2
        ],
        [middleXGeo, middleYGeo],
        [
          lon - lon * scale + (Math.random() * lon * scale) / 2,
          (Math.random() * lat * scale) / 2 + lat
        ],
        [initXGeo, initYGeo]
      ]
    ]
  };
};

/*
LOCATIONS
 */
export const locIds = ["loc1", "loc2", "loc3", "loc4", "loc5", "loc6"];

export const getLocation = (values = {}) => {
  let {
    queriesTracker,
    onlyCached,
    isSaved,
    weatherAreaId,
    geometry,
    name,
    description,
    isOnMap,
    ...rest
  } = values;
  if (queriesTracker === undefined) queriesTracker = 0;
  if (onlyCached === undefined) onlyCached = true;
  if (isSaved === undefined) isSaved = false;
  if (weatherAreaId === undefined) weatherAreaId = null;
  if (geometry === undefined) geometry = generateRandomPolygon();
  if (name === undefined)
    name = faker.lorem.words(Math.ceil(Math.random() * 3));
  if (description === undefined)
    description = faker.lorem.words(Math.ceil(Math.random() * 9));
  if (isOnMap === undefined) isOnMap = false;
  return {
    ...rest,
    queriesTracker,
    onlyCached,
    isSaved,
    weatherAreaId,
    geometry,
    name,
    description,
    isOnMap
  };
};

export const getLocations = (values = {}) =>
  locIds.reduce((acc, locKey, i) => {
    const result = { ...acc };
    result[locKey] = getLocation(values);
    return result;
  }, {});

export const getLocationsWithWAIds = (values = {}) =>
  locIds.reduce((acc, locKey, i) => {
    const result = { ...acc };
    result[locKey] = getLocation({ weatherAreaId: waIds[i], ...values });
    return result;
  }, {});

/*
WEATHER-AREAS
 */
const now = new Date().valueOf();
const msPerDay = 24 * 60 * 60 * 1000;
const dates = new Array(7).fill().map((_, i) => {
  return new Date(new Date(now + (i - 2) * msPerDay).setUTCHours(0, 0, 0, 0));
});

//WeatherAreas
export const waIds = ["wa1", "wa2", "wa3", "wa4", "wa5", "wa6"];

export const getWeatherArea = (values = {}) => {
  const id = values.id !== undefined ? values.id : undefined;
  const updatedAt =
    values.updatedAt !== undefined ? values.updatedAt : new Date(now);
  const isLoading = values.isLoading !== undefined ? values.isLoading : false;
  const initXGeo = Math.random() * 50;
  const initYGeo = Math.random() * 50;
  const middleXGeo = Math.random() * 50 + 50;
  const middleYGeo = Math.random() * 50 + 50;
  const geometry =
    values.geometry !== undefined
      ? values.geometry
      : {
          type: "Polygon",
          coordinates: [
            [
              [initXGeo, initYGeo],
              [
                initXGeo + Math.random() * (middleXGeo - initXGeo),
                middleYGeo + Math.random() * 50
              ],
              [middleXGeo, middleYGeo],
              [
                middleXGeo + Math.random() * 50,
                initYGeo + Math.random() * (middleYGeo - initYGeo)
              ],
              [initXGeo, initYGeo]
            ]
          ]
        };
  const minPos = Math.floor(Math.random() * 3);
  const maxPos = Math.floor(Math.random() * (dates.length - 4)) + 4;
  const days =
    values.days !== undefined
      ? values.days
      : dates.slice(minPos, maxPos).reduce((acc, date) => {
          const key = date.toISOString();
          const icons = [
            "01d",
            "01n",
            "02d",
            "02n",
            "03d",
            "03n",
            "04d",
            "04n",
            "09d",
            "09n",
            "10d",
            "10n",
            "11d",
            "11n",
            "13d",
            "13n",
            "50d",
            "50n"
          ];
          const day = {
            date: date.toISOString(),
            sunrise: date.toISOString(),
            sunset: date.toISOString(),
            tempAvg: faker.random.number({ max: 25, min: 15, precision: 0.01 }),
            tempFeel: faker.random.number({
              max: 25,
              min: 15,
              precision: 0.01
            }),
            minTemp: faker.random.number({ max: 15, min: -5, precision: 0.01 }),
            maxTemp: faker.random.number({ max: 45, min: 25, precision: 0.01 }),
            windSpeed: faker.random.number({ max: 50, precision: 0.01 }),
            windDir: faker.random.number({ max: 359.9, precision: 0.1 }),
            main: faker.lorem.word(),
            description: faker.lorem.words(),
            icon: icons[Math.floor(Math.random() * icons.length)],
            rain: faker.random.number({ max: 100 }),
            snow: faker.random.number({ max: 200 }),
            clouds: faker.random.number({ max: 100 })
          };
          const acumulated = { ...acc };
          acumulated[key] = day;
          return acumulated;
        }, {});
  return {
    id,
    updatedAt,
    isLoading,
    geometry,
    days
  };
};

export const getWeatherAreas = (values = {}) =>
  waIds.reduce((acc, waKey) => {
    const result = { ...acc };
    result[waKey] = getWeatherArea(values);
    return result;
  }, {});
