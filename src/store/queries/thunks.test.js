import thunks from "./thunks";
import backend from "../../backend";
jest.mock("../../backend");

// Mock Arguments
const queriesState = {
  cache: [],
  map: {}
};
const dispatch = jest.fn();
const getState = jest.fn().mockReturnValue({ queries: queriesState });
const mockLocations = [{ id: "loc1" }, { id: "loc2" }];
backend.searchLocationByQuery.mockResolvedValue(mockLocations);

const query = "query";
const errorTracker = "error-123";
const actions = {
  addQuery: jest.fn(),
  removeQuery: jest.fn(),
  removeLastQuery: jest.fn(),
  bumpQuery: jest.fn(),
  setLocations: jest.fn(),
  addLocationsList: jest.fn(),
  addLocations: jest.fn(),
  updateLocationsQueryTrackers: jest.fn(),
  addError: jest.fn(),
  deleteQuery: jest.fn()
};

afterEach(() => {
  getState.mockClear();
  dispatch.mockReset();
  backend.searchLocationByQuery.mockClear();
  actions.addQuery.mockReset();
  actions.removeQuery.mockReset();
  actions.removeLastQuery.mockReset();
  actions.bumpQuery.mockReset();
  actions.setLocations.mockReset();
  actions.addLocationsList.mockReset();
  actions.addLocations.mockReset();
  actions.updateLocationsQueryTrackers.mockReset();
  actions.addError.mockReset();
  actions.deleteQuery.mockReset();
});

describe("Thunk to search locations", () => {
  describe("Query is not in cache and not loading", () => {
    test("It calls backend.searchLocationByQuery", () => {
      thunks.searchingLocations(
        dispatch,
        getState,
        query,
        actions,
        errorTracker
      );
      expect(backend.searchLocationByQuery).toHaveBeenCalledTimes(1);
      expect(backend.searchLocationByQuery).toHaveBeenCalledWith(query);
    });
    test("Dispatch addQuery()", () => {
      const testAction = "DispatchArgument";
      actions.addQuery.mockReturnValue(testAction);
      thunks.searchingLocations(
        dispatch,
        getState,
        query,
        actions,
        errorTracker
      );
      expect(actions.addQuery).toHaveBeenCalledTimes(1);
      expect(actions.addQuery).toHaveBeenCalledWith(query, errorTracker);
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith(testAction);
    });
    describe("On backend.searchLocationByQuery promise fullfillment", () => {
      test("dispatch setLocations()", async () => {
        const testAction = "DispatchSetLocations";
        actions.setLocations.mockReturnValue(testAction);
        await thunks.searchingLocations(
          dispatch,
          getState,
          query,
          actions,
          errorTracker
        );
        expect(actions.setLocations).toHaveBeenCalledTimes(1);
        expect(actions.setLocations).toHaveBeenCalledWith(
          query,
          mockLocations,
          errorTracker
        );
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("If cache lenght > 50 dispatch removeLastQuery()", async () => {
        const testAction = "DispatchRemoveLastQuery";
        actions.removeLastQuery.mockReturnValue(testAction);
        const testState = {
          //NOTE: Array needs to be 51 length as addQuery is beign mocked and doesn't add query to the state
          cache: new Array(51).fill().map(_ => "aaa"),
          map: {}
        };
        const getState = jest.fn().mockReturnValueOnce({ queries: testState });
        await thunks.searchingLocations(
          dispatch,
          getState,
          query,
          actions,
          errorTracker
        );
        expect(actions.removeLastQuery).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
    });
    describe("On backend.searchLocationByQuery promise rejection", () => {
      test("Dispatch addError(error)", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.searchLocationByQuery.mockRejectedValueOnce(
          new Error("Promise rejected")
        );
        await thunks.searchingLocations(
          dispatch,
          getState,
          query,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("error.data is equal to error.response.data", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        const error = { response: { data: "Error data" } };
        backend.searchLocationByQuery.mockRejectedValueOnce(error);
        await thunks.searchingLocations(
          dispatch,
          getState,
          query,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ data: error.response.data })
        );
      });
      test("error.type = 'network'", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.searchLocationByQuery.mockRejectedValueOnce(
          new Error("Test error")
        );
        await thunks.searchingLocations(
          dispatch,
          getState,
          query,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ type: "network" })
        );
      });
      test("error.message is not empty string", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.searchLocationByQuery.mockRejectedValueOnce(
          new Error("Test error")
        );
        await thunks.searchingLocations(
          dispatch,
          getState,
          query,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ message: expect.stringMatching(/\S+/) })
        );
      });
      test("error.id = errorTracker", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.searchLocationByQuery.mockRejectedValueOnce(
          new Error("Test error")
        );
        await thunks.searchingLocations(
          dispatch,
          getState,
          query,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ id: errorTracker })
        );
      });
      test("Dispatch removeQuery()", async () => {
        const testAction = "DispatchRemoveQuery";
        actions.removeQuery.mockReturnValue(testAction);
        backend.searchLocationByQuery.mockRejectedValueOnce(
          new Error("Promise rejected")
        );
        await thunks.searchingLocations(
          dispatch,
          getState,
          query,
          actions,
          errorTracker
        );
        expect(actions.removeQuery).toHaveBeenCalledTimes(1);
        expect(actions.removeQuery).toHaveBeenCalledWith(query, errorTracker);
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
    });
  });
  describe("Query is in cache", () => {
    test("dispatch bumpQuery()", async () => {
      const testAction = "DispatchBumpQuery";
      actions.bumpQuery.mockReturnValue(testAction);
      const query = "a";
      const testState = {
        cache: ["a"],
        map: { a: { locations: [], loading: false } }
      };
      const getState = jest.fn().mockReturnValueOnce({ queries: testState });
      await thunks.searchingLocations(
        dispatch,
        getState,
        query,
        actions,
        errorTracker
      );
      expect(backend.searchLocationByQuery).not.toHaveBeenCalled();
      expect(actions.bumpQuery).toHaveBeenCalledTimes(1);
      expect(actions.bumpQuery).toHaveBeenCalledWith(query, errorTracker);
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
  });
});

describe("Thunk to set locations", () => {
  test("Dispatch addLocationsList", () => {
    const testAction = "DispatchAddLocationsList";
    actions.addLocationsList.mockReturnValue(testAction);
    thunks.settingLocations(
      dispatch,
      getState,
      { query, locations: mockLocations },
      actions,
      errorTracker
    );
    expect(actions.addLocationsList).toHaveBeenCalledTimes(1);
    expect(actions.addLocationsList).toHaveBeenCalledWith(
      query,
      mockLocations.map(loc => loc.id),
      errorTracker
    );
    expect(dispatch.mock.calls).toEqual(expect.arrayContaining([[testAction]]));
  });
  describe("Payload locations is not empty", () => {
    test("If locations not empty dispatch addLocations", () => {
      const testAction = "DispatchAddLocations";
      actions.addLocations.mockReturnValue(testAction);
      thunks.settingLocations(
        dispatch,
        getState,
        { query, locations: mockLocations },
        actions,
        errorTracker
      );
      thunks.settingLocations(
        dispatch,
        getState,
        { query, locations: [] },
        actions,
        errorTracker
      );
      expect(actions.addLocations).toHaveBeenCalledTimes(1);
      expect(actions.addLocations).toHaveBeenCalledWith(
        mockLocations,
        errorTracker
      );
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
    test("If locations not empty dispatch updateLocationsQueryTrackers", () => {
      const testAction = "DispatchLocationsQueryTrackers";
      actions.updateLocationsQueryTrackers.mockReturnValue(testAction);
      thunks.settingLocations(
        dispatch,
        getState,
        { query, locations: mockLocations },
        actions,
        errorTracker
      );
      thunks.settingLocations(
        dispatch,
        getState,
        { query, locations: [] },
        actions,
        errorTracker
      );
      expect(actions.updateLocationsQueryTrackers).toHaveBeenCalledTimes(1);
      expect(actions.updateLocationsQueryTrackers).toHaveBeenCalledWith(
        mockLocations.map(loc => loc.id),
        1,
        errorTracker
      );
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
  });
});

describe("Thunk to remove query", () => {
  test("if query is associated to locations Dispatch updateLocationsQueryTrackers()", () => {
    const testAction = "DispatchUpdateLocationsQueryTrackers";
    actions.updateLocationsQueryTrackers.mockReturnValue(testAction);
    const testQuery = "testQuery";
    const testQuery2 = "testQuery2";
    const queriesState = {
      cache: [testQuery, testQuery2],
      map: {
        testQuery: {
          loading: false,
          locations: mockLocations.map(loc => loc.id)
        },
        testQuery2: { loading: false, locations: [] }
      }
    };
    const getState = jest.fn().mockReturnValue({ queries: queriesState });
    thunks.removingQuery(dispatch, getState, testQuery, actions, errorTracker);
    thunks.removingQuery(dispatch, getState, testQuery2, actions, errorTracker);
    expect(actions.updateLocationsQueryTrackers).toHaveBeenCalledTimes(1);
    expect(actions.updateLocationsQueryTrackers).toHaveBeenCalledWith(
      mockLocations.map(loc => loc.id),
      -1,
      errorTracker
    );
    expect(dispatch.mock.calls).toEqual(expect.arrayContaining([[testAction]]));
  });
  test("Dispatch deleteQuery", () => {
    const testAction = "DispatchDeleteQuery";
    actions.deleteQuery.mockReturnValue(testAction);
    const testQuery = "testQuery";
    const queriesState = {
      cache: [testQuery],
      map: {
        testQuery: { loading: false, locations: mockLocations }
      }
    };
    const getState = jest.fn().mockReturnValue({ queries: queriesState });
    thunks.removingQuery(dispatch, getState, testQuery, actions, errorTracker);
    expect(actions.deleteQuery).toHaveBeenCalledTimes(1);
    expect(actions.deleteQuery).toHaveBeenCalledWith(testQuery, errorTracker);
    expect(dispatch.mock.calls).toEqual(expect.arrayContaining([[testAction]]));
  });
});

describe("Thunk to remove last query", () => {
  test("Dispatch removeeQuery with queries[0]", () => {
    const testAction = "DispatchDeleteQuery";
    actions.removeQuery.mockReturnValue(testAction);
    const testQuery = "testQuery";
    const testQuery2 = "testQuery2";
    const queriesState = {
      cache: [testQuery, testQuery2],
      map: {
        testQuery: {
          loading: false,
          locations: mockLocations.map(loc => loc.id)
        },
        testQuery2: { loading: false, locations: [] }
      }
    };
    const getState = jest.fn().mockReturnValue({ queries: queriesState });
    thunks.removingLastQuery(dispatch, getState, actions, errorTracker);
    expect(actions.removeQuery).toHaveBeenCalledTimes(1);
    expect(actions.removeQuery).toHaveBeenCalledWith(
      queriesState.cache[0],
      errorTracker
    );
    expect(dispatch.mock.calls).toEqual(expect.arrayContaining([[testAction]]));
  });
});
