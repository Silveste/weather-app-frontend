import queries from "./index";
import locs from "../locations";
import errors from "../errors";
import thunks from "./thunks";
import actionTypes from "./actionTypes";

//Mocks & Spies
const query = "testQuery";
const locations = ["id1", "id2"];
const errorTracker = "error-123";
const dispatch = jest.fn();
const getState = jest.fn();

describe("reducer", () => {
  test("It has reducer property", () => {
    expect(queries.reducer).toBeFunction();
  });
});
describe("actions", () => {
  test("Query has actions property", () => {
    expect(queries.actions).toBeObject();
  });
  describe("ActionCreators that return thunks", () => {
    describe("searchLocations", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "searchingLocations").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.searchingLocations.mockClear();
      });
      afterAll(() => {
        thunks.searchingLocations.mockRestore();
      });
      test("Queries actions has searchlocations", () => {
        expect(queries.actions.searchLocations).toBeFunction();
      });
      test("It returns a function", () => {
        expect(queries.actions.searchLocations()).toBeFunction();
      });
      test("Returning function calls searchingLocations thunk", () => {
        queries.actions.searchLocations()();
        expect(thunks.searchingLocations).toHaveBeenCalledTimes(1);
      });
      test("searchingLocations is called with: dispatch, getState, query, actions", () => {
        queries.actions.searchLocations(query, errorTracker)(
          dispatch,
          getState
        );
        expect(thunks.searchingLocations).toHaveBeenCalledWith(
          dispatch,
          getState,
          query,
          expect.anything(),
          errorTracker
        );
      });
      test("actions argument has: addQuery, removeQuery, setLocations, removeLastQuery, bumpQuery, addError actions creators", () => {
        const {
          addQuery,
          removeQuery,
          setLocations,
          removeLastQuery,
          bumpQuery
        } = queries.actions;
        const { add: addError } = errors.actions;
        queries.actions.searchLocations(query, errorTracker)(
          dispatch,
          getState
        );
        expect(thunks.searchingLocations).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          expect.objectContaining({
            addQuery,
            removeQuery,
            setLocations,
            removeLastQuery,
            bumpQuery,
            addError
          }),
          expect.anything()
        );
      });
    });
    describe("setLocations", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "settingLocations").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.settingLocations.mockClear();
      });
      afterAll(() => {
        thunks.settingLocations.mockRestore();
      });
      test("Queries actions has setLocations", () => {
        expect(queries.actions.setLocations).toBeFunction();
      });
      test("It returns a function", () => {
        expect(queries.actions.setLocations()).toBeFunction();
      });
      test("Returning function calls settingLocations", () => {
        queries.actions.setLocations()();
        expect(thunks.settingLocations).toHaveBeenCalledTimes(1);
      });
      test("settingLocations is called with: dispatch, getState, payload, actions, errorTracker", () => {
        queries.actions.setLocations(
          query,
          locations,
          errorTracker
        )(dispatch, getState);
        expect(thunks.settingLocations).toHaveBeenCalledWith(
          dispatch,
          getState,
          expect.anything(),
          expect.anything(),
          errorTracker
        );
      });
      test("Payload argument has: query, locations properties", () => {
        queries.actions.setLocations(
          query,
          locations,
          errorTracker
        )(dispatch, getState);
        expect(thunks.settingLocations).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.objectContaining({ query, locations }),
          expect.anything(),
          expect.anything()
        );
      });
      test("Actions argument has: addLocations, updateLocationsQueryTrackers (from locations add), addLocationsList", () => {
        const { addLocationsList } = queries.actions;
        queries.actions.setLocations(
          query,
          locations,
          errorTracker
        )(dispatch, getState);
        expect(thunks.settingLocations).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          expect.objectContaining({
            addLocations: locs.actions.add,
            addLocationsList,
            updateLocationsQueryTrackers: locs.actions.updateQueryTrackers
          }),
          expect.anything()
        );
      });
    });
    describe("removeQuery", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "removingQuery").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.removingQuery.mockClear();
      });
      afterAll(() => {
        thunks.removingQuery.mockRestore();
      });
      test("Queries actions has removeQuery", () => {
        expect(queries.actions.removeQuery).toBeFunction();
      });
      test("It returns a function", () => {
        expect(queries.actions.removeQuery()).toBeFunction();
      });
      test("Returning function calls removingQuery", () => {
        queries.actions.removeQuery()();
        expect(thunks.removingQuery).toHaveBeenCalledTimes(1);
      });
      test("removingQuery is called with: dispatch, getState, query, actions, errorTracker", () => {
        queries.actions.removeQuery(query, errorTracker)(dispatch, getState);
        expect(thunks.removingQuery).toHaveBeenCalledWith(
          dispatch,
          getState,
          query,
          expect.anything(),
          errorTracker
        );
      });
      test("Actions argument has: searchLocations, setLocations, removeQuery actions creators", () => {
        queries.actions.removeQuery(query, errorTracker)(dispatch, getState);
        expect(thunks.removingQuery).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.anything(),
          expect.objectContaining({
            updateLocationsQueryTrackers: locs.actions.updateQueryTrackers,
            deleteQuery: queries.actions.deleteQuery
          }),
          expect.anything()
        );
      });
    });
    describe("removeLastQuery", () => {
      beforeAll(() => {
        jest.spyOn(thunks, "removingLastQuery").mockImplementation(() => {});
      });
      afterEach(() => {
        thunks.removingLastQuery.mockClear();
      });
      afterAll(() => {
        thunks.removingLastQuery.mockRestore();
      });
      test("Queries actions has removeLastQuery", () => {
        expect(queries.actions.removeLastQuery).toBeFunction();
      });
      test("It returns a function", () => {
        expect(queries.actions.removeLastQuery()).toBeFunction();
      });
      test("Returning function calls removingLastQuery", () => {
        queries.actions.removeLastQuery()();
        expect(thunks.removingLastQuery).toHaveBeenCalledTimes(1);
      });
      test("removingLastQuery is called with: dispatch, getState, actions, errorTracker", () => {
        queries.actions.removeLastQuery(errorTracker)(dispatch, getState);
        expect(thunks.removingLastQuery).toHaveBeenCalledWith(
          dispatch,
          getState,
          expect.anything(),
          errorTracker
        );
      });
      test("Actions argument has: removeQuery actions creator", () => {
        queries.actions.removeLastQuery(errorTracker)(dispatch, getState);
        expect(thunks.removingLastQuery).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          expect.objectContaining({
            removeQuery: queries.actions.removeQuery
          }),
          expect.anything()
        );
      });
    });
  });
  describe("ActionCreators that return actions", () => {
    describe("addQuery", () => {
      test("Queries actions has addQuery", () => {
        expect(queries.actions.addQuery).toBeFunction();
      });
      test("It returns an object", () => {
        expect(queries.actions.addQuery()).toBeObject();
      });
      test("Returning object has type: actionTypes.ADD_QUERY", () => {
        const action = queries.actions.addQuery(query);
        expect(action.type).toBe(actionTypes.ADD_QUERY);
      });
      test("Returning object has payload: query", () => {
        const action = queries.actions.addQuery(query);
        expect(action.payload).toBe(query);
      });
    });
    describe("addLocationsList", () => {
      test("Queries actions has addLocationsList", () => {
        expect(queries.actions.addLocationsList).toBeFunction();
      });
      test("It returns an object", () => {
        expect(queries.actions.addLocationsList()).toBeObject();
      });
      test("Returning object has type: actionTypes.ADD_LOCATIONS_LIST", () => {
        const action = queries.actions.addLocationsList(query, locations);
        expect(action.type).toBe(actionTypes.ADD_LOCATIONS_LIST);
      });
      test("Returning object has payload: {query,locations}", () => {
        const action = queries.actions.addLocationsList(query, locations);
        expect(action.payload).toEqual({ query, locationIds: locations });
      });
    });
    describe("bumpQuery", () => {
      test("Queries actions has bumpQuery", () => {
        expect(queries.actions.bumpQuery).toBeFunction();
      });
      test("It returns an object", () => {
        expect(queries.actions.bumpQuery()).toBeObject();
      });
      test("Returning object has type: actionTypes.BUMP_QUERY", () => {
        const action = queries.actions.bumpQuery(query);
        expect(action.type).toBe(actionTypes.BUMP_QUERY);
      });
      test("Returning object has payload: query", () => {
        const action = queries.actions.bumpQuery(query);
        expect(action.payload).toBe(query);
      });
    });
    describe("deleteQuery", () => {
      test("Queries actions has deleteQuery", () => {
        expect(queries.actions.deleteQuery).toBeFunction();
      });
      test("It returns an object", () => {
        expect(queries.actions.deleteQuery()).toBeObject();
      });
      test("Returning object has type: actionTypes.DELETE_QUERY", () => {
        const action = queries.actions.deleteQuery(query);
        expect(action.type).toBe(actionTypes.DELETE_QUERY);
      });
      test("Returning object has payload: query", () => {
        const action = queries.actions.deleteQuery(query);
        expect(action.payload).toBe(query);
      });
    });
  });
});
