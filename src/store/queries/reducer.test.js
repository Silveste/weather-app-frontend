import reducer from "./reducer";

test("Exports reducer as a function", () => {
  expect(reducer).toBeFunction();
});
describe("Initial state", () => {
  let state;
  beforeEach(() => {
    state = reducer(undefined, { type: null });
  });
  test("It has cache property wich is an empty array", () => {
    expect(state.cache).toBeArrayOfSize(0);
  });
  test("It has map property which is an empty object", () => {
    expect(state.map).toBeObject();
    expect(state.map).toBeEmpty();
  });
});
describe("On ADD_QUERY action", () => {
  const oldState = {
    cache: ["oldquery"],
    map: {
      oldquery: {
        loading: false,
        locations: []
      }
    }
  };
  const query = "query";
  let state;
  beforeEach(() => {
    state = reducer(oldState, { type: "ADD_QUERY", payload: query });
  });
  test("It adds action.payload to state.cache as new string item", () => {
    expect(state.cache).toEqual(expect.arrayContaining([query]));
  });
  test("Payload is added to last position of state.cache array", () => {
    const { cache } = state;
    expect(cache[cache.length - 1]).toBe(query);
  });
  test("It adds new property to state.map: state.map[payload]", () => {
    const { map } = state;
    expect(map).toHaveProperty(query);
  });
  test("state.map[payload] is an object with property loading = true", () => {
    const { map } = state;
    expect(map.query).toEqual({ loading: true });
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    expect(state.cache).not.toBe(oldState.cache);
    expect(state.map).not.toBe(oldState.map);
  });
});
describe("On ADD_LOCATIONS_LIST action", () => {
  const oldState = {
    cache: ["aaa"],
    map: {
      aaa: {
        loading: true,
        locations: []
      }
    }
  };
  const query = "aaa";
  const locationIds = ["loc1", "loc2"];
  let state;
  beforeEach(() => {
    state = reducer(oldState, {
      type: "ADD_LOCATIONS_LIST_TO_QUERY",
      payload: { query, locationIds }
    });
  });
  test("Set state.map[payload.query].locations = payload.locations", () => {
    expect(state.map[query].locations).toEqual(locationIds);
  });
  test("Set state.map[payload.query].loading to false", () => {
    expect(state.map[query].loading).toBeFalse();
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    expect(state.map).not.toBe(oldState.map);
    expect(state.map.aaa).not.toBe(oldState.map.aaa);
  });
});
describe("On BUMP_QUERY action", () => {
  const oldState = {
    cache: ["a", "b", "c"],
    map: {
      a: { loading: false, locations: [] },
      b: { loading: true, locations: [] },
      c: { loading: false, locations: [] }
    }
  };
  test.each(["a", "b"])(
    "%# - It moves payload to have the last index in state.cache array",
    query => {
      const state = reducer(oldState, {
        type: "BUMP_QUERY",
        payload: query
      });
      const { cache } = state;
      expect(cache[cache.length - 1]).toBe(query);
      expect(cache.length).toBe(oldState.cache.length);
      const formerIndex = oldState.cache.indexOf(query);
      expect(cache[formerIndex]).toBe(oldState.cache[formerIndex + 1]);
    }
  );
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const state = reducer(oldState, {
      type: "BUMP_QUERY",
      payload: oldState.cache[1]
    });
    expect(state.cache).not.toBe(oldState.cache);
  });
});
describe("On DELETE_QUERY action", () => {
  const oldState = {
    cache: ["a", "b", "c"],
    map: {
      a: { loading: false, locations: [] },
      b: { loading: true, locations: [] },
      c: { loading: false, locations: [] }
    }
  };
  let state;
  let query = oldState.cache[1];
  beforeEach(() => {
    state = reducer(oldState, {
      type: "DELETE_QUERY",
      payload: query
    });
  });
  afterEach(() => {
    state = undefined;
    query = oldState.cache[1];
  });
  test("It removes payload from state.cache", () => {
    expect(state.cache).toBeArrayOfSize(oldState.cache.length - 1);
    expect(state.cache).not.toIncludeAnyMembers([query]);
  });
  test("It removes state.map[payload] from state.map", () => {
    expect(state.map[query]).toBeUndefined();
    expect(Object.keys(state.map)).not.toIncludeAnyMembers([query]);
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    expect(state.map).not.toBe(oldState.map);
    expect(state.cache).not.toBe(oldState.cache);
  });
});
