import thunks from "./thunks";
import backend from "../backend";
import {
  getWeatherAreas,
  getLocations,
  getWeatherArea,
  getLocation,
  generateRandomPolygonAround,
  generateRandomPolygonNotAround
} from "./__mocks__/state";
jest.mock("../backend");

const payload = { lon: 35.24, lat: 26.32 };
const errorTracker = "error-aaa";
const dispatch = jest.fn();

const locationAround = getLocation({
  geometry: generateRandomPolygonAround(payload.lon, payload.lat, 5),
  id: "testLoc1"
});
const waAround = getWeatherArea({
  geometry: generateRandomPolygonAround(payload.lon, payload.lat, 50),
  id: "testWa1"
});

backend.getAllByCoords.mockResolvedValue({
  location: locationAround,
  weatherArea: waAround
});

backend.getWeatherByCoords.mockResolvedValue(waAround);

backend.searchLocationByCoords.mockResolvedValue(locationAround);

const actions = {
  addNewWeatherArea: jest.fn(),
  addLocation: jest.fn(),
  setWeatherArea: jest.fn(),
  setLocationOnMap: jest.fn(),
  setLocationOnlyCached: jest.fn(),
  addError: jest.fn()
};

afterEach(() => {
  dispatch.mockReset();
  backend.getAllByCoords.mockClear();
  backend.getWeatherByCoords.mockClear();
  backend.searchLocationByCoords.mockClear();
  actions.addNewWeatherArea.mockReset();
  actions.addLocation.mockReset();
  actions.setWeatherArea.mockReset();
  actions.setLocationOnMap.mockReset();
  actions.setLocationOnlyCached.mockReset();
  actions.addError.mockReset();
});

describe("Thunk to show data of a point in the map", () => {
  describe("weatherArea and location not in the store", () => {
    const getState = jest.fn().mockReturnValue({
      weatherAreas: getWeatherAreas({
        geometry: generateRandomPolygonNotAround(payload.lon, payload.lat)
      }),
      locations: getLocations({
        geometry: generateRandomPolygonNotAround(payload.lon, payload.lat)
      })
    });
    afterEach(() => {
      getState.mockClear();
    });
    test("It calls backend.getAllByCoords", () => {
      thunks.showingPointData(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(backend.getAllByCoords).toHaveBeenCalledTimes(1);
      expect(backend.getAllByCoords).toHaveBeenCalledWith(
        payload.lon,
        payload.lat
      );
    });
    describe("On backend.getAllByCoords promise fullfillment", () => {
      test("dispatch addNewWeatherArea with payload = weatherArea and errorTracker", async () => {
        const testAction = "DispatchAddNewWeatherArea";
        actions.addNewWeatherArea.mockReturnValue(testAction);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addNewWeatherArea).toHaveBeenCalledTimes(1);
        expect(actions.addNewWeatherArea).toHaveBeenCalledWith(
          waAround,
          errorTracker
        );
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("dispatch addLocation", async () => {
        const testAction = "DispatchAddLocation";
        actions.addLocation.mockReturnValue(testAction);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addLocation).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("addLocation payload has onlyCached=false,isOnMap=true and weatherArea set to received waId and errorTracker", async () => {
        const testAction = "DispatchAddLocation";
        actions.addLocation.mockReturnValue(testAction);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addLocation).toHaveBeenCalledTimes(1);
        expect(actions.addLocation).toHaveBeenCalledWith(
          {
            ...locationAround,
            onlyCached: false,
            isOnMap: true,
            weatherAreaId: waAround.id
          },
          errorTracker
        );
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
    });
    describe("On backend.getAllByCoords promise rejection", () => {
      test("Dispatch addError(error)", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.getAllByCoords.mockRejectedValueOnce(new Error("Async error"));
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("error.data is equal to error.response.data", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        const error = { response: { data: "Error data" } };
        backend.getAllByCoords.mockRejectedValueOnce(error);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ data: error.response.data })
        );
      });
      test("error.type = 'network'", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.getAllByCoords.mockRejectedValueOnce(new Error("Async error"));
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ type: "network" })
        );
      });
      test("error.message is not empty string", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.getAllByCoords.mockRejectedValueOnce(new Error("Async error"));
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ message: expect.stringMatching(/\S+/) })
        );
      });
      test("error.id = errorTracker", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.getAllByCoords.mockRejectedValueOnce(new Error("Async error"));
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ id: errorTracker })
        );
      });
    });
  });
  describe("Weather area not in the store, location in the store", () => {
    const getState = jest.fn().mockReturnValue({
      weatherAreas: getWeatherAreas({
        geometry: generateRandomPolygonNotAround(payload.lon, payload.lat)
      }),
      locations: {
        ...getLocations({
          geometry: generateRandomPolygonNotAround(payload.lon, payload.lat)
        }),
        testLoc1: getLocation({
          geometry: generateRandomPolygonAround(payload.lon, payload.lat, 5)
        })
      }
    });
    afterEach(() => {
      getState.mockClear();
    });
    test("It calls backend.getWeatherByCoords", () => {
      thunks.showingPointData(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(backend.getWeatherByCoords).toHaveBeenCalledTimes(1);
      expect(backend.getWeatherByCoords).toHaveBeenCalledWith(
        payload.lon,
        payload.lat
      );
    });
    describe("On backend.getWeatherByCoords promise fullfillment", () => {
      test("dispatch addNewWeatherArea with payload = weatherArea and errorTracker", async () => {
        const testAction = "DispatchAddNewWeatherArea";
        actions.addNewWeatherArea.mockReturnValue(testAction);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addNewWeatherArea).toHaveBeenCalledTimes(1);
        expect(actions.addNewWeatherArea).toHaveBeenCalledWith(
          waAround,
          errorTracker
        );
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("dispatch setWeatherArea with locId, waId and errorTracker", async () => {
        const testAction = "DispatchSetWeatherArea";
        actions.setWeatherArea.mockReturnValue(testAction);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.setWeatherArea).toHaveBeenCalledTimes(1);
        expect(actions.setWeatherArea).toHaveBeenCalledWith(
          locationAround.id,
          waAround.id,
          errorTracker
        );
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("dispatch setLocationOnMap with locId, isOnMap=true, errorTracker", async () => {
        const testAction = "DispatchSetLocationsOnMap";
        actions.setLocationOnMap.mockReturnValue(testAction);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.setLocationOnMap).toHaveBeenCalledTimes(1);
        expect(actions.setLocationOnMap).toHaveBeenCalledWith(
          locationAround.id,
          true,
          errorTracker
        );
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
    });
    describe("On backend.getWeatherByCoords promise rejected", () => {
      test("Dispatch addError(error)", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.getWeatherByCoords.mockRejectedValueOnce(
          new Error("Async error")
        );
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("error.data is equal to error.response.data", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        const error = { response: { data: "Error data" } };
        backend.getWeatherByCoords.mockRejectedValueOnce(error);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ data: error.response.data })
        );
      });
      test("error.type = 'network'", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.getWeatherByCoords.mockRejectedValueOnce(
          new Error("Async error")
        );
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ type: "network" })
        );
      });
      test("error.message is not empty string", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.getWeatherByCoords.mockRejectedValueOnce(
          new Error("Async error")
        );
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ message: expect.stringMatching(/\S+/) })
        );
      });
      test("error.id = errorTracker", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.getWeatherByCoords.mockRejectedValueOnce(
          new Error("Async error")
        );
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ id: errorTracker })
        );
      });
    });
  });
  describe("location not in the store, weather area in the store", () => {
    const getState = jest.fn().mockReturnValue({
      weatherAreas: {
        ...getWeatherAreas({
          geometry: generateRandomPolygonNotAround(payload.lon, payload.lat)
        }),
        testWa1: getWeatherArea({
          geometry: generateRandomPolygonAround(payload.lon, payload.lat, 50)
        })
      },
      locations: getLocations({
        geometry: generateRandomPolygonNotAround(payload.lon, payload.lat)
      })
    });
    afterEach(() => {
      getState.mockClear();
    });
    test("It calls backend.searchLocationByCoords", () => {
      thunks.showingPointData(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(backend.searchLocationByCoords).toHaveBeenCalledTimes(1);
      expect(backend.searchLocationByCoords).toHaveBeenCalledWith(
        payload.lon,
        payload.lat
      );
    });
    describe("On backend.searchLocationByCoords promise fullfillment", () => {
      test("dispatch addLocation", async () => {
        const testAction = "DispatchAddLocation";
        actions.addLocation.mockReturnValue(testAction);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addLocation).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("addLocation payload has onlyCached=false,isOnMap=true and weatherArea set to existing waId and errorTracker", async () => {
        const testAction = "DispatchAddLocation";
        actions.addLocation.mockReturnValue(testAction);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addLocation).toHaveBeenCalledTimes(1);
        expect(actions.addLocation).toHaveBeenCalledWith(
          {
            ...locationAround,
            onlyCached: false,
            isOnMap: true,
            weatherAreaId: waAround.id
          },
          errorTracker
        );
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
    });
    describe("On backend.searchLocationByCoords promise rejection", () => {
      test("Dispatch addError(error)", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.searchLocationByCoords.mockRejectedValueOnce(
          new Error("Async error")
        );
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalled();
        expect(dispatch.mock.calls).toEqual(
          expect.arrayContaining([[testAction]])
        );
      });
      test("error.data is equal to error.response.data", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        const error = { response: { data: "Error data" } };
        backend.searchLocationByCoords.mockRejectedValueOnce(error);
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ data: error.response.data })
        );
      });
      test("error.type = 'network'", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.searchLocationByCoords.mockRejectedValueOnce(
          new Error("Async error")
        );
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ type: "network" })
        );
      });
      test("error.message is not empty string", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.searchLocationByCoords.mockRejectedValueOnce(
          new Error("Async error")
        );
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ message: expect.stringMatching(/\S+/) })
        );
      });
      test("error.id = errorTracker", async () => {
        const testAction = "DispatchAddError";
        actions.addError.mockReturnValue(testAction);
        backend.searchLocationByCoords.mockRejectedValueOnce(
          new Error("Async error")
        );
        await thunks.showingPointData(
          dispatch,
          getState,
          payload,
          actions,
          errorTracker
        );
        expect(actions.addError).toHaveBeenCalledTimes(1);
        expect(actions.addError).toHaveBeenCalledWith(
          expect.objectContaining({ id: errorTracker })
        );
      });
    });
  });
  describe("Weather area and location in the store", () => {
    const getState = jest.fn().mockReturnValue({
      weatherAreas: {
        ...getWeatherAreas({
          geometry: generateRandomPolygonNotAround(payload.lon, payload.lat)
        }),
        testWa1: getWeatherArea({
          geometry: generateRandomPolygonAround(payload.lon, payload.lat, 50)
        })
      },
      locations: {
        ...getLocations({
          geometry: generateRandomPolygonNotAround(payload.lon, payload.lat)
        }),
        testLoc1: getLocation({
          geometry: generateRandomPolygonAround(payload.lon, payload.lat, 5)
        })
      }
    });
    afterEach(() => {
      getState.mockClear();
    });
    test("It doesn't call backend", () => {
      thunks.showingPointData(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(backend.getAllByCoords).not.toHaveBeenCalled();
      expect(backend.getWeatherByCoords).not.toHaveBeenCalled();
      expect(backend.searchLocationByCoords).not.toHaveBeenCalled();
    });
    test("dispatch setWeatherArea with locId, waId, errorTracker", () => {
      const testAction = "DispatchSetWeatherArea";
      actions.setWeatherArea.mockReturnValue(testAction);
      thunks.showingPointData(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.setWeatherArea).toHaveBeenCalledTimes(1);
      expect(actions.setWeatherArea).toHaveBeenCalledWith(
        locationAround.id,
        waAround.id,
        errorTracker
      );
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
    test("dispatch setLocationOnMap with locId, isOnMap=true, errorTracker", () => {
      const testAction = "DispatchSetLocationOnMap";
      actions.setLocationOnMap.mockReturnValue(testAction);
      thunks.showingPointData(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.setLocationOnMap).toHaveBeenCalledTimes(1);
      expect(actions.setLocationOnMap).toHaveBeenCalledWith(
        locationAround.id,
        true,
        errorTracker
      );
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
    test("dispatch setLocationOnlyCached with locId, onlyCached=false, errorTracker", () => {
      const testAction = "DispatchSetOnlyCached";
      actions.setLocationOnlyCached.mockReturnValue(testAction);
      thunks.showingPointData(
        dispatch,
        getState,
        payload,
        actions,
        errorTracker
      );
      expect(actions.setLocationOnlyCached).toHaveBeenCalledTimes(1);
      expect(actions.setLocationOnlyCached).toHaveBeenCalledWith(
        locationAround.id,
        false,
        errorTracker
      );
      expect(dispatch).toHaveBeenCalled();
      expect(dispatch.mock.calls).toEqual(
        expect.arrayContaining([[testAction]])
      );
    });
  });
});
