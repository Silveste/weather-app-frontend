import faker from "faker";
import errors from "./index";
import actionTypes from "./actionTypes";

describe("reducer", () => {
  test("It has reducer property", () => {
    expect(errors.reducer).toBeFunction();
  });
});

describe("actions", () => {
  test("It has actions property", () => {
    expect(errors.actions).toBeObject();
  });
  describe("Action creators that return actions", () => {
    describe("add", () => {
      const errorMock = {
        data: {
          message: faker.lorem.sentence(),
          code: faker.helpers.replaceSymbolWithNumber("###")
        },
        type: "network",
        message: faker.lorem.sentence(),
        payload: {
          query: faker.address.city(),
          lat: faker.address.latitude(),
          lon: faker.address.longitude()
        }
      };
      test("It is a property of errors.actions", () => {
        expect(errors.actions.add).toBeFunction();
      });
      test("It returns an object", () => {
        expect(errors.actions.add(errorMock)).toBeObject();
      });
      test("Returning abject has type: actionTypes.ADD", () => {
        const action = errors.actions.add(errorMock);
        expect(action.type).toBe(actionTypes.ADD);
      });
      test("Returning object has payload: errors", () => {
        const action = errors.actions.add(errorMock);
        expect(action.payload).toEqual(errorMock);
      });
    });
    describe("setResolved", () => {
      const errorId = "errorId";
      test("It is a property of errors.actions", () => {
        expect(errors.actions.setResolved).toBeFunction();
      });
      test("It returns an object", () => {
        expect(errors.actions.setResolved(errorId)).toBeObject();
      });
      test("Returning abject has type: actionTypes.SET_RESOLVED", () => {
        const action = errors.actions.setResolved(errorId);
        expect(action.type).toBe(actionTypes.SET_RESOLVED);
      });
      test("Returning object has payload: errorId", () => {
        const action = errors.actions.add(errorId);
        expect(action.payload).toEqual(errorId);
      });
    });
  });
});
