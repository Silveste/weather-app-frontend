const actionTypes = {
  ADD: "ADD_ERROR",
  SET_RESOLVED: "SET_ERROR_RESOLVED"
};

export default actionTypes;
