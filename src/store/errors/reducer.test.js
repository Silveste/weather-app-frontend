import reducer from "./reducer";
import faker from "faker";
import getUniqueId from "../../utils/uniqueId";

const errorId = "ErrorId";
jest.mock("../../utils/uniqueId", () => {
  return {
    __esModule: true,
    default: jest.fn(() => errorId)
  };
});

const payloadMock = {
  data: {
    message: faker.lorem.sentence(),
    code: faker.helpers.replaceSymbolWithNumber("###")
  },
  type: "network",
  message: faker.lorem.sentence(),
  payload: {
    query: faker.address.city(),
    lat: faker.address.latitude(),
    lon: faker.address.longitude()
  }
};

test("Exports reducer as a fucntion", () => {
  expect(reducer).toBeFunction();
});
describe("Initial state", () => {
  let state;
  beforeEach(() => {
    state = reducer(undefined, { type: null });
  });
  test("It is an empty object", () => {
    expect(state).toBeObject();
    expect(state).toBeEmpty();
  });
});

describe("ADD_ERROR", () => {
  const reducerArgs = [
    undefined,
    {
      type: "ADD_ERROR",
      payload: payloadMock
    }
  ];
  test("Creates new error with unique Id, If payload Id is nil", () => {
    getUniqueId.mockClear();
    const state = reducer(...reducerArgs);
    expect(getUniqueId).toHaveBeenCalled();
    expect(state).toHaveProperty(errorId);
  });
  test("Creates new error with payload id when it is included", () => {
    getUniqueId.mockClear();
    const id = "testId";
    const testReducerArgs = [
      undefined,
      {
        type: "ADD_ERROR",
        payload: { ...payloadMock, id }
      }
    ];
    const state = reducer(...testReducerArgs);
    expect(state).toHaveProperty(id);
  });
  test.each([
    ["data", "Object"],
    ["type", "String"],
    ["message", "String"],
    ["resolved", "Boolean"],
    ["timestamp", "Date"]
  ])("Returned error has property: %s type %s", (property, type) => {
    const state = reducer(...reducerArgs);
    expect(state[errorId]).toHaveProperty(property);
    expect(state[errorId][property]).not.toBeNil();
    switch (type.toLowerCase()) {
      case "boolean":
        expect(state[errorId][property]).toBeBoolean();
        break;
      case "array":
        expect(state[errorId][property]).toBeArray();
        break;
      case "number":
        expect(state[errorId][property]).toBeNumber();
        break;
      case "date":
        expect(state[errorId][property]).toBeDate();
        break;
      case "function":
        expect(state[errorId][property]).toBeFunction();
        break;
      case "object":
        expect(state[errorId][property]).toBeObject();
        break;
      default:
        expect(state[errorId][property].constructor.name).toBe(type);
    }
  });
  test.each(["message", "type"])(
    "Error is not created if payload.%s is missing",
    property => {
      const { type, payload } = reducerArgs[1];
      const newPayload = { ...payload };
      delete newPayload[property];
      const state = reducer(undefined, { type, payload: newPayload });
      expect(state[errorId]).toBeNil();
    }
  );
  test("If optional payload.date is missing, default is current date", () => {
    const mockDate = new Date(Date.now());
    const dateSpy = jest
      .spyOn(global, "Date")
      .mockImplementation(() => mockDate);
    const state = reducer(...reducerArgs);
    expect(dateSpy).toHaveBeenCalled();
    expect(state[errorId].timestamp).toEqual(mockDate);
    dateSpy.mockRestore();
  });
  test("If optional payload.resolved is missing, default value is false", () => {
    const state = reducer(...reducerArgs);
    expect(state[errorId].resolved).toBeFalse();
  });
  test("If optional payload.data is missing, default is undefined", () => {
    const { type, payload } = reducerArgs[1];
    const newPayload = { ...payload };
    delete newPayload.data;
    const state = reducer(undefined, { type, payload: newPayload });
    expect(state[errorId].data).toBeNil();
  });
});
describe("SET_ERROR_RESOLVED", () => {
  const initialState = {};
  initialState[errorId] = {
    ...payloadMock,
    timestamp: new Date(Date.now()),
    resolved: false
  };
  test("Set resolved error property to true", () => {
    const state = reducer(initialState, {
      type: "SET_ERROR_RESOLVED",
      payload: errorId
    });
    expect(state[errorId].resolved).toBeTrue();
  });
  test("Returns multiple nested shallow copies to keep inmutability", () => {
    const testInitialState = {
      key1: { test: "test" },
      ...initialState
    };
    const state = reducer(testInitialState, {
      type: "SET_ERROR_RESOLVED",
      payload: errorId
    });
    expect(state).not.toBe(testInitialState);
    expect(state.key1).toBe(testInitialState.key1);
    expect(state[errorId]).not.toBe(testInitialState[errorId]);
    expect(state[errorId].data).toBe(testInitialState[errorId].data);
    expect(state[errorId].timestamp).toBe(testInitialState[errorId].timestamp);
  });
});
