import React from "react";
import { useSelector } from "react-redux";
import { render, act } from "@testing-library/react";
import { actions } from "../../store";
import LocManager from "./index";

//Mocks for internal functions
const useSelectorMock = cb => cb({ queries, locations });
const mockDispatch = jest.fn();
jest.mock("react-redux", () => ({
  useSelector: jest.fn(useSelectorMock),
  useDispatch: () => mockDispatch
}));

const errorId = "error1";
let mockErrors = {};
const mockTryDispatch = jest.fn();
const mockResolve = jest.fn();
jest.mock("../../hooks/errorsListener", () => () => ({
  errors: mockErrors,
  tryDispatch: mockTryDispatch,
  resolve: mockResolve
}));

const searchLocationsSpy = jest.spyOn(actions.queries, "searchLocations");
const useLocationSpy = jest.spyOn(actions.locations, "useLocation");
const removeLocationSpy = jest.spyOn(actions.locations, "removeLocation");
const setLocationSavedSpy = jest.spyOn(actions.locations, "setIsSaved");
const toggleWeatherSpy = jest.spyOn(actions.locations, "toggleWeather");
const addToLocationSpy = jest.spyOn(actions.weatherAreas, "addToLocation");

//Mocks for Props
let childrenArgs;
const children = props => {
  childrenArgs = props;
  return <div data-testid="children"></div>;
};

//Mock for state
const queries = {
  cache: ["aaa", "bbb"],
  map: {
    aaa: { locations: ["locid1"], loading: false },
    bbb: { locations: ["locid2", "locid1"], loading: false }
  }
};
const locations = {
  locid1: {
    queriesTracker: 2,
    onlyCached: false,
    isSaved: false,
    weatherAreaId: "waid1",
    geometry: ["geometry1"],
    name: "City 1",
    description: "Description 1",
    isOnMap: true,
    importance: 1
  },
  locid2: {
    queriesTracker: 1,
    onlyCached: false,
    isSaved: true,
    weatherAreaId: "waid2",
    geometry: [],
    name: "City 2",
    description: "Description 2",
    isOnMap: false,
    importance: 0
  },
  locid3: {
    queriesTracker: 0,
    onlyCached: true,
    isSaved: false,
    weatherAreaId: "waid3",
    geometry: [],
    name: "City 3",
    description: "Description 3",
    isOnMap: false,
    importance: 0
  },
  locid4: {
    queriesTracker: 0,
    onlyCached: false,
    isSaved: false,
    weatherAreaId: "waid4",
    geometry: [],
    name: "City 4",
    description: "Description 4",
    isOnMap: true,
    importance: 0
  }
};

describe("Behaviour", () => {
  beforeEach(() => {
    searchLocationsSpy.mockClear();
    useLocationSpy.mockClear();
    removeLocationSpy.mockClear();
    setLocationSavedSpy.mockClear();
    toggleWeatherSpy.mockClear();
    addToLocationSpy.mockClear();
    useSelector.mockClear();
    mockDispatch.mockClear();
    mockTryDispatch.mockClear();
  });
  afterEach(() => {
    useSelector.mockImplementation(useSelectorMock);
  });
  describe("Query search, triggered by handleSearch prop", () => {
    test("It dispatches listening for errors, searchLocations action creator", () => {
      const query = "query";
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.handleSearch).toBeFunction();
      act(() => childrenArgs.handleSearch(query));
      expect(mockTryDispatch).toHaveBeenCalledTimes(1);
      expect(mockTryDispatch).toHaveBeenCalledWith(searchLocationsSpy, query);
    });
    test("If query in store, it does not call searchLocation", () => {
      render(<LocManager>{children}</LocManager>);
      act(() => childrenArgs.handleSearch(queries.cache[0]));
      expect(mockTryDispatch).not.toHaveBeenCalled();
    });
    test("After dispatch query, if it is loading, searching = true", () => {
      const query = "query";
      const { cache, map } = queries;
      const queriesNewState = {
        cache: [...cache, query],
        map: {
          ...map,
          query: { loading: true }
        }
      };
      useSelector.mockImplementation(cb =>
        cb({ queries: queriesNewState, locations })
      );
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.searching).toBeFalse();
      act(() => childrenArgs.handleSearch(query));
      expect(childrenArgs.searching).toBeTrue();
    });
    test("If current query has errors, show message", () => {
      mockTryDispatch.mockReturnValue(errorId);
      const sendMessageMock = jest.fn();
      mockResolve.mockImplementation(data => ({
        sendMessage: sendMessageMock
      }));
      const { rerender } = render(<LocManager>{children}</LocManager>);
      act(() => childrenArgs.handleSearch("query with error"));
      mockErrors = {
        [errorId]: {
          data: { error: "error 1" },
          type: "network",
          message: "error from test",
          timestamp: new Date(),
          resolve: false
        }
      };
      rerender(<LocManager>{children}</LocManager>);
      expect(sendMessageMock).toHaveBeenCalled();
      mockTryDispatch.mockReset();
      mockResolve.mockReset();
      mockErrors = {};
    });
    test("If current query has errors, do not dispatch other search", () => {
      mockErrors = {
        [errorId]: {
          data: { error: "error 1" },
          type: "network",
          message: "error from test",
          timestamp: new Date(),
          resolve: false
        }
      };
      render(<LocManager>{children}</LocManager>);
      act(() => childrenArgs.handleSearch("query 1"));
      expect(mockTryDispatch).not.toHaveBeenCalled();
      mockErrors = {};
    });
    test("If former query has errors, resolve silently", () => {
      mockTryDispatch.mockReturnValueOnce("error before");
      mockTryDispatch.mockReturnValue(errorId);
      const sendMessageMock = jest.fn();
      mockResolve.mockImplementation(data => ({
        sendMessage: sendMessageMock
      }));
      const { rerender } = render(<LocManager>{children}</LocManager>);
      act(() => childrenArgs.handleSearch("query with error"));
      act(() => childrenArgs.handleSearch("query with no error"));
      mockErrors = {
        "error before": {
          data: { error: "error before" },
          type: "network",
          message: "error from test",
          timestamp: new Date(),
          resolve: false
        }
      };
      rerender(<LocManager>{children}</LocManager>);
      expect(mockResolve).toHaveBeenCalledTimes(1);
      expect(mockResolve).toHaveBeenCalledWith("error before");
      expect(sendMessageMock).not.toHaveBeenCalled();
      mockTryDispatch.mockReset();
      mockResolve.mockReset();
      mockErrors = {};
    });
    test("If not query, searchResults is null/undefined", () => {
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.searchResults).toBeNil();
    });
    test("If query in store, searchResults: [{id, description}]", () => {
      const query = queries.cache[0];
      const id = queries.map[query].locations[0];
      const description = locations[id].description;
      const expected = [{ id, description }];
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.searchResults).toBeNil();
      act(() => childrenArgs.handleSearch(query));
      expect(childrenArgs.searchResults).toEqual(expected);
    });
    test("If query not in store, searchResults is Nil", () => {
      const query = "query";
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.searchResults).toBeNil();
      act(() => childrenArgs.handleSearch(query));
      expect(childrenArgs.searchResults).toBeNil();
    });
  });
  describe("Submit found location, triggered by handleSubmit", () => {
    test("It dispatches addLocation action creator, with location payload", () => {
      const locId = Object.keys(locations)[0];
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.handleSubmit).toBeFunction();
      act(() => childrenArgs.handleSubmit(locId));
      expect(mockDispatch).toHaveBeenCalledTimes(1);
      expect(mockDispatch).toHaveBeenCalledWith(
        useLocationSpy.mock.results[0].value
      );
      expect(useLocationSpy).toHaveBeenCalledTimes(1);
      expect(useLocationSpy).toHaveBeenCalledWith(locId);
    });
    test("It restores searchResults to Nil", () => {
      const query = queries.cache[0];
      const locId = queries.map[query].locations[0];
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.searchResults).toBeNil();
      act(() => childrenArgs.handleSearch(query));
      expect(childrenArgs.searchResults).not.toBeNil();
      act(() => childrenArgs.handleSubmit(locId));
      expect(childrenArgs.searchResults).toBeNil();
    });
  });
  describe("Manage locations", () => {
    test("removeLocation(locId) dispatches the action creator, payload: locId", () => {
      const locId = Object.keys(locations)[0];
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.removeLocation).toBeFunction();
      act(() => childrenArgs.removeLocation(locId));
      expect(mockDispatch).toHaveBeenCalledTimes(1);
      expect(mockDispatch).toHaveBeenCalledWith(
        removeLocationSpy.mock.results[0].value
      );
      expect(removeLocationSpy).toHaveBeenCalledTimes(1);
      expect(removeLocationSpy).toHaveBeenCalledWith(locId);
    });
    test("toggleLocationSaved dispatches setLocationSaved, payload: { locId, !location.isSaved }", () => {
      const locId1 = Object.keys(locations)[0];
      const locId2 = Object.keys(locations)[0];
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.toggleLocationSaved).toBeFunction();
      act(() => childrenArgs.toggleLocationSaved(locId1));
      act(() => childrenArgs.toggleLocationSaved(locId2));
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(mockDispatch).toHaveBeenNthCalledWith(
        1,
        setLocationSavedSpy.mock.results[0].value
      );
      expect(mockDispatch).toHaveBeenNthCalledWith(
        2,
        setLocationSavedSpy.mock.results[1].value
      );
      expect(setLocationSavedSpy).toHaveBeenCalledTimes(2);
      expect(setLocationSavedSpy).toHaveBeenNthCalledWith(
        1,
        locId1,
        !locations[locId1].isSaved
      );
      expect(setLocationSavedSpy).toHaveBeenNthCalledWith(
        2,
        locId2,
        !locations[locId2].isSaved
      );
    });
    test("togglelocationOnMap dispatches action and listen for errors", () => {});
    test("toggleLocationOnMap dispatches toggleWeather with payload: locId, !location.isOnMap ", () => {
      const locId1 = Object.keys(locations)[0];
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.toggleLocationOnMap).toBeFunction();
      act(() => childrenArgs.toggleLocationOnMap(locId1));
      expect(mockTryDispatch).toHaveBeenCalledTimes(1);
      expect(mockTryDispatch).toHaveBeenCalledWith(
        toggleWeatherSpy,
        locId1,
        !locations[locId1].isOnMap
      );
    });
    test('If locations has no waId and while listening for togglelocationOnMap errors, weatherAreaId = ""', () => {
      const locId1 = Object.keys(locations)[0];
      const testLocations = {
        ...locations,
        [locId1]: {
          ...locations[locId1],
          weatherAreaId: null
        }
      };
      useSelector.mockImplementation(cb =>
        cb({ queries, locations: testLocations })
      );
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.toggleLocationOnMap).toBeFunction();
      act(() => childrenArgs.toggleLocationOnMap(locId1));
      expect(childrenArgs.locations).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ weatherAreaId: "", id: locId1 })
        ])
      );
    });
    test("If togglelocationOnMap causes an error, show message", () => {
      const locId1 = Object.keys(locations)[0];
      const testLocations = {
        ...locations,
        [locId1]: {
          ...locations[locId1],
          weatherAreaId: null
        }
      };
      useSelector.mockImplementation(cb =>
        cb({ queries, locations: testLocations })
      );
      mockTryDispatch.mockReturnValue(errorId);
      const sendMessageMock = jest.fn();
      mockResolve.mockImplementation(data => ({
        sendMessage: sendMessageMock
      }));
      const { rerender } = render(<LocManager>{children}</LocManager>);
      act(() => childrenArgs.toggleLocationOnMap(locId1));
      mockErrors = {
        [errorId]: {
          data: { error: "error 1" },
          type: "network",
          message: "error from test",
          timestamp: new Date(),
          resolve: false
        }
      };
      rerender(<LocManager>{children}</LocManager>);
      expect(sendMessageMock).toHaveBeenCalled();
      mockTryDispatch.mockReset();
      mockResolve.mockReset();
      mockErrors = {};
    });
    test("If togglelocationOnMap causes an error, set IsOnMap = false", () => {
      const locId1 = Object.keys(locations)[0];
      const testLocations = {
        ...locations,
        [locId1]: {
          ...locations[locId1],
          weatherAreaId: null
        }
      };
      useSelector.mockImplementation(cb =>
        cb({ queries, locations: testLocations })
      );
      mockTryDispatch.mockReturnValue(errorId);
      const sendMessageMock = jest.fn();
      mockResolve.mockImplementation(data => ({
        sendMessage: sendMessageMock
      }));
      const { rerender } = render(<LocManager>{children}</LocManager>);
      act(() => childrenArgs.toggleLocationOnMap(locId1));
      mockErrors = {
        [errorId]: {
          data: { error: "error 1" },
          type: "network",
          message: "error from test",
          timestamp: new Date(),
          resolve: false
        }
      };
      rerender(<LocManager>{children}</LocManager>);
      expect(toggleWeatherSpy).toHaveBeenLastCalledWith(locId1, false);
      mockTryDispatch.mockReset();
      mockResolve.mockReset();
      mockErrors = {};
    });
    test("If togglelocationOnMap causes an error, set weatherAreaId = null", () => {
      const locId1 = Object.keys(locations)[0];
      const testLocations = {
        ...locations,
        [locId1]: {
          ...locations[locId1],
          weatherAreaId: null
        }
      };
      useSelector.mockImplementation(cb =>
        cb({ queries, locations: testLocations })
      );
      mockTryDispatch.mockReturnValue(errorId);
      const sendMessageMock = jest.fn();
      mockResolve.mockImplementation(data => ({
        sendMessage: sendMessageMock
      }));
      const { rerender } = render(<LocManager>{children}</LocManager>);
      act(() => childrenArgs.toggleLocationOnMap(locId1));
      mockErrors = {
        [errorId]: {
          data: { error: "error 1" },
          type: "network",
          message: "error from test",
          timestamp: new Date(),
          resolve: false
        }
      };
      expect(childrenArgs.locations).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ weatherAreaId: "", id: locId1 })
        ])
      );
      rerender(<LocManager>{children}</LocManager>);
      expect(childrenArgs.locations).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ weatherAreaId: null, id: locId1 })
        ])
      );
      mockTryDispatch.mockReset();
      mockResolve.mockReset();
      mockErrors = {};
    });

    describe("onSelectedLocation", () => {
      test("Set selectedLocationName", () => {
        const locId = Object.keys(locations)[0];
        render(<LocManager>{children}</LocManager>);
        expect(childrenArgs.onSelectLocation).toBeFunction();
        expect(childrenArgs.selectedLocationName).toBeNull();
        act(() => childrenArgs.onSelectLocation(locId));
        expect(childrenArgs.selectedLocationName).toBe(locations[locId].name);
      });
      test("Set selectedWeatherAreaId", () => {
        const locId = Object.keys(locations)[0];
        render(<LocManager>{children}</LocManager>);
        expect(childrenArgs.onSelectLocation).toBeFunction();
        expect(childrenArgs.selectedWeatherAreaId).toBeNil();
        act(() => childrenArgs.onSelectLocation(locId));
        expect(childrenArgs.selectedWeatherAreaId).toBe(
          locations[locId].weatherAreaId
        );
      });
      test("If weatherAreaId is null dispatch wa.addTolocation and listen for errors", () => {
        const locId1 = Object.keys(locations)[0];
        const testLocations = {
          ...locations,
          [locId1]: {
            ...locations[locId1],
            weatherAreaId: null
          }
        };
        useSelector.mockImplementation(cb =>
          cb({ queries, locations: testLocations })
        );
        render(<LocManager>{children}</LocManager>);
        act(() => childrenArgs.onSelectLocation(locId1));
        expect(mockTryDispatch).toHaveBeenCalledTimes(1);
        expect(mockTryDispatch).toHaveBeenCalledWith(
          addToLocationSpy,
          locId1,
          locations[locId1].geometry
        );
      });
      test("If errors retrieving waId, send message to the user", () => {
        mockTryDispatch.mockReturnValue(errorId);
        const sendMessageMock = jest.fn();
        mockResolve.mockImplementation(data => ({
          sendMessage: sendMessageMock
        }));
        const locId1 = Object.keys(locations)[0];
        const testLocations = {
          ...locations,
          [locId1]: {
            ...locations[locId1],
            weatherAreaId: null
          }
        };
        useSelector.mockImplementation(cb =>
          cb({ queries, locations: testLocations })
        );
        const { rerender } = render(<LocManager>{children}</LocManager>);
        act(() => childrenArgs.onSelectLocation(locId1));
        mockErrors = {
          [errorId]: {
            data: { error: "error 1" },
            type: "network",
            message: "error from test",
            timestamp: new Date(),
            resolve: false
          }
        };
        rerender(<LocManager>{children}</LocManager>);
        expect(sendMessageMock).toHaveBeenCalled();
        mockTryDispatch.mockReset();
        mockResolve.mockReset();
        mockErrors = {};
      });
      test("If errors retrieving waId, deselect location", () => {
        mockTryDispatch.mockReturnValue(errorId);
        const sendMessageMock = jest.fn();
        mockResolve.mockImplementation(data => ({
          sendMessage: sendMessageMock
        }));
        const locId1 = Object.keys(locations)[0];
        const testLocations = {
          ...locations,
          [locId1]: {
            ...locations[locId1],
            weatherAreaId: null
          }
        };
        useSelector.mockImplementation(cb =>
          cb({ queries, locations: testLocations })
        );
        const { rerender } = render(<LocManager>{children}</LocManager>);
        act(() => childrenArgs.onSelectLocation(locId1));
        expect(childrenArgs.selectedLocationName).toBe(locations[locId1].name);
        expect(childrenArgs.selectedWeatherAreaId).toBe("");
        mockErrors = {
          [errorId]: {
            data: { error: "error 1" },
            type: "network",
            message: "error from test",
            timestamp: new Date(),
            resolve: false
          }
        };
        rerender(<LocManager>{children}</LocManager>);
        expect(childrenArgs.selectedLocationName).toBeNil();
        expect(childrenArgs.selectedWeatherAreaId).toBeNil();
        mockTryDispatch.mockReset();
        mockResolve.mockReset();
        mockErrors = {};
      });
    });

    test("locations prop: all locs in store with onlyCached = false", () => {
      const expectedLocations = Object.keys(locations)
        .filter(id => !locations[id].onlyCached)
        .map(id => {
          const {
            name,
            description: desc,
            weatherAreaId,
            isSaved,
            isOnMap
          } = locations[id];
          return { id, name, desc, isSaved, isOnMap, weatherAreaId };
        });
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.locations).toIncludeSameMembers(expectedLocations);
    });
    test("locations prop is empty if all locs in store have onlyCached = true", () => {
      const onlyCachedLocations = {};
      for (const prop in locations) {
        onlyCachedLocations[prop] = {
          ...locations[prop],
          onlyCached: true
        };
      }
      const useSelectorMock = cb =>
        cb({
          queries,
          locations: onlyCachedLocations
        });
      useSelector.mockImplementation(useSelectorMock);
      render(<LocManager>{children}</LocManager>);
      expect(childrenArgs.locations).toBeArrayOfSize(0);
    });
    test.each(["id", "name", "desc", "isSaved", "isOnMap"])(
      "locations prop is array of objects containing: %s",
      val => {
        render(<LocManager>{children}</LocManager>);
        childrenArgs.locations.forEach(loc => {
          expect(loc).toContainKey(val);
        });
      }
    );
  });
});
