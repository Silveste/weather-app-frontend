import React from "react";
import { useSelector } from "react-redux";
import { render, act } from "@testing-library/react";
import MapFrame from "./index";
import { actions } from "../../store";
import {
  getLocation,
  getWeatherArea,
  getLocationsWithWAIds,
  getWeatherAreas,
  waIds
} from "../../store/__mocks__/state";

//Mocks for internal functions
const store = {
  locations: getLocationsWithWAIds({ isOnMap: true }),
  weatherAreas: getWeatherAreas()
};

const useSelectorMock = cb => cb(store);
jest.mock("react-redux", () => ({
  useSelector: jest.fn(useSelectorMock)
}));

const showPointDataSpy = jest.spyOn(actions, "showPointData");

const errorId = "error1";
let mockErrors = {};
const mockTryDispatch = jest.fn();
mockTryDispatch.mockReturnValue(errorId);
const mockSendMessage = jest.fn();
const mockResolve = jest.fn(data => ({
  sendMessage: mockSendMessage
}));
jest.mock("../../hooks/errorsListener", () => () => ({
  errors: mockErrors,
  tryDispatch: mockTryDispatch,
  resolve: mockResolve
}));

//Mocks for Props
let childrenArgs;
const children = props => {
  childrenArgs = props;
  return <div data-testid="children"></div>;
};

describe("Behaviour", () => {
  beforeEach(() => {
    childrenArgs = null;
    useSelector.mockClear();
    mockTryDispatch.mockClear();
    mockSendMessage.mockClear();
    mockResolve.mockClear();
    showPointDataSpy.mockClear();
  });
  afterEach(() => {
    useSelector.mockImplementation(useSelectorMock);
    mockErrors = {};
  });
  test("Calls children() passing an object with children props", () => {
    render(<MapFrame>{children}</MapFrame>);
    expect(childrenArgs).toBeObject();
  });
  test.each([
    ["dayHandler", "function"],
    ["datesRange", "object"],
    ["selectedDate", "Date"],
    ["icons", "array"],
    ["onAction", "function"]
  ])("Children props object has prop: %s which is a %s ", (prop, type) => {
    render(<MapFrame>{children}</MapFrame>);
    switch (type.toLowerCase()) {
      case "boolean":
        expect(childrenArgs[prop]).toBeBoolean();
        break;
      case "array":
        expect(childrenArgs[prop]).toBeArray();
        break;
      case "number":
        expect(childrenArgs[prop]).toBeNumber();
        break;
      case "date":
        expect(childrenArgs[prop]).toBeDate();
        break;
      case "function":
        expect(childrenArgs[prop]).toBeFunction();
        break;
      case "object":
        expect(childrenArgs[prop]).toBeObject();
        break;
      default:
        expect(childrenArgs[prop].constructor.name).toBe(type);
    }
  });
  describe("datesRange Object", () => {
    const expected = Object.keys(store.weatherAreas).reduce((acc, waKey) => {
      const { min, max } = acc;
      const days = Object.keys(store.weatherAreas[waKey].days)
        .map(dayKey =>
          Number(new Date(dayKey).setUTCHours(0, 0, 0, 0).valueOf())
        )
        .sort((a, b) => a - b);
      return {
        min: !min || days[0] < min ? days[0] : min,
        max: !max || days[days.length - 1] > max ? days[days.length - 1] : max
      };
    }, {});
    test("Contains min, max and current", () => {
      render(<MapFrame>{children}</MapFrame>);
      const { datesRange } = childrenArgs;
      expect(datesRange.min).toBeDate();
      expect(datesRange.max).toBeDate();
      expect(datesRange.current).toBeDate();
    });
    test("max is the latest date within all weatherAreas dates", () => {
      render(<MapFrame>{children}</MapFrame>);
      const { datesRange } = childrenArgs;
      expect(datesRange.max.valueOf()).toBe(expected.max);
    });
    test("min is the earliest date within all weatherAreas dates", () => {
      render(<MapFrame>{children}</MapFrame>);
      const { datesRange } = childrenArgs;
      expect(datesRange.min.valueOf()).toBe(expected.min);
    });
    test("Current is within max an min (both inclusive)", () => {
      render(<MapFrame>{children}</MapFrame>);
      const { datesRange } = childrenArgs;
      expect(datesRange.current.valueOf()).toBeWithin(
        expected.min,
        expected.max + 1
      );
    });
    test("If not locations on map, current, max, min have same value = today", () => {
      const { locations } = store;
      const nonLocOnMap = Object.keys(locations).reduce((acc, locKey) => {
        const result = { ...acc };
        result[locKey] = {
          ...locations[locKey],
          isOnMap: false
        };
        return result;
      }, {});
      const specificStore = {
        ...store,
        locations: nonLocOnMap
      };
      useSelector.mockImplementation(cb => cb(specificStore));
      render(<MapFrame>{children}</MapFrame>);
      const { datesRange } = childrenArgs;
      const expected = new Date(new Date().setUTCHours(0, 0, 0, 0));
      expect(datesRange.min).toEqual(expected);
      expect(datesRange.max).toEqual(expected);
      expect(datesRange.current).toEqual(expected);
    });
    test("If any weatherArea loading & locations on map, datesRange = Nil", () => {
      const testStore = {
        locations: {
          ...store.locations,
          testLoc1: getLocation({ isOnMap: true, weatherAreaId: "testWA1" })
        },
        weatherAreas: {
          ...store.weatherAreas,
          testWA1: getWeatherArea({ isLoading: true })
        }
      };
      useSelector.mockImplementation(cb => cb(testStore));
      render(<MapFrame>{children}</MapFrame>);
      const { datesRange } = childrenArgs;
      expect(datesRange).toBeNil();
    });
  });
  test("dayHandler(X) changes current date X relative positions from days sorted array", () => {
    const sortedDates = Object.keys(store.weatherAreas)
      .reduce((acc, waKey) => {
        const days = Object.keys(store.weatherAreas[waKey].days)
          .map(dayKey => Number(new Date(dayKey).valueOf()))
          .filter(day => !acc.includes(day));
        return [...acc, ...days];
      }, [])
      .sort((a, b) => a - b);
    render(<MapFrame>{children}</MapFrame>);
    const dayHandler = childrenArgs.dayHandler;
    const initialCurrentIndex = sortedDates.indexOf(
      childrenArgs.datesRange.current.valueOf()
    );
    let nextMove = initialCurrentIndex !== 0 ? -1 : 1;
    act(() => dayHandler(nextMove));
    expect(childrenArgs.datesRange.current.valueOf()).toBe(
      sortedDates[initialCurrentIndex + nextMove]
    );
    nextMove = nextMove * -1;
    act(() => dayHandler(nextMove));
    expect(childrenArgs.datesRange.current.valueOf()).toBe(
      sortedDates[initialCurrentIndex]
    );
  });
  test("selectedDate is same date as datesRange.current", () => {
    render(<MapFrame>{children}</MapFrame>);
    const { datesRange, selectedDate } = childrenArgs;
    expect(datesRange.current).toEqual(selectedDate);
  });
  test("If any weatherArea loading, selectDate is not Nil", () => {
    const { weatherAreas } = store;
    const { wa3 } = weatherAreas;
    const loadingWAStore = {
      ...store,
      weatherAreas: {
        ...weatherAreas,
        wa3: {
          ...wa3,
          loading: true
        }
      }
    };
    useSelector.mockImplementation(cb => cb(loadingWAStore));
    render(<MapFrame>{children}</MapFrame>);
    const { selectedDate } = childrenArgs;
    expect(selectedDate).not.toBeNil();
    expect(selectedDate).toBeDate();
  });
  test("If no locations on map, selectedDate is Nil", () => {
    const { locations } = store;
    const nonLocOnMap = Object.keys(locations).reduce((acc, locKey) => {
      const result = { ...acc };
      result[locKey] = {
        ...locations[locKey],
        isOnMap: false
      };
      return result;
    }, {});
    const specificStore = {
      ...store,
      locations: nonLocOnMap
    };
    useSelector.mockImplementation(cb => cb(specificStore));
    render(<MapFrame>{children}</MapFrame>);
    const { selectedDate } = childrenArgs;
    expect(selectedDate).toBeNil();
  });
  test("onAction dispatches showPointData(lon,lat) and listen for errors", () => {
    const coords = [2, 3];
    render(<MapFrame>{children}</MapFrame>);
    act(() => childrenArgs.onAction(coords));
    expect(mockTryDispatch).toHaveBeenCalledTimes(1);
    expect(mockTryDispatch).toHaveBeenCalledWith(
      showPointDataSpy,
      coords[0],
      coords[1]
    );
  });
  test("If no error, unsubscribe from errors listener", () => {
    const coords = [2, 3];
    const testStore = {
      locations: {
        testLoc1: getLocation({
          isOnMap: true,
          weatherAreaId: "testWA1",
          geometry: {
            type: "Polygon",
            coordinates: [
              [
                [0, 0],
                [0, 5],
                [5, 5],
                [5, 0],
                [0, 0]
              ]
            ]
          }
        }),
        testLoc2: getLocation({
          isOnMap: true,
          weatherAreaId: "testWA1",
          geometry: {
            type: "Polygon",
            coordinates: [
              [
                [0, 5],
                [0, 10],
                [5, 10],
                [5, 5],
                [0, 5]
              ]
            ]
          }
        })
      },
      weatherAreas: {
        testWA1: getWeatherArea({
          geometry: {
            type: "Polygon",
            coordinates: [
              [
                [0, 0],
                [0, 20],
                [20, 20],
                [20, 0],
                [0, 0]
              ]
            ]
          }
        })
      }
    };
    const useSelectorMock = cb => cb(testStore);
    useSelector.mockImplementation(useSelectorMock);
    render(<MapFrame>{children}</MapFrame>);
    act(() => childrenArgs.onAction(coords));
    expect(mockTryDispatch).toHaveBeenCalledTimes(1);
    expect(mockResolve).toHaveBeenCalledTimes(1);
    expect(mockSendMessage).not.toHaveBeenCalled();
  });
  test("If errors send message to the user", () => {
    const coords = [25, 25];
    const testStore = {
      locations: {
        testLoc1: getLocation({
          isOnMap: true,
          weatherAreaId: "testWA1",
          geometry: {
            type: "Polygon",
            coordinates: [
              [
                [0, 0],
                [0, 5],
                [5, 5],
                [5, 0],
                [0, 0]
              ]
            ]
          }
        }),
        testLoc2: getLocation({
          isOnMap: true,
          weatherAreaId: "testWA1",
          geometry: {
            type: "Polygon",
            coordinates: [
              [
                [0, 5],
                [0, 10],
                [5, 10],
                [5, 5],
                [0, 5]
              ]
            ]
          }
        })
      },
      weatherAreas: {
        testWA1: getWeatherArea({
          geometry: {
            type: "Polygon",
            coordinates: [
              [
                [0, 0],
                [0, 20],
                [20, 20],
                [20, 0],
                [0, 0]
              ]
            ]
          }
        })
      }
    };
    const useSelectorMock = cb => cb(testStore);
    useSelector.mockImplementation(useSelectorMock);
    const { rerender } = render(<MapFrame>{children}</MapFrame>);
    act(() => childrenArgs.onAction(coords));
    mockErrors = {
      [errorId]: "Testing errors"
    };
    rerender(<MapFrame>{children}</MapFrame>);
    expect(mockTryDispatch).toHaveBeenCalledTimes(1);
    expect(mockResolve).toHaveBeenCalledTimes(1);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });
  describe("icons array", () => {
    test("It has property geometry, iconType, weatherAreaId, locationNames, iconId", () => {
      render(<MapFrame>{children}</MapFrame>);
      const { icons } = childrenArgs;
      expect(icons.length).toBeGreaterThan(0);
      icons.forEach(obj => {
        expect(obj.geometry).toBeObject();
        expect(obj.iconType).toBeString();
        expect(obj.weatherAreaId).toBeString();
        expect(obj.locationNames).toBeOneOf([
          expect.arrayContaining([expect.any(String)]),
          expect.any(String)
        ]);
        expect(obj.iconId).toBeString();
      });
    });
    test("If weatherArea loading, iconType && weatherAreaId = empty string", () => {
      const waLoading = getWeatherArea({ isLoading: true });
      const testStore = {
        locations: {
          ...store.locations,
          testLoc1: getLocation({ isOnMap: true, weatherAreaId: "testWA1" })
        },
        weatherAreas: {
          ...store.weatherAreas,
          testWA1: waLoading
        }
      };
      useSelector.mockImplementation(cb => cb(testStore));
      render(<MapFrame>{children}</MapFrame>);
      const { icons } = childrenArgs;
      const iconWALoading = icons.find(icon => icon.iconId === "testWA1");
      expect(iconWALoading).not.toBeNil();
      expect(iconWALoading.iconType).toBe("");
      expect(iconWALoading.weatherAreaId).toBe("");
    });
    test("Array includes only iconIds of locations on map", () => {
      const testStore = {
        locations: {
          ...store.locations,
          testLoc1: getLocation({ isOnMap: false, weatherAreaId: "testWA1" })
        },
        weatherAreas: {
          ...store.weatherAreas,
          testWA1: getWeatherArea()
        }
      };
      useSelector.mockImplementation(cb => cb(testStore));
      render(<MapFrame>{children}</MapFrame>);
      const { icons } = childrenArgs;
      const receivedIds = icons.map(icon => icon.iconId);
      expect(receivedIds).not.toIncludeAnyMembers(["testWA1"]);
      expect(receivedIds).toIncludeAllMembers(waIds);
    });
    test("onAction creates a point with geometry = argument, if no wa contains the point", () => {
      const coords = [-200, -300]; //geometry of locations and wa mocks are always postive
      render(<MapFrame>{children}</MapFrame>);
      act(() => childrenArgs.onAction(coords));
      const { icons } = childrenArgs;
      const recievedPoints = icons.map(icon =>
        icon.geometry.coordinates.toString()
      );
      expect(recievedPoints).toEqual(
        expect.arrayContaining([coords.toString()])
      );
    });
    test("If errors, loading point is removed", () => {
      const coords = [-200, -300]; //geometry of locations and wa mocks are always postive
      const { rerender } = render(<MapFrame>{children}</MapFrame>);
      act(() => childrenArgs.onAction(coords));
      mockErrors = {
        [errorId]: "Testing errors"
      };
      rerender(<MapFrame>{children}</MapFrame>);
      const { icons } = childrenArgs;
      const recievedPoints = icons.map(icon =>
        icon.geometry.coordinates.toString()
      );
      expect(recievedPoints).not.toIncludeAnyMembers([coords.toString()]);
    });
    test("If weather area contains onAction point, loading point is not rendered", () => {
      const coords = [2, 3];
      const testStore = {
        locations: {
          testLoc1: getLocation({
            isOnMap: true,
            weatherAreaId: "testWA1",
            geometry: {
              type: "Polygon",
              coordinates: [
                [
                  [0, 0],
                  [0, 5],
                  [5, 5],
                  [5, 0],
                  [0, 0]
                ]
              ]
            }
          }),
          testLoc2: getLocation({
            isOnMap: true,
            weatherAreaId: "testWA1",
            geometry: {
              type: "Polygon",
              coordinates: [
                [
                  [0, 5],
                  [0, 10],
                  [5, 10],
                  [5, 5],
                  [0, 5]
                ]
              ]
            }
          })
        },
        weatherAreas: {
          testWA1: getWeatherArea({
            geometry: {
              type: "Polygon",
              coordinates: [
                [
                  [0, 0],
                  [0, 20],
                  [20, 20],
                  [20, 0],
                  [0, 0]
                ]
              ]
            }
          })
        }
      };
      const useSelectorMock = cb => cb(testStore);
      useSelector.mockImplementation(useSelectorMock);
      render(<MapFrame>{children}</MapFrame>);
      act(() => childrenArgs.onAction(coords));
      const { icons } = childrenArgs;
      const recievedPoints = icons.map(icon =>
        icon.geometry.coordinates.toString()
      );
      expect(recievedPoints).not.toEqual(
        expect.arrayContaining([coords.toString()])
      );
    });
    test("location names are grouped when locations have same wa", () => {
      const groupedLocations = {
        testLoc1: getLocation({ isOnMap: true, weatherAreaId: "testWA1" }),
        testLoc2: getLocation({ isOnMap: true, weatherAreaId: "testWA1" }),
        testLoc3: getLocation({ isOnMap: true, weatherAreaId: "testWA1" })
      };
      const expectedNames = Object.keys(groupedLocations).map(
        key => groupedLocations[key].name
      );
      const testStore = {
        locations: groupedLocations,
        weatherAreas: {
          testWA1: getWeatherArea()
        }
      };
      useSelector.mockImplementation(cb => cb(testStore));
      render(<MapFrame>{children}</MapFrame>);
      const { icons } = childrenArgs;
      expect(icons).toBeArrayOfSize(1);
      expect(icons[0].locationNames).toIncludeSameMembers(expectedNames);
    });
    test("If icon do not group locations, geometry = location center of mass", () => {
      const testStore = {
        locations: {
          testLoc1: getLocation({
            isOnMap: true,
            weatherAreaId: "testWA1",
            geometry: {
              type: "Polygon",
              coordinates: [
                [
                  [0, 0],
                  [0, 5],
                  [5, 5],
                  [5, 0],
                  [0, 0]
                ]
              ]
            }
          })
        },
        weatherAreas: {
          testWA1: getWeatherArea()
        }
      };
      useSelector.mockImplementation(cb => cb(testStore));
      render(<MapFrame>{children}</MapFrame>);
      const { icons } = childrenArgs;
      const geometry = icons[0].geometry;
      expect(geometry).toEqual({
        type: "Point",
        coordinates: [expect.any(Number), expect.any(Number)]
      });
      expect(geometry.coordinates[0]).toBeCloseTo(2.5, 1);
      expect(geometry.coordinates[1]).toBeCloseTo(2.5, 1);
    });
    test("If icon group locations, geometry = wa center of mass", () => {
      const testStore = {
        locations: {
          testLoc1: getLocation({
            isOnMap: true,
            weatherAreaId: "testWA1",
            geometry: {
              type: "Polygon",
              coordinates: [
                [
                  [0, 0],
                  [0, 5],
                  [5, 5],
                  [5, 0],
                  [0, 0]
                ]
              ]
            }
          }),
          testLoc2: getLocation({
            isOnMap: true,
            weatherAreaId: "testWA1",
            geometry: {
              type: "Polygon",
              coordinates: [
                [
                  [0, 5],
                  [0, 10],
                  [5, 10],
                  [5, 5],
                  [0, 5]
                ]
              ]
            }
          })
        },
        weatherAreas: {
          testWA1: getWeatherArea({
            geometry: {
              type: "Polygon",
              coordinates: [
                [
                  [0, 0],
                  [0, 20],
                  [20, 20],
                  [20, 0],
                  [0, 0]
                ]
              ]
            }
          })
        }
      };
      useSelector.mockImplementation(cb => cb(testStore));
      render(<MapFrame>{children}</MapFrame>);
      const { icons } = childrenArgs;
      const geometry = icons[0].geometry;
      expect(geometry).toEqual({
        type: "Point",
        coordinates: [expect.any(Number), expect.any(Number)]
      });
      expect(geometry.coordinates[0]).toBeCloseTo(10, 1);
      expect(geometry.coordinates[1]).toBeCloseTo(10, 1);
    });
  });
});
