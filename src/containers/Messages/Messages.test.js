import React from "react";
import { render, fireEvent, within, waitFor } from "@testing-library/react";
import faker from "faker";
import { useSelector } from "react-redux";
import { actions } from "../../store";
import Messages from "./index";

//Fake store.messages
let count = 0;
const getFakeMessage = () => {
  //count ensures all fields are different
  count++;
  return {
    message: `${count}-${faker.hacker.phrase()}`,
    type: faker.random.arrayElement(["Danger", "Default", "Highlight"]),
    id: `id-${count}${faker.random.alphaNumeric(10)}`,
    timerId: `timer-${count}${faker.random.alphaNumeric(10)}`,
    duration: faker.random.number({ min: 1000, max: 7000 })
  };
};
const messages = [
  ...new Array(faker.random.number({ min: 2, max: 7 }))
].map(_ => getFakeMessage());

//Mocks for redux
const useSelectorMock = cb => cb({ messages });
const mockDispatch = jest.fn();

jest.mock("react-redux", () => ({
  useSelector: jest.fn(useSelectorMock),
  useDispatch: () => mockDispatch
}));

const removeMessageSpy = jest.spyOn(actions.messages, "remove");

describe("Behaviour", () => {
  beforeEach(() => {
    useSelector.mockClear();
    mockDispatch.mockClear();
    removeMessageSpy.mockClear();
  });
  test("Render the store messages", () => {
    const { queryByText } = render(<Messages />);
    expect.assertions(messages.length);
    messages.forEach(message => {
      expect(queryByText(message.message)).not.toBeNil();
    });
  });
  test("On message closed, dispatch message.remove", async () => {
    const testAction = "DispatchRemove";
    removeMessageSpy.mockReturnValue(testAction);
    const { queryByText } = render(<Messages />);
    const message = queryByText(messages[0].message);
    expect(message).not.toBeNil();
    //assumes message container has button tag to close it
    const btn = within(message.parentElement).getByRole("button");
    fireEvent.click(btn);
    await waitFor(() => {
      expect(mockDispatch).toHaveBeenCalledTimes(1);
      expect(mockDispatch).toHaveBeenCalledWith(testAction);
      expect(removeMessageSpy).toHaveBeenCalledTimes(1);
      expect(removeMessageSpy).toHaveBeenCalledWith(messages[0].id);
    });
  });
});
