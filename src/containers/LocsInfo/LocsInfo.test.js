import React from "react";
import { useSelector } from "react-redux";
import { render, act, fireEvent } from "@testing-library/react";
import LocsInfo from "./index";
import { actions } from "../../store";
import { getWeatherArea } from "../../store/__mocks__/state";

//Mocks for internal functions
const store = {
  weatherAreas: {
    wa1: getWeatherArea()
  }
};
const useSelectorMock = cb => cb(store);
jest.mock("react-redux", () => ({
  useSelector: jest.fn(useSelectorMock)
}));

const updateWASpy = jest.spyOn(actions.weatherAreas, "update");

let mockWeatherInfoProps;
jest.mock("../../components/WeatherInfo", () => props => {
  mockWeatherInfoProps = props;
  return (
    <div data-testid="WeatherInfo">
      {Object.keys(props).map(k => (
        <div key={k} data-testid={`WeatherInfo-${k}`}></div>
      ))}
    </div>
  );
});

let mockDateHandlerReceivedProps;
jest.mock("../../components/DateHandler", () => props => {
  mockDateHandlerReceivedProps = props;
  return (
    <div data-testid="DateHandler">
      {Object.keys(props).map(k => (
        <div key={k} data-testid={k}></div>
      ))}
    </div>
  );
});

jest.mock("../../components/UI/ScreenLoader", () => props => (
  <div data-testid="ScreenLoader">
    {Object.keys(props).map(k => (
      <div key={k} data-testid={k}></div>
    ))}
  </div>
));

jest.mock("../../components/Question", () => props => (
  <div data-testid="Question">
    {Object.keys(props).map(k => (
      <div key={k} data-testid={k}></div>
    ))}
    <button onClick={props.options.onAction}>Click here</button>
  </div>
));

const errorId = "error1";
let mockErrors = {};
const mockTryDispatch = jest.fn();
mockTryDispatch.mockReturnValue(errorId);
const mockSendMessage = jest.fn();
const mockResolve = jest.fn(data => ({
  sendMessage: mockSendMessage
}));
jest.mock("../../hooks/errorsListener", () => () => ({
  errors: mockErrors,
  tryDispatch: mockTryDispatch,
  resolve: mockResolve
}));

//Mocks for props
const waId = Object.keys(store.weatherAreas)[0];
const locationNames = ["City Name 1", "City Name 2"];

//Internal defaults

let consoleErrorMock;
beforeEach(() => {
  consoleErrorMock = jest.spyOn(console, "error");
  useSelector.mockClear();
  mockTryDispatch.mockClear();
  mockSendMessage.mockClear();
  mockResolve.mockClear();
  updateWASpy.mockClear();
});
afterEach(() => {
  consoleErrorMock.mockRestore();
  mockDateHandlerReceivedProps = null;
  mockWeatherInfoProps = null;
  useSelector.mockImplementation(useSelectorMock);
  mockErrors = {};
});
describe("Content", () => {
  test("It renders a header with the city name", () => {
    const cityName = "CityName1";
    const { getByText } = render(
      <LocsInfo id={waId} locationNames={cityName} />
    );
    getByText(cityName);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders a header with the city names", () => {
    const { getByText } = render(
      <LocsInfo id={waId} locationNames={locationNames} />
    );
    locationNames.forEach(val => {
      getByText(val);
    });
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders the WeatherInfo", () => {
    const { getByTestId } = render(
      <LocsInfo id={waId} locationNames={locationNames} />
    );
    getByTestId("WeatherInfo");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders the DateHandler", () => {
    const { getByTestId } = render(
      <LocsInfo id={waId} locationNames={locationNames} />
    );
    getByTestId("DateHandler");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders question when there are errors", () => {
    const { weatherAreas } = store;
    const { wa1 } = weatherAreas;
    const notUpdatedStore = {
      ...store,
      weatherAreas: {
        ...weatherAreas,
        wa1: {
          ...wa1,
          updatedAt: new Date(new Date().setUTCHours(0, 0, 0, 0) - 1)
        }
      }
    };
    useSelector.mockImplementation(cb => cb(notUpdatedStore));
    const { rerender, getByTestId } = render(
      <LocsInfo id={waId} locationNames={locationNames} />
    );
    mockErrors = {
      [errorId]: {
        data: { error: errorId },
        type: "network",
        message: "error from test",
        timestamp: new Date(),
        resolve: false
      }
    };
    rerender(<LocsInfo id={waId} locationNames={locationNames} />);
    getByTestId("Question");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});

describe("Behaviour", () => {
  test("Renders ScreenLoader passing city name = ''", () => {
    const cityName = "";
    const { getByTestId } = render(
      <LocsInfo id={waId} locationNames={cityName} />
    );
    getByTestId("ScreenLoader");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Renders ScreenLoader passing if city name array contains empty strings", () => {
    const locationNamesWithEmptyString = ["", ...locationNames];
    const { getByTestId, getByText } = render(
      <LocsInfo id={waId} locationNames={locationNamesWithEmptyString} />
    );

    getByTestId("ScreenLoader");
    locationNamesWithEmptyString.forEach(val => {
      if (val !== "") {
        getByText(val);
      }
    });
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If it receives props.date, DateHandler is not rendered", () => {
    const { queryByTestId } = render(
      <LocsInfo id={waId} locationNames={locationNames} date={new Date()} />
    );
    expect(queryByTestId("DateHandler")).toBeNull();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Renders DateHandler passing dayHandler function", () => {
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    const { dayHandler } = mockDateHandlerReceivedProps;
    expect(dayHandler).toBeFunction();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If not weatherAreaId, DateHandler receives dates === Nil", () => {
    render(<LocsInfo id="" locationNames={locationNames} />);
    const { dates } = mockDateHandlerReceivedProps;
    expect(dates).toBeNil();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If wa isLoading, DateHandler receives dates === Nil", () => {
    const isLoadingStore = {
      weatherAreas: {
        wa1: getWeatherArea({ isLoading: true })
      }
    };
    const newUseSelectorMock = cb => cb(isLoadingStore);
    useSelector.mockImplementation(newUseSelectorMock);
    render(<LocsInfo id="wa1" locationNames={locationNames} />);
    const { dates } = mockDateHandlerReceivedProps;
    expect(dates).toBeNil();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Renders DateHandler passing min, max and selected date", () => {
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    const { dates } = mockDateHandlerReceivedProps;
    expect(dates).toEqual({
      min: expect.any(Date),
      max: expect.any(Date),
      current: expect.any(Date)
    });
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("DateHandler prop dates.min is the min date", () => {
    const days = Object.keys(store.weatherAreas[waId].days).sort(
      (a, b) => Date.parse(a) - Date.parse(b)
    );
    const expected = new Date(days[0]);
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    const { dates } = mockDateHandlerReceivedProps;
    expect(dates.min).toEqual(expected);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("DateHandler prop dates.max is the max date", () => {
    const days = Object.keys(store.weatherAreas[waId].days).sort(
      (a, b) => Date.parse(a) - Date.parse(b)
    );
    const expected = new Date(days[days.length - 1]);
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    const { dates } = mockDateHandlerReceivedProps;
    expect(dates.max).toEqual(expected);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("DateHandler prop dates.current is one of weatherArea.days", () => {
    const days = Object.keys(store.weatherAreas[waId].days).map(
      date => new Date(date)
    );
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    const { dates } = mockDateHandlerReceivedProps;
    expect(dates.current).toBeOneOf(days);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("dayHandler(X) changes dates.current X relative positions from days sorted array", () => {
    const days = Object.keys(store.weatherAreas[waId].days)
      .sort((a, b) => Date.parse(a) - Date.parse(b))
      //Convert to primitive values
      .map(date => Date.parse(date));
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    const dayHandler = mockDateHandlerReceivedProps.dayHandler;
    const initialCurrentIndex = days.indexOf(
      mockDateHandlerReceivedProps.dates.current.valueOf()
    );
    let nextMove = initialCurrentIndex !== 0 ? -1 : 1;
    act(() => dayHandler(nextMove));
    expect(mockDateHandlerReceivedProps.dates.current.valueOf()).toBe(
      days[initialCurrentIndex + nextMove]
    );
    nextMove = nextMove * -1;
    act(() => dayHandler(nextMove));
    expect(mockDateHandlerReceivedProps.dates.current.valueOf()).toBe(
      days[initialCurrentIndex]
    );
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If weather is loading, weather info receives empty props", () => {
    const isLoadingStore = {
      weatherAreas: {
        wa1: getWeatherArea({ isLoading: true })
      }
    };
    const newUseSelectorMock = cb => cb(isLoadingStore);
    useSelector.mockImplementation(newUseSelectorMock);
    render(<LocsInfo id="wa1" locationNames={locationNames} />);
    expect(mockWeatherInfoProps.data).toBeNil();
    expect(mockWeatherInfoProps.daySelected).toBeNil();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Renders weather info passign selected day property", () => {
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    expect(mockWeatherInfoProps.daySelected).toEqual(expect.any(Date));
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Renders weather info passign all days property", () => {
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    const days = store.weatherAreas[waId].days;
    expect(mockWeatherInfoProps.data).toEqual(days);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If WeatherArea is not updated dispatch update and listen for errors", () => {
    const { weatherAreas } = store;
    const { wa1 } = weatherAreas;
    const notUpdatedStore = {
      ...store,
      weatherAreas: {
        ...weatherAreas,
        wa1: {
          ...wa1,
          updatedAt: new Date(new Date().setUTCHours(0, 0, 0, 0) - 1)
        }
      }
    };
    useSelector.mockImplementation(cb => cb(notUpdatedStore));
    render(<LocsInfo id={waId} locationNames={locationNames} />);
    expect(mockTryDispatch).toHaveBeenCalledTimes(1);
    expect(mockTryDispatch).toHaveBeenCalledWith(updateWASpy, "wa1");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If there is error with current wa id send message to the user", () => {
    const { weatherAreas } = store;
    const { wa1 } = weatherAreas;
    const notUpdatedStore = {
      ...store,
      weatherAreas: {
        ...weatherAreas,
        wa1: {
          ...wa1,
          updatedAt: new Date(new Date().setUTCHours(0, 0, 0, 0) - 1)
        }
      }
    };
    useSelector.mockImplementation(cb => cb(notUpdatedStore));
    const { rerender } = render(
      <LocsInfo id={waId} locationNames={locationNames} />
    );
    mockErrors = {
      [errorId]: {
        data: { error: errorId },
        type: "network",
        message: "error from test",
        timestamp: new Date(),
        resolve: false
      }
    };
    rerender(<LocsInfo id={waId} locationNames={locationNames} />);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If error with different wa id resolve silently", () => {
    const { weatherAreas } = store;
    const { wa1 } = weatherAreas;
    const notUpdatedStore = {
      ...store,
      weatherAreas: {
        ...weatherAreas,
        wa1: {
          ...wa1,
          updatedAt: new Date(new Date().setUTCHours(0, 0, 0, 0) - 1)
        }
      }
    };
    useSelector.mockImplementation(cb => cb(notUpdatedStore));
    const { rerender } = render(
      <LocsInfo id={waId} locationNames={locationNames} />
    );
    mockErrors = {
      differentErrorId: {
        data: { error: errorId },
        type: "network",
        message: "error from test",
        timestamp: new Date(),
        resolve: false
      }
    };
    rerender(<LocsInfo id={waId} locationNames={locationNames} />);
    expect(mockSendMessage).not.toHaveBeenCalled();
    expect(mockResolve).toHaveBeenCalledTimes(1);
    expect(mockResolve).toHaveBeenCalledWith("differentErrorId");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("If no errors when receiving wa, untrack error", () => {
    const { weatherAreas } = store;
    const { wa1 } = weatherAreas;
    const notUpdatedStore = {
      ...store,
      weatherAreas: {
        ...weatherAreas,
        wa1: {
          ...wa1,
          updatedAt: new Date(new Date().setUTCHours(0, 0, 0, 0) - 1)
        }
      }
    };
    useSelector.mockImplementation(cb => cb(notUpdatedStore));
    const { rerender } = render(
      <LocsInfo id={waId} locationNames={locationNames} />
    );
    useSelector.mockImplementation(useSelectorMock);
    rerender(<LocsInfo id={waId} locationNames={locationNames} />);
    expect(mockSendMessage).not.toHaveBeenCalled();
    expect(mockResolve).toHaveBeenCalledTimes(1);
    expect(mockResolve).toHaveBeenCalledWith(errorId);
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("Error question action dispatches update again", () => {
    const { weatherAreas } = store;
    const { wa1 } = weatherAreas;
    const notUpdatedStore = {
      ...store,
      weatherAreas: {
        ...weatherAreas,
        wa1: {
          ...wa1,
          updatedAt: new Date(new Date().setUTCHours(0, 0, 0, 0) - 1)
        }
      }
    };
    useSelector.mockImplementation(cb => cb(notUpdatedStore));
    const { rerender, getByTestId } = render(
      <LocsInfo id={waId} locationNames={locationNames} />
    );
    mockErrors = {
      [errorId]: {
        data: { error: errorId },
        type: "network",
        message: "error from test",
        timestamp: new Date(),
        resolve: false
      }
    };
    rerender(<LocsInfo id={waId} locationNames={locationNames} />);
    const btn = getByTestId("Question").querySelector("button");
    fireEvent.click(btn);
    expect(mockTryDispatch).toHaveBeenCalled();
    expect(mockTryDispatch).toHaveBeenLastCalledWith(updateWASpy, "wa1");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});
