import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

//Mock for internal components
jest.mock("./containers/MapFrame", () => props => (
  <div data-testid="MapFrame">
    {props.children({
      dateHandler: jest.fn(),
      datesRange: { min: "1", max: "2", current: "3" }
    })}
  </div>
));

jest.mock("./components/SideDrawer", () => props => <div>SideDrawer</div>);

jest.mock("./containers/Messages", () => props => (
  <div data-testid="Messages">Messages</div>
));

jest.mock("./components/DateHandler", () => props => (
  <div data-testid="DateHandler">{props.children}</div>
));

jest.mock("./components/Map", () => props => (
  <div data-testid="Map">{props.children}</div>
));

describe("content", () => {});

describe("Content:", () => {
  let consoleErrorMock;
  beforeEach(() => {
    consoleErrorMock = jest.spyOn(console, "error");
  });
  afterEach(() => {
    consoleErrorMock.mockRestore();
  });
  test("Renders the MapFrame", () => {
    const { getByTestId } = render(<App />);
    const mainViewElement = getByTestId("MapFrame");
    expect(mainViewElement).toBeInTheDocument();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("renders the SideDrawer", () => {
    const { getByText } = render(<App />);
    const sideDrawerElement = getByText(/sidedrawer/i);
    expect(sideDrawerElement).toBeInTheDocument();
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders DateHandler", () => {
    const { getByTestId } = render(<App />);
    getByTestId("DateHandler");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders the Map", () => {
    const { getByTestId } = render(<App />);
    getByTestId("Map");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
  test("It renders Messages", () => {
    const { getByTestId } = render(<App />);
    getByTestId("Messages");
    // If prop-types print errors the test is not passed
    expect(consoleErrorMock).not.toHaveBeenCalled();
  });
});
