import { renderHook, act } from "@testing-library/react-hooks";
import { useDispatch } from "react-redux";
import { actions } from "../../store";
import useMessages from "./index.js";

/* Specs */
const defaultMessageType = "Default";
const defaultDurationTime = 10000;

/* Mocks */
const mockDispatch = jest.fn();
jest.mock("react-redux", () => ({
  useDispatch: jest.fn()
}));
useDispatch.mockImplementation(() => mockDispatch);
const messagesActions = actions.messages;
const setResolvedSpy = jest
  .spyOn(messagesActions, "add")
  .mockImplementation(() => messagesActions);

afterEach(() => {
  useDispatch.mockClear();
  mockDispatch.mockClear();
  setResolvedSpy.mockClear();
});

describe("useMessages", () => {
  test("it returns a function", () => {
    expect(renderHook(() => useMessages()).result.current).toBeFunction();
  });
  test("Returning function dispatch message", () => {
    const message = "Test-Returning function dispatch message";
    const sendMessage = renderHook(() => useMessages()).result.current;
    expect(useDispatch).toHaveBeenCalledTimes(1);
    expect(mockDispatch).not.toHaveBeenCalled();
    act(() => sendMessage(message));
    expect(mockDispatch).toHaveBeenCalledTimes(1);
    expect(mockDispatch).toHaveBeenCalledWith(messagesActions);
    expect(setResolvedSpy).toHaveBeenCalledTimes(1);
    expect(setResolvedSpy).toHaveBeenCalledWith(
      message,
      expect.anything(),
      expect.anything()
    );
  });
  test(`Message default duration: ${defaultDurationTime}ms, default type: ${defaultMessageType}`, () => {
    const message = `Test- Message default duration: ${defaultDurationTime}ms, default type: ${defaultMessageType}`;
    const sendMessage = renderHook(() => useMessages()).result.current;
    act(() => sendMessage(message));
    expect(setResolvedSpy).toHaveBeenCalledWith(
      message,
      defaultMessageType,
      defaultDurationTime
    );
  });
  test("Returning function dispatch message with custom duration", () => {
    const message =
      "Test- Returning function dispatch message with custom duration";
    const customDuration = 150000;
    const sendMessage = renderHook(() => useMessages()).result.current;
    act(() => sendMessage(message, customDuration));
    expect(setResolvedSpy).toHaveBeenCalledWith(
      message,
      expect.anything(),
      customDuration
    );
  });
  test.each(["Default, Danger, Highlight"])(
    "Returning function dispatch message with type: %p",
    type => {
      const message =
        "Test- Returning function dispatch message with custom duration";
      const sendMessage = renderHook(() => useMessages()).result.current;
      act(() => sendMessage(message, undefined, type));
      expect(setResolvedSpy).toHaveBeenCalledWith(
        message,
        type,
        expect.anything()
      );
    }
  );
});
