import { renderHook, act } from "@testing-library/react-hooks";
import { useSelector, useDispatch } from "react-redux";
import getUniqueId from "../../utils/uniqueId";
import { actions } from "../../store";
import useErrorsListener from "./index";

const errors = {}; //useSelector must return always same object in order to avoid rerenders
jest.mock("react-redux", () => ({
  useSelector: jest.fn(cb => cb({ errors })),
  useDispatch: jest.fn(() => jest.fn())
}));

jest.mock("../../utils/uniqueId", () => {
  return {
    __esModule: true,
    default: jest.fn(() => "errorId")
  };
});

const mockActionGenerator = jest.fn();
const mockPayload = "payload";

afterEach(() => {
  useSelector.mockClear();
  useSelector.mockImplementation(cb => cb({ errors }));
  useDispatch.mockReset();
  useDispatch.mockImplementation(() => jest.fn());
  getUniqueId.mockReset();
  mockActionGenerator.mockClear();
});

describe("useErrorListener", () => {
  test("It returns and object with 3 keys", () => {
    expect(
      renderHook(() => useErrorsListener()).result.current
    ).toContainAllKeys(["errors", "tryDispatch", "resolve"]);
  });
  test("errors is object with all tracked errors", () => {
    expect(
      renderHook(() => useErrorsListener()).result.current.errors
    ).toBeObject();
  });
  test("tryDispatch is a function", () => {
    expect(
      renderHook(() => useErrorsListener()).result.current.tryDispatch
    ).toBeFunction();
  });
  test("resolve is a function", () => {
    expect(
      renderHook(() => useErrorsListener()).result.current.resolve
    ).toBeFunction();
  });
});

describe("errors", () => {
  const errorsStateMock = {
    error1: "error1",
    error2: "error2"
  };
  const errorIds = ["error1", "error2", "error3"];
  test("It contains only Ids errors that match subcribed dispatches", () => {
    useSelector.mockImplementation(cb => cb({ errors: errorsStateMock }));
    const { result } = renderHook(() => useErrorsListener());
    errorIds.forEach(err =>
      act(() => {
        getUniqueId.mockReturnValueOnce(err);
        result.current.tryDispatch(mockActionGenerator, mockPayload);
      })
    );
    expect(result.current.errors).toContainAllKeys(["error1", "error2"]);
  });
});

describe("tryDispatch", () => {
  const errorId = "test-tryDispatch-errorId";
  beforeEach(() => {
    getUniqueId.mockReturnValueOnce(errorId);
  });
  test("It returns error Id", () => {
    const {
      result: {
        current: { tryDispatch }
      }
    } = renderHook(() => useErrorsListener());
    act(() => {
      const result = tryDispatch(mockActionGenerator, mockPayload);
      expect(result).toBe(errorId);
    });
  });
  test("It dispatches the actionGenerator including payload and errorId", () => {
    const mockDispatch = jest.fn();
    useDispatch.mockReturnValueOnce(mockDispatch);
    const testAction = "dispatchActionCreator";
    mockActionGenerator.mockReturnValueOnce(testAction);
    const {
      result: {
        current: { tryDispatch }
      }
    } = renderHook(() => useErrorsListener());
    act(() => {
      tryDispatch(mockActionGenerator, mockPayload);
    });
    expect(mockActionGenerator).toHaveBeenCalledTimes(1);
    expect(mockActionGenerator).toHaveBeenCalledWith(mockPayload, errorId);
    expect(mockDispatch).toHaveBeenCalledTimes(1);
    expect(mockDispatch).toHaveBeenCalledWith(testAction);
  });
  test("It subscribes the errorId", () => {
    const errorsStateMock = {
      [errorId]: { resolved: false }
    };
    useSelector.mockImplementation(cb =>
      cb({ errors: errorsStateMock, test: true })
    );
    const { result } = renderHook(() => useErrorsListener());
    expect(result.current.errors).not.toContainKey(errorId);
    act(() => {
      getUniqueId.mockReturnValueOnce(errorId);
      result.current.tryDispatch(mockActionGenerator, mockPayload);
    });
    expect(result.current.errors).toContainKey(errorId);
  });
});

describe("resolve", () => {
  const errorId = "test-non-resolved-errorId";
  const errorId2 = "test-resolved-errorId";
  const errorsStateMock = {
    [errorId]: { resolved: false, message: `message from ${errorId}` },
    [errorId2]: { resolved: true }
  };
  const mockDispatch = jest.fn();
  beforeEach(() => {
    useSelector.mockImplementation(cb => cb({ errors: errorsStateMock }));
    useDispatch.mockReturnValue(mockDispatch);
  });
  afterEach(() => {
    mockDispatch.mockClear();
  });
  test("It unsubscribes the dispatch", () => {
    const { result } = renderHook(() => useErrorsListener());
    act(() => {
      getUniqueId.mockReturnValueOnce(errorId);
      result.current.tryDispatch(mockActionGenerator, mockPayload);
    });
    expect(result.current.errors).toContainKey(errorId);
    act(() => {
      result.current.resolve(errorId);
    });
    expect(result.current.errors).not.toContainKey(errorId);
  });
  test("It marks the error as resolved", () => {
    const errorsActions = actions.errors;
    const setResolvedSpy = jest
      .spyOn(errorsActions, "setResolved")
      .mockImplementation(() => "setResolvedActionGenerator");
    const { result } = renderHook(() => useErrorsListener());
    act(() => {
      getUniqueId.mockReturnValueOnce(errorId);
      result.current.tryDispatch(mockActionGenerator, mockPayload);
    });
    mockDispatch.mockClear();
    act(() => {
      result.current.resolve(errorId);
    });
    expect(setResolvedSpy).toHaveBeenCalledTimes(1);
    expect(setResolvedSpy).toHaveBeenCalledWith(errorId);
    expect(mockDispatch).toHaveBeenCalledTimes(1);
    expect(mockDispatch).toHaveBeenCalledWith("setResolvedActionGenerator");
  });
  describe("Returning value", () => {
    test("It is null if errorId does not match a non resolved error", () => {
      const { result } = renderHook(() => useErrorsListener());
      act(() => {
        getUniqueId.mockReturnValueOnce(errorId2);
        result.current.tryDispatch(mockActionGenerator, mockPayload);
      });
      let resolveReturn;
      act(() => {
        resolveReturn = result.current.resolve(errorId);
      });
      expect(resolveReturn).toBeNil();
    });
    test("It is object with resolving methods", () => {
      const { result } = renderHook(() => useErrorsListener());
      act(() => {
        getUniqueId.mockReturnValueOnce(errorId);
        result.current.tryDispatch(mockActionGenerator, mockPayload);
      });
      let resolveReturn;
      act(() => {
        resolveReturn = result.current.resolve(errorId);
      });
      expect(resolveReturn).toContainAllKeys(["sendMessage"]);
    });
    describe("Resolving methods - sendMessage", () => {
      let sendMessage, resolvedReturnedValue;
      const defaultArgs = {
        message: "Test message",
        duration: 5000
      };
      beforeEach(() => {
        const { result } = renderHook(() => useErrorsListener());
        act(() => {
          getUniqueId.mockReturnValueOnce(errorId);
          result.current.tryDispatch(mockActionGenerator, mockPayload);
        });
        act(() => {
          resolvedReturnedValue = result.current.resolve(errorId);
          sendMessage = resolvedReturnedValue.sendMessage;
        });
      });
      afterEach(() => {
        mockDispatch.mockClear();
      });
      test("It is a function", () => {
        expect(sendMessage).toBeFunction();
      });
      test("It dispatches a new message", () => {
        const messageActions = actions.messages;
        const addMessageSpy = jest
          .spyOn(messageActions, "add")
          .mockImplementation(() => "adMsgActionGenerator");
        const { message, duration } = defaultArgs;
        sendMessage(message, duration);
        expect(addMessageSpy).toHaveBeenCalledTimes(1);
        expect(addMessageSpy).toHaveBeenCalledWith(message, "Danger", duration);
        expect(mockDispatch).toHaveBeenCalledWith("adMsgActionGenerator");
      });
      test("New message can have custom message", () => {
        const messageActions = actions.messages;
        const addMessageSpy = jest
          .spyOn(messageActions, "add")
          .mockImplementation(() => "adMsgActionGenerator");
        const { duration } = defaultArgs;
        const customMessage = "custom message";
        sendMessage(customMessage, duration);
        expect(addMessageSpy).toHaveBeenCalledWith(
          "custom message",
          "Danger",
          duration
        );
      });
      test("If not custom message, error.message is used instead", () => {
        const messageActions = actions.messages;
        const addMessageSpy = jest
          .spyOn(messageActions, "add")
          .mockImplementation(() => "adMsgActionGenerator");
        const { duration } = defaultArgs;
        sendMessage(undefined, duration);
        expect(addMessageSpy).toHaveBeenCalledWith(
          errorsStateMock[errorId].message,
          "Danger",
          duration
        );
      });
      test("Default message delay is 10000ms", () => {
        const messageActions = actions.messages;
        const addMessageSpy = jest
          .spyOn(messageActions, "add")
          .mockImplementation(() => "adMsgActionGenerator");
        sendMessage();
        expect(addMessageSpy).toHaveBeenCalledWith(
          errorsStateMock[errorId].message,
          "Danger",
          10000
        );
      });
      test("Message delay can be passed as argument", () => {
        const messageActions = actions.messages;
        const addMessageSpy = jest
          .spyOn(messageActions, "add")
          .mockImplementation(() => "adMsgActionGenerator");
        const { duration } = defaultArgs;
        sendMessage(undefined, duration);
        expect(addMessageSpy).toHaveBeenCalledWith(
          expect.anything(),
          expect.anything(),
          duration
        );
      });
      test("It returns parent object to allow composition", () => {
        const messageActions = actions.messages;
        jest
          .spyOn(messageActions, "add")
          .mockImplementation(() => "adMsgActionGenerator");
        const parent = sendMessage();
        expect(parent).toBe(resolvedReturnedValue);
      });
    });
  });
});
