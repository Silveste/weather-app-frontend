import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import withUserInput from './index';

// Mocks for internal functions
jest.mock(
  '../../components/UI/Modal',
  () => props => (<><h1>Modal</h1>{props.children}</>)
)
jest.mock(
  '../../components/Question',
  () => props => (
    <>
      <div>{props.children}</div>
      {[].concat(props.options).map(c => (
        <button
          key={c.text}
          onClick={c.onAction}
          className={c.format}
          title={c.tooltip}
        >
          {c.text}
        </button>
      ))}
    </>
  )
)

// withUserInput internal defaults (chenge them if you change the defaults)
const defaultCancelBtnText = 'Cancel'

// Config controls
const controlConfig = {
  text: 'Test control',
  cbName: 'mappedCallback'
}

// Mocks for props functions
const mappedCallback = jest.fn();
const normalCallback = jest.fn();

// Wrapped Component definitions
const simpleComponent = props => {
  const { mappedCallback, normalCallback } = props;
  return (
    <>
      <h1>Wrapped Component</h1>
      <button onClick={mappedCallback}>Mapped Callback</button>
      <button onClick={normalCallback}>Normal Callback</button>
    </>
  )
}
const argsComponent = props => {
  const { mappedCallback, normalCallback } = props;
  return (
    <>
      <h1>Wrapped Component</h1>
      <button onClick={() => mappedCallback('arg1','arg2')}>Mapped Callback</button>
      <button onClick={normalCallback}>Normal Callback</button>
    </>
  )
}


describe('Content', () => {
  beforeEach(() => {
    mappedCallback.mockClear();
    normalCallback.mockClear();
  })
  test('Provides an modal to get input from the user', () => {
    const WrappedComponent = withUserInput(simpleComponent, controlConfig);
    const { getByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    const childComponentBtn = getByText(/mapped/i);
    fireEvent.click(childComponentBtn);
    getByText(/modal/i);
  });
  test('Provides a description in the modal', () => {
    const config = {...controlConfig, description: 'desc'};
    const WrappedComponent = withUserInput(simpleComponent, config);
    const { getByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    const childComponentBtn = getByText(/mapped/i);
    fireEvent.click(childComponentBtn);
    getByText(/desc/i);
  })
  test('Provides controls that map to callbacks', () => {
    const WrappedComponent = withUserInput(simpleComponent, controlConfig);
    const { getByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    const childComponentBtn = getByText(/mapped/i);
    fireEvent.click(childComponentBtn);
    getByText(/control/i);
  });
  test('Wrapped component gets all non-mapped props (original)', () => {
    const WrappedComponent = withUserInput(simpleComponent, controlConfig);
    const { getByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    const childComponentBtn = getByText(/normal/i);
    fireEvent.click(childComponentBtn);
    expect(normalCallback).toHaveBeenCalledTimes(1);
  });
  test('Wrapped component gets all mapped props (with a pointer to other function)', () => {
    const WrappedComponent = withUserInput(simpleComponent, controlConfig);
    const { getByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    const childComponentBtn = getByText(/mapped/i);
    fireEvent.click(childComponentBtn);
    expect(mappedCallback).not.toHaveBeenCalled();
  });
})
describe('Behaviour', () => {
  beforeEach(() => {
    mappedCallback.mockClear();
    normalCallback.mockClear();
  })
  test('It always renders the wrapped component', () => {
      const config = {...controlConfig, closeControl: true }
      const WrappedComponent = withUserInput(simpleComponent, config);
      const { getByText, queryByText } = render(
        <WrappedComponent
          mappedCallback={mappedCallback}
          normalCallback={normalCallback}
        />
      );
      expect(queryByText(/wrapped/i)).not.toBeNull();
      const childComponentBtn = getByText(/mapped/i);
      fireEvent.click(childComponentBtn);
      const closeModalBtn = getByText(new RegExp(defaultCancelBtnText, 'i'));
      expect(queryByText(/wrapped/i)).not.toBeNull();
      fireEvent.click(closeModalBtn);
      expect(queryByText(/wrapped/i)).not.toBeNull();
    });
  test('Wrapped component triggers the modal when calls mapped callback', () => {
      const WrappedComponent = withUserInput(simpleComponent,controlConfig);
      const { getByText, queryByText } = render(
        <WrappedComponent
          mappedCallback={mappedCallback}
          normalCallback={normalCallback}
        />
      );
      expect(queryByText(/modal/i)).toBeNull();
      const childComponentBtn = getByText(/mapped/i);
      fireEvent.click(childComponentBtn);
      expect(queryByText(/modal/i)).not.toBeNull();
    });
  test('inputRequired receives array with args passed to the callback and original props', () => {
    const inputRequired = jest.fn();
    const WrappedComponent = withUserInput(
      argsComponent,
      { ...controlConfig, inputRequired }
    );
    const { getByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    const childComponentBtn = getByText(/mapped/i);
    fireEvent.click(childComponentBtn);
    expect(inputRequired).toHaveBeenCalledWith(
      expect.arrayContaining(['arg1', 'arg2']),
      expect.objectContaining({ mappedCallback, normalCallback })
    );
  });
  test('If inputRequired function returns true. Modal renders', () => {
      const WrappedComponent = withUserInput(
        simpleComponent,
        { ...controlConfig, inputRequired: () => true }
      );
      const { getByText, queryByText } = render(
        <WrappedComponent
          mappedCallback={mappedCallback}
          normalCallback={normalCallback}
        />
      );
      expect(queryByText(/modal/i)).toBeNull();
      const childComponentBtn = getByText(/mapped/i);
      fireEvent.click(childComponentBtn);
      expect(queryByText(/modal/i)).not.toBeNull();
    });
  test('If inputRequired function returns false. Modal do not render', () => {
    const WrappedComponent = withUserInput(
      simpleComponent,
      { ...controlConfig, inputRequired: () => false }
    );
    const { getByText, queryByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    expect(queryByText(/modal/i)).toBeNull();
    const childComponentBtn = getByText(/mapped/i);
    fireEvent.click(childComponentBtn);
    expect(queryByText(/modal/i)).toBeNull();
  });
  test('If contitionalArgs returns false. callback is executed', () => {
    const WrappedComponent = withUserInput(
      simpleComponent,
      { ...controlConfig, inputRequired: () => false }
    );
    const { getByText, queryByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    const childComponentBtn = getByText(/mapped/i);
    fireEvent.click(childComponentBtn);
    expect(queryByText(/modal/i)).toBeNull();
    expect(mappedCallback).toHaveBeenCalledTimes(1);
  });
  test('Callback is executed after user interaction with mapped control', () => {
      const WrappedComponent = withUserInput(simpleComponent,controlConfig);
      const { getByText, queryByText } = render(
        <WrappedComponent
          mappedCallback={mappedCallback}
          normalCallback={normalCallback}
        />
      );
      expect(queryByText(/modal/i)).toBeNull();
      const childComponentBtn = getByText(/mapped/i);
      fireEvent.click(childComponentBtn);
      const controlBtn = getByText(/control/i);
      fireEvent.click(controlBtn);
      expect(mappedCallback).toHaveBeenCalledTimes(1);
  });
  test('The modal close after callback is executed', () => {
    const WrappedComponent = withUserInput(simpleComponent,controlConfig);
    const { getByText, queryByText } = render(
      <WrappedComponent
        mappedCallback={mappedCallback}
        normalCallback={normalCallback}
      />
    );
    expect(queryByText(/modal/i)).toBeNull();
    const childComponentBtn = getByText(/mapped/i);
    fireEvent.click(childComponentBtn);
    const controlBtn = getByText(/control/i);
    fireEvent.click(controlBtn);
    expect(queryByText(/modal/i)).toBeNull();
  });
  test('Callback arguments are passed', () => {
      const WrappedComponent = withUserInput(argsComponent,controlConfig);
      const { getByText, queryByText } = render(
        <WrappedComponent
          mappedCallback={mappedCallback}
          normalCallback={normalCallback}
        />
      );
      expect(queryByText(/modal/i)).toBeNull();
      const childComponentBtn = getByText(/mapped/i);
      fireEvent.click(childComponentBtn);
      const controlBtn = getByText(/control/i);
      fireEvent.click(controlBtn);
      expect(mappedCallback).toHaveBeenCalledTimes(1);
      expect(mappedCallback).toHaveBeenCalledWith('arg1','arg2');
    });
})
