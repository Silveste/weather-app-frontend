import React from "react";
import Header from "../../components/UI/Headers/H2";
import styles from "./About.module.css";

const About = () => (
  <article className={styles.Container}>
    <Header>About Weather App</Header>
    <div className={styles.Main}>
      <p>
        App built by{" "}
        <a
          href="https://www.silveste.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Antonio Fernandez @Silveste
        </a>
      </p>
      <p>
        The source code is available here:{" "}
        <a
          href="hhttps://gitlab.com/Silveste/weather-app-backend"
          target="_blank"
          rel="noopener noreferrer"
        >
          backend
        </a>{" "}
        and{" "}
        <a
          href="https://gitlab.com/Silveste/weather-app-frontend"
          target="_blank"
          rel="noopener noreferrer"
        >
          frontend
        </a>
        . under{" "}
        <a
          href="https://mit-license.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          MIT license
        </a>
        .
      </p>
      <p>
        If you find something useful, feel free to bring it into your project.
        Acknowledge is not required however is always welcome.
      </p>
      <p>
        The weather icons have been created by Sihan Liu and are available at{" "}
        <a
          href="https://www.iconfinder.com/iconsets/weather-color-2"
          target="_blank"
          rel="noopener noreferrer"
        >
          www.iconfinder.com
        </a>
      </p>
      <p>
        The backend uses{" "}
        <a
          href="https://nominatim.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Nominatim
        </a>{" "}
        and{" "}
        <a
          href="https://openweathermap.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          open weather APIs
        </a>
      </p>
    </div>
  </article>
);

export default About;
