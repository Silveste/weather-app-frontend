//TODO Check what happens when response is empty array
import faker from "faker";
import uniqueId from "../utils/uniqueId";
import { mapLocation, sortAndMapLocations, mapWeatherArea } from "./dataMaps";

//helpers
const propTypeChecker = (prop, type) => {
  switch (type.toLowerCase()) {
    case "boolean":
      expect(prop).toBeBoolean();
      break;
    case "array":
      expect(prop).toBeArray();
      break;
    case "number":
      expect(prop).toBeNumber();
      break;
    case "date":
      expect(prop).toBeDate();
      break;
    case "function":
      expect(prop).toBeFunction();
      break;
    case "object":
      expect(prop).toBeObject();
      break;
    default:
      expect(prop.constructor.name).toBe(type);
  }
};

//Mocks
const getBackendLocMock = (obj = {}) => ({
  name: faker.lorem.words(Math.ceil(Math.random() * 3)),
  geometry: {
    type: "Polygon",
    coordinates: [
      [0, 0],
      [1, 0],
      [1, 1],
      [0, 1],
      [0, 0]
    ]
  },
  county: faker.lorem.words(Math.ceil(Math.random() * 3)),
  country: faker.lorem.words(Math.ceil(Math.random() * 3)),
  displayName: faker.lorem.words(Math.ceil(Math.random() * 9)),
  type: faker.lorem.word(),
  importance: `${faker.random.number(50)}`,
  score: obj.score === undefined ? `${faker.random.number(50)}` : obj.score,
  id: uniqueId("Fake_location")
});

const getBackendWeatherListMock = (obj = {}) => {
  const day = obj.day || new Date(new Date().setUTCHours(0, 0, 0, 0));
  const avgTemp = Math.random() * 10 + 20;
  return {
    dt: day.toISOString(),
    sunrise: day.valueOf() + Math.floor((5 + Math.random() * 3) * 3600000),
    sunset: day.valueOf() + Math.floor((18 + Math.random() * 4) * 3600000),
    temp: {
      avg: avgTemp,
      feelsLike: Math.random() * 4 + avgTemp - 2,
      min: Math.random() * -20 + avgTemp,
      max: Math.random() * 20 + avgTemp
    },
    main: [
      {
        main: faker.lorem.word(),
        description: faker.lorem.words(Math.ceil(Math.random() * 5)),
        icon: [
          "01d",
          "01n",
          "02d",
          "02n",
          "03d",
          "03n",
          "04d",
          "04n",
          "09d",
          "09n",
          "10d",
          "10n",
          "11d",
          "11n",
          "13d",
          "13n",
          "50d",
          "50n"
        ][Math.floor(Math.random() * 18)]
      }
    ],
    clouds:
      obj.clouds !== undefined
        ? obj.clouds
        : Math.random() < 0.5
        ? Math.random() * 100
        : undefined,
    wind:
      obj.wind !== undefined
        ? obj.wind
        : Math.random() < 0.5
        ? { speed: Math.random() * 300, deg: Math.random() * 360 }
        : undefined,
    rain:
      obj.rain !== undefined
        ? obj.rain
        : Math.random() < 0.5
        ? Math.random() * 500
        : undefined,
    snow:
      obj.snow !== undefined
        ? obj.snow
        : Math.random() < 0.5
        ? Math.random() * 500
        : undefined
  };
};

const getBackendWAMock = (obj = {}) => ({
  geometry: {
    type: "Polygon",
    coordinates: [
      [0, 0],
      [1, 0],
      [1, 1],
      [0, 1],
      [0, 0]
    ]
  },
  list:
    obj.list !== undefined
      ? obj.list
      : new Array(7).fill().map((_, i) =>
          getBackendWeatherListMock({
            day: new Date(
              new Date().setUTCHours(0, 0, 0, 0) + (i - 1) * 24 * 3600000
            )
          })
        ),
  id: uniqueId("Fake_location")
});

const getFrontendLocProps = () => ({
  queriesTracker: 0,
  onlyCached: true,
  isOnMap: false,
  isSaved: false,
  weatherArea: uniqueId("Fake_weatherArea")
});

describe("mapLoaction", () => {
  test("Expect to return object with only required keys", () => {
    const frontendProps = getFrontendLocProps();
    const result = mapLocation(getBackendLocMock(), frontendProps);
    expect(result).toContainAllKeys([
      ...Object.keys(frontendProps),
      "name",
      "geometry",
      "description",
      "importance",
      "id"
    ]);
  });
  test.each([
    ["queriesTracker", "number"],
    ["onlyCached", "boolean"],
    ["isSaved", "boolean"],
    ["weatherArea", "String"],
    ["geometry", "object"],
    ["name", "String"],
    ["description", "String"],
    ["importance", "number"],
    ["isOnMap", "boolean"],
    ["id", "String"]
  ])("It returns object with property %s, type of %s", (prop, type) => {
    const result = mapLocation(getBackendLocMock(), getFrontendLocProps());
    expect(prop).not.toBeNil();
    propTypeChecker(result[prop], type);
  });
});

describe("sortAndMapLocations", () => {
  test("It returns array of same size than arg", () => {
    const rawLocations = new Array(6).fill().map(_ => getBackendLocMock());
    const result = sortAndMapLocations(rawLocations);
    expect(result).toBeArrayOfSize(6);
  });
  test.each(["name", "geometry", "description", "importance", "id"])(
    "It returns array of objects that includes key: %s",
    key => {
      const rawLocations = new Array(6).fill().map(_ => getBackendLocMock());
      const result = sortAndMapLocations(rawLocations);
      expect.hasAssertions();
      result.forEach(loc => {
        expect(loc).toContainKey(key);
      });
    }
  );
  test.each([
    ["queriesTracker", 0],
    ["onlyCached", true],
    ["isOnMap", false],
    ["weatherAreaId", null],
    ["isSaved", false]
  ])(
    "Returns array of objects where key %s is initialized to value: %p",
    (key, value) => {
      const rawLocations = new Array(6).fill().map(_ => getBackendLocMock());
      const result = sortAndMapLocations(rawLocations);
      expect.hasAssertions();
      result.forEach(loc => {
        expect(loc).toContainEntry([key, value]);
      });
    }
  );
  test("It returns array sorted by score", () => {
    const rawLocations = new Array(6).fill().map(_ => getBackendLocMock());
    const sorted = [...rawLocations].sort((a, b) => b.score - a.score);
    const result = sortAndMapLocations(rawLocations);
    expect.hasAssertions();
    result.forEach((loc, i) => {
      expect(loc.id).toBe(sorted[i].id);
    });
  });
  test("If sorted is null, it returns array sorted by importance", () => {
    const rawLocations = new Array(6)
      .fill()
      .map(_ => getBackendLocMock({ score: null }));
    const sorted = [...rawLocations].sort(
      (a, b) => b.importance - a.importance
    );
    const result = sortAndMapLocations(rawLocations);
    expect.hasAssertions();
    result.forEach((loc, i) => {
      expect(loc.id).toBe(sorted[i].id);
    });
  });
});

describe("mapWeatherArea", () => {
  test("Expect to return object with only required keys", () => {
    const frontendProps = { updatedAt: new Date(), isLoading: false };
    const result = mapWeatherArea(getBackendWAMock(), frontendProps);
    expect(result).toContainAllKeys([
      ...Object.keys(frontendProps),
      "days",
      "geometry",
      "id"
    ]);
  });
  test.each([
    ["days", "object"],
    ["geometry", "object"],
    ["id", "String"]
  ])("It returns object with property %p, type %p", (prop, type) => {
    const frontendProps = { updatedAt: new Date(), loading: false };
    const result = mapWeatherArea(getBackendWAMock(), frontendProps);
    expect(prop).not.toBeNil();
    propTypeChecker(result[prop], type);
  });
  test("days property contains object with keys = date ISO string at time 0", () => {
    const frontendProps = { updatedAt: new Date(), loading: false };
    const result = mapWeatherArea(getBackendWAMock(), frontendProps);
    const { days } = result;
    expect.hasAssertions();
    Object.keys(days).forEach(key => {
      const date = new Date(key).setUTCHours(0, 0, 0, 0);
      expect(key).toBe(new Date(date).toISOString());
    });
  });
  test.each([
    ["days", "object"],
    ["geometry", "object"],
    ["id", "String"]
  ])("It returns object with property %p, type %p", (prop, type) => {
    const frontendProps = { updatedAt: new Date(), loading: false };
    const result = mapWeatherArea(getBackendWAMock(), frontendProps);
    expect(prop).not.toBeNil();
    propTypeChecker(result[prop], type);
  });
  test.each([
    ["date", "Date"],
    ["sunrise", "Date"],
    ["sunset", "Date"],
    ["clouds", "number"],
    ["rain", "Number"],
    ["snow", "Number"],
    ["temp", "Object"],
    ["wind", "Object"],
    ["main", "Object"]
  ])(
    "result[x_Key].days contains objects with property %p type %p",
    (prop, type) => {
      const frontendProps = { updatedAt: new Date(), loading: false };
      const wa = mapWeatherArea(getBackendWAMock(), frontendProps);
      const { days } = wa;
      expect.hasAssertions();
      Object.keys(days).forEach(key => {
        propTypeChecker(days[key][prop], type);
      });
    }
  );
  test.each([
    ["avg", "Number"],
    ["feelsLike", "Number"],
    ["min", "Number"],
    ["max", "Number"]
  ])(
    "result[x_Key].days[y_key].temp contains objects with property %p type %p",
    (prop, type) => {
      const frontendProps = { updatedAt: new Date(), loading: false };
      const wa = mapWeatherArea(getBackendWAMock(), frontendProps);
      const { days } = wa;
      expect.hasAssertions();
      Object.keys(days).forEach(key => {
        propTypeChecker(days[key].temp[prop], type);
      });
    }
  );
  test.each([
    ["speed", "Number"],
    ["deg", "Number"]
  ])(
    "result[x_Key].days[y_key].wind contains objects with property %p type %p",
    (prop, type) => {
      const frontendProps = { updatedAt: new Date(), loading: false };
      const wa = mapWeatherArea(getBackendWAMock(), frontendProps);
      const { days } = wa;
      expect.hasAssertions();
      Object.keys(days).forEach(key => {
        propTypeChecker(days[key].wind[prop], type);
      });
    }
  );
  test.each([
    ["main", "String"],
    ["description", "String"],
    ["icon", "String"]
  ])(
    "result[x_Key].days[y_key].main contains objects with property %p type %p",
    (prop, type) => {
      const frontendProps = { updatedAt: new Date(), loading: false };
      const wa = mapWeatherArea(getBackendWAMock(), frontendProps);
      const { days } = wa;
      expect.hasAssertions();
      Object.keys(days).forEach(key => {
        propTypeChecker(days[key].main[prop], type);
      });
    }
  );
  test("It returns object where Nil props/values are initialized", () => {
    const frontendProps = { updatedAt: new Date(), loading: false };
    const date = new Date(new Date().setUTCHours(0, 0, 0, 0));
    const list = [
      getBackendWeatherListMock({
        day: new Date(new Date().setUTCHours(0, 0, 0, 0)),
        clouds: null,
        wind: null,
        rain: null,
        snow: null
      })
    ];
    const wa = mapWeatherArea(getBackendWAMock({ list }), frontendProps);
    const day = wa.days[date.toISOString()];
    expect.hasAssertions();
    propTypeChecker(day.clouds, "Number");
    propTypeChecker(day.wind, "Object");
    propTypeChecker(day.rain, "Number");
    propTypeChecker(day.snow, "Number");
  });
});
